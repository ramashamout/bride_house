<?php
Route::resource('itemsDeliverd', 'App\Http\Controllers\ItemsDeliverdController');
Route::get('/create_itemsDeliverd_with_id/{contract_id}','App\Http\Controllers\ItemsDeliverdController@create_itemsDeliverd_with_id');
Route::get('/display_itemsDeliverd/{contract_id}','App\Http\Controllers\ItemsDeliverdController@display_itemsDeliverd');
