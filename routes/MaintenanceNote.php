<?php

//Route::resource('maintenance', 'App\Http\Controllers\MaintenanceNoteController');
//Route::get('/maintenance','App\Http\Controllers\MaintenanceNoteController@display');
//Route::get('/markAsMainRead/{id}','App\Http\Controllers\MaintenanceNoteController@mark_as_read')->name('markAsMainRead');
Route::resource('maintenance', 'App\Http\Controllers\MaintenanceNoteController');
Route::get('/maintenance','App\Http\Controllers\MaintenanceNoteController@display');
Route::get('/markAsMainRead/{id}','App\Http\Controllers\MaintenanceNoteController@mark_as_read')->name('markAsMainRead');
