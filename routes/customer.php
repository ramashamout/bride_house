<?php
Route::resource('customer', 'App\Http\Controllers\CustomerController');
Route::get('/all_customer','App\Http\Controllers\CustomerController@all_customer');
Route::get('/customer_note/{id}','App\Http\Controllers\CustomerController@customer_note');