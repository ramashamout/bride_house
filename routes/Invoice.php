<?php
Route::resource('invoice', 'App\Http\Controllers\InvoiceController');

Route::get('/invoice_total/{id}','App\Http\Controllers\InvoiceController@total_price')->name('invoice_total');
Route::get('/biginvoice','App\Http\Controllers\InvoiceController@create');
Route::post('/filter_selling','App\Http\Controllers\Filter@filter_selling')->name('filter_selling');
Route::post('/filter_invoice_date','App\Http\Controllers\Filter@filter_invoice_date')->name('filter_invoice_date');