<?php

Route::resource('clean', 'App\Http\Controllers\CleaningController');
Route::get('/addclean','App\Http\Controllers\CleaningController@create');
Route::get('/markAsCleanRead/{id}','App\Http\Controllers\CleaningController@mark_as_read')->name('markAsCleanRead');

