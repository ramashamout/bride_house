<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\API\ClotheController;
use App\Http\Controllers\API\AppointmentController;
use App\Http\Controllers\API\AccessoryController;
use App\Http\Controllers\API\NewPasswordController;

use App\Http\Controllers\API\EmailVerificationController;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::get('/no_winner','App\Http\Controllers\HomeController@no_winner');
Route::get('/send_info/{id}/{price}','App\Http\Controllers\HomeController@send_info');
Route::get('/send_piece/{id}','App\Http\Controllers\HomeController@send_piece');
Route::get('/getallpic','App\Http\Controllers\API\HomeController@getallpic');

Route::get('/send_description/{id}','App\Http\Controllers\HomeController@send_description');
//API route for register new user
Route::post('/register', [App\Http\Controllers\API\AuthController::class, 'register']);
//API route for login user
Route::post('/login', [App\Http\Controllers\API\AuthController::class, 'login']);
Route::middleware('auth:sanctum','verified')->get('/user', function (Request $request) {
    return $request->user();
});

//Protecting Routes
Route::group(['middleware' => ['auth:sanctum']], function () {
   /////////////////////////////user
Route::get('/profile', [App\Http\Controllers\API\AuthController::class, 'user']);
Route::put('/update_profile', [App\Http\Controllers\API\AuthController::class, 'Update_profile']);


/////////////////////////////////Appointment
Route::post('/create', [AppointmentController::Class,'create']);
Route::get('/booked', [AppointmentController::Class,'booked']);
Route::post('/delete_appointment/{id}',[AppointmentController::Class,'deleteAppointment']);
Route::put('/update',[AppointmentController::Class,'updateAppointment']);
Route::get('/Appointment', [App\Http\Controllers\API\AppointmentController::Class,'getAppointment']);
///////////////////////////////piece
Route::get('/categories', [App\Http\Controllers\API\HomeController::Class,'getCategory']);

Route::post('/clothe', [App\Http\Controllers\API\PieceController::Class,'show_all_piece']);

//////////////////////////home
Route::get('/banners', [App\Http\Controllers\API\HomeController::Class,'getbanner']);
Route::get('/home', [App\Http\Controllers\API\HomeController::Class,'getHomeData']);

//////////////////////////favorites
Route::get('/favorites', [App\Http\Controllers\API\PieceController::Class,'getfavorite']);
Route::post('/addfavorites', [App\Http\Controllers\API\PieceController::Class,'addfavorite']);
// Route::delete('/delete', [App\Http\Controllers\API\ClotheController::Class,'deletefavorite']);


// API route for logout user
 Route::post('/logout', [App\Http\Controllers\API\AuthController::class, 'logout']);


//////////////////////////////notification
Route::post('/save-push-notification-token', [App\Http\Controllers\API\NotifyController::class, 'savePushNotificationToken'])->name('save-push-notification-token');
Route::get('/notification', [App\Http\Controllers\API\NotifyController::Class,'getnotify']);
 Route::delete('/delete', [App\Http\Controllers\API\NotifyController::Class,'delete']);


});
////////////////////////email
Route::post('email/verification-notification', [EmailVerificationController::class, 'sendVerificationEmail'])->middleware('auth:sanctum');
Route::post('verify-email', [EmailVerificationController::class, 'verify'])->name('verification.verify')->middleware('auth:sanctum');

Route::post('forgot-password', [NewPasswordController::class, 'forgotPassword']);
Route::post('reset-password', [NewPasswordController::class, 'reset']);
Route::get('/notification', [App\Http\Controllers\API\NotifyController::Class,'getnotify']);