<?php

Route::resource('invoiceLine', 'App\Http\Controllers\InvoiceLineController');
Route::get('/create_invoiceLine_with_id/{invoice_id}','App\Http\Controllers\InvoiceLineController@create_invoiceLine_with_id');
Route::get('/display_invoiceLine/{invoice_id}','App\Http\Controllers\InvoiceLineController@display_invoiceLine');
Route::get('/in/{invoice_id}','App\Http\Controllers\InvoiceLineController@index');