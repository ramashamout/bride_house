<?php

Route::resource('appointment', 'App\Http\Controllers\AppointmentController');
Route::get('/ap','App\Http\Controllers\AppointmentController@index')->middleware('auth');
Route::get('/all_app','App\Http\Controllers\AppointmentController@all')->middleware('auth');
Route::get('/markAsRead/{id}','App\Http\Controllers\AppointmentController@mark_as_read')->name('markAsRead');
Route::get('/Unfixed_appointments','App\Http\Controllers\AppointmentController@Unfixed_appointments');
Route::get('/app_s/{id}','App\Http\Controllers\AppointmentController@save_s')->name('app');
Route::get('/update_statue_Installations/{id}','App\Http\Controllers\AppointmentController@update_statue_Installations')->name('update_statue_Installations/{id}');

Route::get('/update_statue_delete/{id}','App\Http\Controllers\AppointmentController@update_statue_delete')->name('update_statue_delete/{id}');
