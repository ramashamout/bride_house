<?php

Route::resource('clothe', 'App\Http\Controllers\ClotheController');
Route::get('/index','App\Http\Controllers\Filter@index');
Route::get('/filter/{name}','App\Http\Controllers\Filter@show')->name('filter');
Route::post('/filt','App\Http\Controllers\Filter@filt')->name('filt');
Route::get('/search_form',function (){
    return view('clothe.search_form');
});
Route::post('/search_piece','App\Http\Controllers\ClotheController@search_piece')->name('search_piece');

Route::get('/add_dress','App\Http\Controllers\ClotheController@create')->name('add_dress');
Route::get('/check','App\Http\Controllers\ClotheController@check');
Route::get('/product/{id}','App\Http\Controllers\ClotheController@display');
Route::get('master','App\Http\Controllers\Filter@index');
Route::get('/gallery','App\Http\Controllers\Filter@display');
Route::get('/search_piece','App\Http\Controllers\Filter@search');

Route::get('/update_statue_c/{id}','App\Http\Controllers\ClotheController@update_statue_c')->name('/update_statue_c/{id}');
Route::get('master','App\Http\Controllers\ClotheController@master');
