<?php

use Illuminate\Support\Facades\Route;

Route::resource('pieceReminder', 'App\Http\Controllers\PiecesReminderController');

Route::get('/pieceNotification','App\Http\Controllers\PiecesReminderController@piece_notification')->name('pieceNotification');
Route::get('/markAsRead/{id}','App\Http\Controllers\PiecesReminderController@mark_as_read')->name('markAsRead');
