<?php
use Illuminate\Support\Facades\Route;

Route::resource('brideReminder', 'App\Http\Controllers\BridesRemindersController');
Route::get('/brideNotification','App\Http\Controllers\BridesRemindersController@bride_notification');
Route::get('/markAsRead/{id}','App\Http\Controllers\BridesRemindersController@mark_as_read')->name('markAsRead');
Route::get('/notification','App\Http\Controllers\BridesRemindersController@bride');
