<?php

Route::resource('receipt', 'App\Http\Controllers\ReceiptReceivedController');
Route::get('/create_receipt_with_id/{contract_id}','App\Http\Controllers\ReceiptReceivedController@create_receipt_with_id');
Route::get('/display_receipt/{contract_id}','App\Http\Controllers\ReceiptReceivedController@display_receipt');
