<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','App\Http\Controllers\HomeController@home' );

Route::middleware(['auth:sanctum', 'verified'])->get('/dashboard','App\Http\Controllers\HomeController@home' );
Route::group(['middleware' => ['auth']], function() {
   Route::get('/send-push-notification', [App\Http\Controllers\API\NotifyController::class, 'sendPushNotification'])->name('send.push-notification');
   Route::post('/sendnotifyappointment/{id}', ['as' => 'sendnotifyappointment' , 'uses' => 'App\Http\Controllers\API\NotifyController@sendnotifyappointment']);
Route::post('/sendnotifyappointmentCancel/{id}', ['as' => 'sendnotifyappointmentCancel' , 'uses' => 'App\Http\Controllers\API\NotifyController@sendnotifyappointmentCancel']);
   
    Route::resource('users', UserController::class);
    Route::get('/block/{id}', 'App\Http\Controllers\UserController@block')->name('block');
Route::get('/unblock/{id}', 'App\Http\Controllers\UserController@unblock')->name('unblock');
 });
 Route::resource('roles', RoleController::class);
Route::get('/ex','App\Http\Controllers\Store_ClotheController@v' );
Route::get('/gotomazad/{id}','App\Http\Controllers\HomeController@gotomazad')->name('gotomazad');

