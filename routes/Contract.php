<?php

Route::resource('contract', 'App\Http\Controllers\ContractController');
Route::get('/contract', 'App\Http\Controllers\ContractController@create');
Route::get('/cont/{id}', 'App\Http\Controllers\ContractController@edit');

Route::post('/filter_payment','App\Http\Controllers\Filter@filter_payment')->name('filter_payment');
Route::get('/contract/update_color/{id}','App\Http\Controllers\ContractController@update_color')->name('contract/update_color/{id}');

Route::get('/markConAsRead/{id}','App\Http\Controllers\ContractController@mark_as_read')->name('markConAsRead');
Route::get('/show_information/{id}','App\Http\Controllers\ContractController@show_information')->name('show_information');
