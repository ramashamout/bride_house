<?php


Route::resource('accessory', 'App\Http\Controllers\AccessoryController');

Route::get('Accessory', 'App\Http\Controllers\AccessoryController@create');
Route::get('/crowns/{name}', 'App\Http\Controllers\AccessoryController@crowns');
Route::post('/update_user/{id}','App\Http\Controllers\UserController@update_personal_information')->name(
    '/update_user/{id}');
Route::get('/update_statue_a/{id}','App\Http\Controllers\AccessoryController@update_statue_a');

