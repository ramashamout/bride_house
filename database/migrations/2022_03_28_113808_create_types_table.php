Connection('mysql')-><?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */


    public $incrementing = false;


    public function up()
    {
        Schema::Connection('mysql2')->create('types', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('image')->nullable();
             $table->string('type_piece');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('types');
    }
}
