<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContractsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Connection('mysql2')->create('contracts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('customer_id');
            $table->integer('insurance_price');
            $table->integer('total_price');
            $table->integer('deposit');
            $table->integer('first_batch');
            $table->integer('second_batch');
            $table->string('hall');
            $table->date('start');
            $table->date('end')->nullable();
            $table->date('contract_date');
            $table->string('color');
            $table->string('title');
            $table->integer('piece_id');
            $table->date('event_date');
            $table->string('contract_type');//white coloder child
            $table->integer('acc_id');
            $table->boolean('is_check');
            $table->boolean('is_check1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contracts');
    }
}
