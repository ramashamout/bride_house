<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCleaningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Connection('mysql2')->create('cleanings', function (Blueprint $table) {
             $table->bigIncrements('id');
             $table->string('name_product');
            $table->string('laundry');
            $table->integer('piece_id');
            $table->string('color');
            $table->string('cleaning_price');
            $table->string('title');
            $table->date('end');
            $table->date('start');
             $table->boolean('is_check1');
              $table->boolean('is_check');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('cleanings');
    }
}
