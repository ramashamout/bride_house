<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBodyMeasurementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Connection('mysql2')->create('body__measurements', function (Blueprint $table) {
             $table->bigIncrements('id');
           $table->integer('design_id');
             $table->string('chest_circumference');
            $table->string('waistline');
            $table->string('hip_circumference');
            $table->string('shoulder_width');
            $table->string('shoulder_length');
            $table->string('back_length');
            $table->string('breast_length');
            $table->string('distance_breasts');
            $table->string('Sleeve_Length')->nullable();;
            $table->string('cuff_bracelet')->nullable();;
            $table->string('skirt_length')->nullable();;
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('body__measurements');
    }
}
