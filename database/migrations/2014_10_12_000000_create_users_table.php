<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->bigInteger('mobile_number')->unique()->nullable();
             $table->enum('type', ['admin', 'employ', 'user']);
            $table->rememberToken();
            $table->string('profile_photo_path', 2048)->nullable();
             $table->string('image_path')->nullable();
             $table->string('device_token')->nullable(); 
             $table->integer('status')->default(1);
             $table->integer('status_bride')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //      DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        // Schema::drop('users');
        // DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        Schema::dropIfExists('users');
    }
};
