<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePiecesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Connection('mysql2')->create('pieces', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->string('name');
            $table->string('piece_img');
            $table->double('selling_price');
            $table->double('old_price');
            $table->double('discount')->default(0);
            $table->double('rent_price');
            $table->boolean('inFavorites')->default(0);
            $table->string('designer_name')->nullable();
            $table->string('color')->nullable();
            $table->bigInteger('size')->nullable();
            $table->string('type');
            $table->string('statue')->nullable();
            $table->string('statue_acc')->nullable();
            $table->string('clothes_type')->nullable();
            $table->string('category')->nullable();
            $table->string('Type_accessory')->nullable();
            $table->string('other_image')->nullable();

            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pieces');
    }
}
