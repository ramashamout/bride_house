<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMaintenanceNotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Connection('mysql2')->create('maintenance__notes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('piece_id');
            $table->string('name_product');
             $table->string('name_customer')->nullable();
             $table->string('note_content');
             $table->date('start');
             $table->integer('customer_id')->nullable();
             $table->date('end');
              $table->boolean('is_check1');
              $table->boolean('is_check');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('maintenance__notes');
    }
}
