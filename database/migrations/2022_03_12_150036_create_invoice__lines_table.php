<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvoiceLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Connection('mysql2')->create('invoice__lines', function (Blueprint $table) {
            $table->bigIncrements('id');
             $table->integer('piece_id');
             $table->integer('invoice_id');
            $table->longText('description');
            $table->integer('number_of_pieces');
            $table->integer('unit_price');
            $table->integer('total_price');
            $table->timestamps();
        });
    }


    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invoice__lines');
    }
}
