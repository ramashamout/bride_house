<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStoreClothesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Connection('mysql2')->create('store__clothes', function (Blueprint $table) {
            $table->id();
            $table->integer('piece_id');
            $table->integer('number_of_pieces');
//            $table->integer('counter');

//             $table->string('name')->nullable();
//            $table->string('piece_img')->nullable();
//            $table->double('selling_price')->nullable();
//            $table->double('old_price')->nullable();
//            $table->double('discount')->default(0);
//            $table->double('rent_price')->nullable();
//            $table->string('designer_name')->nullable();
//            $table->string('color')->nullable();
//            $table->bigInteger('size')->nullable();
//            $table->string('type')->nullable();
//            $table->string('clothes_type')->nullable();
//            $table->string('other_image')->nullable();
//            $table->string('category')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('store__clothes');
    }
}
