<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAppointmentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Connection('mysql2')->create('appointments', function (Blueprint $table) {
             $table->bigIncrements('id');
            $table->date('date');
            $table->time('time');
            $table->string('title');
            $table->integer('mobile_number');
            $table->string('color');
            $table->string('type');
            $table->dateTime('start');
             $table->enum('statue_app', ['قيد الانتظار ', 'مثبت ', 'ملغى']);
            $table->dateTime('end');
            $table->integer('read')->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('appointments');
    }
}
