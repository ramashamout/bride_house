<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCalendarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Connection('mysql2')->create('calendars', function (Blueprint $table) {
            $table->id();
               $table->string('title');
            $table->string('color');
            $table->dateTime('start');
            $table->integer('piece_id');
             $table->boolean('is_check');
            $table->dateTime('end');
            $table->boolean('is_check1');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('calendars');
    }
}
