<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDesignsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::Connection('mysql2')->create('designs', function (Blueprint $table) {
              $table->bigIncrements('id');
            $table->integer('customer_id');
            $table->string('color');
            $table->string('fabric_type');
            $table->string('model');
            $table->integer('general_size');
            $table->longText('design_notes');
            $table->date('delivery_date');
            $table->date('received_date');
            $table->integer('designing_price');
            $table->string('type');//white coloder child
            $table->string('design_img');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('designs');
    }
}
