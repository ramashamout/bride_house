<?php

namespace Database\Seeders;
use Illuminate\Database\Seeder;
use App\Models\User;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
  
class CreateAdminUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
     $user = User::create([
            'name' => 'Admin', 
            'email' => 'admin@admin.com',
            'password' => bcrypt('123456789'),
            'type'=>'admin',
            'image_path'=>'images/33.png',
        ]);
    
        $role = Role::on('mysql2')->create(['name' => 'مدير']);
     
        $permissions = Permission::on('mysql2')->pluck('id','id')->all();
   
        $role->syncPermissions($permissions);


        $user->syncPermissions($permissions);
     
        $user->assignRole([$role->id]);
    }
}
