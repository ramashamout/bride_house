<?php
  
namespace Database\Seeders;
  
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
  
class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = [
           
           'إدارة الفواتير ',
           'إدارة التقارير ',
           'إدارة المستخدمين',
           'إدارة الصلاحيات ',
           'إدارة الملحقات ',
           'إدارة الفساتين',
           'التنظيف' ,
           'التصميم',
           'الصيانة',
           'العقود',
           'التذكيرات' ,
           'إدارة المواعيد ',
           'إدارة المستودع ',
           'إدارة الأنواع ',
           'إدارة التطبيق ',

        ];
     
        foreach ($permissions as $permission) {
             Permission::on('mysql2')->create(['name' => $permission]);
        }
    }
}