<?php
  
namespace Database\Seeders;
  
use Illuminate\Database\Seeder;
use App\Models\size_general;
 use App\Models\time; 
  use App\Models\Range; 
class SizeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $size_generals = [
          '38',
          '40',
          '42',
          '44',
          '46',
          '48',
          '50',
          '52'


        ];
        $time = time::create([
            'start_time' => '12:00:00', 
            'end_time' => '20:00:00',
        
        ]);
        Range::create([
            'from' => '500000', 
            'to' => '1000000',
            'name'=>'منخفض' 
        
        ]);
     Range::create([
            'from' => '1000000', 
            'to' => '2000000',
            'name'=>'متوسط ' 
        
        ]);
     Range::create([
            'from' => '2000000', 
            'to' => '4000000',
            'name'=>'مرتفع ' 
        
        ]);
     
        foreach ($size_generals as $size_general) {
             size_general::create(['value' => $size_general]);
        }
    }
}