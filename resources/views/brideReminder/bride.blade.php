@extends('base')
@section('main')
<div class="row">
    <div class="col-sm-12">
        <h1 class="display-3">bride Notification</h1>
        <div>
            <a href="{{ route('brideReminder.create')}}" class="btn btn-primary mb-3">Add bride Notification</a>
        </div>

        @if(session()->get('success'))
        <div class="alert alert-success">
            {{ session()->get('success') }}
        </div>
        @endif
        <table class="table table-striped">
            <thead>
                <tr>
                    <td>ID</td>
                    <td> content</td>
                    <td> date</td>
                    <td>Mobile Number</td>
                    <td>emboridery</td>
                </tr>
            </thead>
            <tbody>
                <h1>Bride </h1>
                @foreach($user->unreadNotifications as $brideReminder)
                @if($brideReminder->data['type'] =="bride")
                <tr>
                    <td>{{$brideReminder->data['id']}}</td>
                    <td>{{$brideReminder->data['content']}} </td>
                    <td>{{$brideReminder->data['date']}}</td>
                    <td>{{$brideReminder->data['mobile_number']}}</td>
                    <td>{{$brideReminder->data['emboridery']}}</td>
                    <td>
                        <a href="{{ route('markAsRead',$brideReminder->id)}}" class="btn btn-primary">mark as read</a>
                    </td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
        <div>
        </div>
    </div>
</div>
@endsection