@extends('master') @section('contents')
<div class="wrapper style3 ">
  <div class="title ">
    <h2>
      <a href="index.html">التذكيرات</a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">تذكير BrideRobe</a>
          </li>
          <li>
            <a data-toggle="tab" href="#status" class="contact-map"> تذكير فستان </a>
          </li>
          <li>
            <a data-toggle="tab" href="#edit">جميع التذكيرات </a>
          </li>

        </ul>
      </div>
      <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information customer -->
          <div id="overview" class="tab-pane active">
            <div class="row">
              <div class="  col-md-8 col-md-offset-2 ">
                <form role="form" class="form-horizontal" method="post" action="{{ route('brideReminder.store') }}">
                  @csrf
                  <br>
                  <!-- start name -->
                  <div class="form-group">
                    <label class="activity-panel control-label" style="margin-right:180px; ">الاسم الثلاثي </label>
                    <div class="col-lg-6 col-md-6 col-lg-offset-1">
                      <input type="text" placeholder=" " id="name" class="form-control " name="content">
                    </div>
                  </div>
                  <!-- end name -->
                  <!-- start phone -->
                  <div class="form-group">
                    <label class="activity-panel control-label" style="margin-right:180px; ">رقم الهاتف</label>
                    <div class="col-lg-6 col-md-6 col-lg-offset-1">
                      <input type="text" placeholder=" " id="phone" class="form-control " name="mobile_number">
                    </div>
                  </div>
                  <!-- end phone -->

                  <!-- start notification  date-->
                  <div class="form-group">
                    <label class="activity-panel control-label" style="margin-right:180px; "> تاريخ التذكير</label>
                    <div class="col-lg-6 col-md-6 col-lg-offset-1">
                      <input type="date" placeholder=" " id="addres" class="form-control " name="date" value="">
                    </div>
                  </div>
                  <!-- end notification  date-->
                  <!-- start notification content -->
                  <div class="form-group">
                    <label class="activity-panel control-label" style="margin-right:180px; ">التطريز</label>
                    <div class="col-lg-6 col-md-6 col-lg-offset-1">
                      <input type="text" placeholder=" " id="addres" class="form-control " name="emboridery" value="">
                    </div>
                  </div>
                  <!-- end notification content -->

                  <!-- update button -->
                  <div class="form-group">
                    <div class=" pull-left ">
                      <button class="btn  btn-theme02" type="submit">حفظ </button>
                    </div>
                  </div>
                  <!-- update button -->
                  <div>
                    @if ($bride_errors!=null)
                    <div class="alert alert-danger">
                      <ul>
                        <li>
                          {{ $bride_errors }}
                        </li>
                      </ul>
                    </div><br />
                    @endif
                  </div>
                </form>
              </div>
              <!-- start image customer-->
              <div class="col-md-1">

                <img src="images/5.jpg" id="image">

              </div>
              <!-- end image customer-->
            </div>
          </div>
          <!-- end information customer -->

          <!-- start status dress -->
          <div id="status" class="tab-pane">
            <div class="row">
              <div class="  col-md-8 col-md-offset-2 ">
                <form role="form" class="form-horizontal" method="post" action="{{ route('pieceReminder.store') }}">
                  @csrf
                  <br>
                  <!-- start name -->
                  <div class="form-group">
                    <label class="activity-panel control-label" style="margin-right:180px; ">الاسم الثلاثي </label>
                    <div class="col-lg-6 col-md-6 col-lg-offset-1">
                      <input type="text" placeholder=" " id="name" class="form-control " name="customer_name">
                    </div>
                  </div>
                  <!-- end name -->
                  <!-- start phone -->
                  <div class="form-group">
                    <label class="activity-panel control-label" style="margin-right:180px; ">رقم الهاتف</label>
                    <div class="col-lg-6 col-md-6 col-lg-offset-1">
                      <input type="text" placeholder=" " id="phone" class="form-control " name="mobile_number">
                    </div>
                  </div>
                  <!-- end phone -->

                  <!-- start notification  date-->
                  <div class="form-group">
                    <label class="activity-panel control-label" style="margin-right:180px; "> تاريخ التذكير</label>
                    <div class="col-lg-6 col-md-6 col-lg-offset-1">
                      <input type="date" placeholder=" " id="addres" class="form-control " name="date" value="">
                    </div>
                  </div>
                  <!-- end notification  date-->
                  <!-- start notification content -->
                  <div class="form-group">
                    <label class="activity-panel control-label" style="margin-right:180px; ">التذكير</label>
                    <div class="col-lg-6 col-md-6 col-lg-offset-1">
                      <input type="text" placeholder=" " id="addres" class="form-control " name="content">
                    </div>
                  </div>
                  <!-- end notification content -->
                  <!-- update button -->
                  <div class="form-group">
                    <div class=" pull-left ">
                      <button class="btn  btn-theme02" type="submit">حفظ </button>
                    </div>
                  </div>
                  <!-- update button -->
                  <div>
                    @if ($piece_errors!=null)
                    <div class="alert alert-danger">
                      <ul>
                        <li>
                          {{ $piece_errors }}
                        </li>
                      </ul>
                    </div><br />
                    @endif
                  </div>
                </form>
              </div>
              <!-- start image customer-->
              <div class="col-md-1">

                <img src="images/9.jpg" id="image">

              </div>
              <!-- end image customer-->
            </div>
          </div>
          <!-- end status dress -->
          <!-- start notes maintenace -->
          <div id="edit" class="tab-pane">
            <div class="row mt">
              <div class="col-md-12">
                <!-- start section notes to one dress-->
                <section class="task-panel tasks-widget ">
                  <div class="panel-heading">
                    <div class="pull-right">
                      <h5 style="padding: 8px 30px;
                          background: #e0e1e7;color: #5a5a5a;">
                        <i class="fa fa-bell"></i> تذكيرات الفساتين
                      </h5>
                    </div>

                    <!-- start buttom show all notes -->
                    <p>
                      <a class="btn btn-theme02 btn-sm  pull-left" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fa fa-eye "></i> عرض التذكيرات </a>
                    </p>
                    <!-- end buttom show all notes -->
                    <!-- start collapse show all notes -->
                    <div class="collapse" id="collapseExample">
                      <div class="panel-body">
                        <!-- start all notes to one dress -->
                        <div class="task-content">
                          <ul class="task-list">
                            <br>
                            <!-- start note to one dress -->
                            @foreach($pieceReminder as $pieceReminder)
                            <li>
                              <div class="task-title">
                                <span class="task-title-sp">
                                  <i class="fa fa-circle" style="font-size:10px"></i>&nbsp;{{$pieceReminder->customer_name}}&nbsp; <a href="#" id="mytooltip" data-toggle="tooltip" title="{{$pieceReminder->date}}" data-placement="left">
                                    <i class="fa fa-calendar " style="font-size:15px ; color:black; "></i>
                                  </a>
                                </span>
                                <p> المحتوى : {{$pieceReminder->content}}</p>
                                <p> رقم الهاتف : {{$pieceReminder->mobile_number}}</p>
                                <div class="pull-left hidden-phone">

                                  <!-- end madal edit notes -->
                                  <div class="col-lg-8 col-md-8">
                                    <form action="{{ route('pieceReminder.destroy', $pieceReminder->id)}}" method="post">
                                      @csrf
                                      @method('DELETE')
                                      <button class="btn btn-danger btn-xs " class="button" method="post" href="{{ route('pieceReminder.destroy', $pieceReminder->id)}}" style="margin-right:-5px; ">
                                        <i class="fa fa-trash-o "></i>
                                      </button>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </li>
                            @endforeach
                            <!-- end note to one dress -->
                          </ul>
                        </div>
                        <!-- end all notes to one dress  -->
                      </div>
                    </div>
                    <!-- end collapse show all notes -->

                    <br>
                  </div>
                </section>
                <!-- end section notes to one dress-->
                <!-- start section notes to one dress-->
                <section class="task-panel tasks-widget ">
                  <div class="panel-heading">
                    <div class="pull-right">
                      <h5 style="padding: 8px 30px;
                          background: #e0e1e7;color: #5a5a5a;">
                        <i class="fa fa-bell"></i> تذكيرات BrideRobe
                      </h5>
                    </div>

                    <!-- start buttom show all notes -->
                    <p>
                      <a class="btn btn-theme02 btn-sm  pull-left" data-toggle="collapse" href="#collapse" role="button" aria-expanded="false" aria-controls="collapseExample">
                        <i class="fa fa-eye "></i> عرض التذكيرات </a>
                    </p>
                    <!-- end buttom show all notes -->
                    <!-- start collapse show all notes -->
                    <div class="collapse" id="collapse">
                      <div class="panel-body">
                        <!-- start all notes to one dress -->
                        <div class="task-content">
                          <ul class="task-list">
                            <br>
                            <!-- start note to one dress -->
                            @foreach($brideReminder as $brideReminder)
                            <li>
                              <div class="task-title">
                                <span class="task-title-sp">
                                  <i class="fa fa-circle" style="font-size:10px"></i>&nbsp;{{$brideReminder->customer_name}}&nbsp; <a href="#" id="mytooltip" data-toggle="tooltip" title="{{$brideReminder->date}}" data-placement="left">
                                    <i class="fa fa-calendar " style="font-size:15px ; color:black; "></i>
                                  </a>
                                </span>
                                <p>&nbsp;رقم الهاتف : {{$brideReminder->mobile_number}}</p>
                                <p> التطريز : {{$brideReminder->emboridery}} </p>
                                <div class="pull-left hidden-phone">

                                  <!-- end madal edit notes -->
                                  <div class="col-lg-8 col-md-8">
                                    <form action="{{ route('brideReminder.destroy', $brideReminder->id)}}" method="post">
                                      @csrf
                                      @method('DELETE')
                                      <button class="btn btn-danger btn-xs " class="button" method="post" href="{{ route('brideReminder.destroy', $brideReminder->id)}}" style="margin-right:-5px; ">
                                        <i class="fa fa-trash-o "></i>
                                      </button>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </li>
                            @endforeach
                            {{-- <p>تذكيرات العقود</p>--}}
                            {{-- @foreach(auth()->user()->unreadNotifications as $contractReminder)--}}
                            {{-- @if($contractReminder->data['type'] =="contract")--}}
                            {{-- <li>--}}
                            {{-- <div class="task-title">--}}
                            {{-- <span class="task-title-sp">--}}
                            {{-- <i class="fa fa-circle" style="font-size:10px"></i>&nbsp;{{$contractReminder->data['contract']['id']}}&nbsp; <a href="#" id="mytooltip" data-toggle="tooltip" title="{{$contractReminder->data['contract']['end']}}" data-placement="left">--}}
                              {{-- <i class="fa fa-calendar " style="font-size:15px ; color:black; "></i>--}}
                              {{-- </a>--}}
                              {{-- </span>--}}
                              {{-- <p>customer_id {{$contractReminder->data['contract']['customer_id']}}</p>--}}
                              {{-- <p>insurance_price {{$contractReminder->data['contract']['insurance_price']}}</p>--}}
                              {{-- <p>total_price  {{$contractReminder->data['contract']['total_price']}}</p>--}}
                              {{-- <p>deposit   {{$contractReminder->data['contract']['deposit']}}</p>--}}
                              {{-- <p>first_batch   {{$contractReminder->data['contract']['first_batch']}}</p>--}}
                              {{-- <p>second_batch   {{$contractReminder->data['contract']['second_batch']}}</p>--}}
                              {{-- <p>hall  {{$contractReminder->data['contract']['hall']}}</p>--}}
                              {{-- <p>start  {{$contractReminder->data['contract']['start']}}</p>--}}
                              {{-- <p>end  {{$contractReminder->data['contract']['end']}}</p>--}}
                              {{-- <p>contract_date  {{$contractReminder->data['contract']['contract_date']}}</p>--}}
                              {{-- <p>color   {{$contractReminder->data['contract']['color']}}</p>--}}
                              {{-- <p>title   {{$contractReminder->data['contract']['title']}}</p>--}}
                              {{-- <p>event_date   {{$contractReminder->data['contract']['event_date']}}</p>--}}
                              {{-- @foreach(\App\Models\Contract::find($contractReminder->data['contract']['id'])->piece_contracts as $c)--}}
                              {{-- @if($c->clothe !=null)--}}
                              {{-- <p>piece   {{$c->piece->name}}</p>--}}
                              {{-- @endif--}}
                              {{-- @if($c->accessory !=null)--}}
                              {{-- <p>accessory   {{$c->piece->name}}</p>--}}
                              {{-- @endif--}}
                              {{-- @endforeach--}}
                              {{-- <div class="pull-left hidden-phone">--}}

                              {{-- <!-- end madal edit notes -->--}}
                              {{-- <div class="col-lg-8 col-md-8">--}}
                              {{-- <button class="btn btn-danger btn-xs " class="button"--}}
                              {{-- method="post" href="{{ route('markAsRead',$contractReminder->id)}}" style="margin-right:-5px; ">--}}
                              {{-- <i class="fa fa-trash-o "></i>--}}
                              {{-- </button>--}}
                              {{-- <a href="{{ route('markConAsRead',$contractReminder->id)}}" class="btn btn-primary"><i class="fa fa-trash-o "></i></a>--}}
                            {{-- </div>--}}
                            {{-- </div>--}}
                            {{-- </div>--}}
                            {{-- </li>--}}
                            {{-- @endif--}}
                            {{-- @endforeach--}}
                            <!-- end note to one dress -->
                          </ul>
                        </div>
                        <!-- end all notes to one dress  -->
                      </div>
                    </div>
                    <!-- end collapse show all notes -->

                    <br>
                  </div>
                </section>
                <!-- end section notes to one dress-->
              </div>
            </div>
          </div>
          <!-- end notes maintenace -->
        </div>
      </div>
      <!-- end panel-body -->
    </div>
  </div>
</div>
@endsection