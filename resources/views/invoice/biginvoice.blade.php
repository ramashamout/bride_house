@extends('master') @section('contents') <div class="wrapper style3 ">
  <div class="title ">
    <h2>
      <a href="index.html">الفواتير الأساسية </a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">
              <i class="fa fa-edit" style="color: black;"></i>معلومات الفاتورة </a>
          </li>
          <li>
            <a data-toggle="tab" href="#edit">جميع الفواتير الأساسية </a>
          </li>
        </ul>
      </div>
      <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information cotract -->
          <div id="overview" class="tab-pane active">
            <form method="POST" action="{{ route('invoice.store') }}" enctype="multipart/form-data">
              @csrf

              <div class="row  text-center">

                <div class="col-md-8 col-lg-8 col-md-offset-2">
                  <img src="/images/invoice.jpg" id="image3">
                  <div class="form-group " style="width:300px;margin-right: 220px;">
                    <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" placeholder="تاريخ الفاتورة " class="form-control" name="invoice_date" />
                  </div>


                </div>
                <!-- start image cleaning-->

                <!-- end image customer-->
                <div class=" col-md-4 col-lg-4 " style="margin-left: 40px;margin-bottom: 20px;">
                  <button type="submit" class="btn btn-theme04 pull-left">حفظ</button>
                </div>
              </div>
              @if($errors->any())
                    <div class="alert alert-danger">
                        <ul>
                            <li>
                                {{$errors->first()}}
                            </li>
                        </ul>
                    </div><br />
                @endif
            </form>
          </div>
          <!-- end information contract -->
          <!-- start all contract -->
          <div id="edit" class="tab-pane">
            <div class="row mt">
              <div class="col-md-12">
                <section class="task-panel tasks-widget">
                  <div class="panel-body">
                    <div class="task-content">
                      <ul class="task-list">
                        @foreach($invoice as $invoice)
                        <li>

                          <div class="task-title">
                            <span class="task-title-sp">
                              <i class="fa fa-circle" style="font-size:10px"></i>&nbsp;&nbsp; {{$invoice->invoice_date}} <a href="#" id="mytooltip" data-toggle="tooltip" title="" data-placement="left">

                              </a>
                            </span>
                            <div class="pull-left hidden-phone">
                              
                              <div class="pull-left" style="margin-right: 3px;">
                                <form action="{{ route('invoice.destroy', $invoice->id)}}" method="post">
                                  @csrf
                                  @method('DELETE')
                                  <button class="btn btn-danger btn-xs " class="button" method="post" href="{{ route('invoice.destroy', $invoice->id)}}">
                                    <i class="fa fa-trash-o "></i>
                                  </button>
                                </form>
                              </div>


                              <a href="/in/{{$invoice->id}}" class="btn btn-primary btn-xs"><i class="fa fa-eye"></i></a>



                            </div>
                          </div>
                        </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
          <!-- end all contract -->
        </div>
      </div>
      <!-- end panel-body -->
    </div>
  </div>
</div>
@endsection