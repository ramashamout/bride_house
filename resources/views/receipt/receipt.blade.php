@extends('master') @section('contents') 
<div class="wrapper style3 ">
  <div class="title ">
    <h2>
      <a href="index.html">وصولة القبض</a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">
              <i class="fa fa-edit" style="color: black;"></i>وصولة القبض</a>
          </li>
          <li>
            <a data-toggle="tab" href="#edit">جميع وصولة القبض</a>
          </li>
        </ul>
      </div>
       
       <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information cotract -->
          <div id="overview" class="tab-pane active">
            <form style="padding-top: 30px;" method="post" action="{{ route('receipt.store') }}">
                    @csrf
              <div class="row mt">
                <div class="col-md-6 col-lg-6 col-md-offset-2">
                
                 
                  <div class="form-group">
                    <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" placeholder="كمية القطع "  name="amount">
                  </div>
                  <div class="form-group ">
                    <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" placeholder="الوصف "  name="description">
                  </div>
                 
                
                   <div class="form-group ">
                    <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'"
                    placeholder="تاريخ القبض" class="form-control" name="receipt_date"/>
                  </div>
                  <div>
                        <input type="hidden" name="contract_id"  value= {{$contract_id}} >
                    </div>
                  
                </div>
                <!-- start image cleaning-->
                <div class="col-md-3 col-lg-3">
                  <img src="/images/31.png" id="image">
                </div>
                <!-- end image customer-->
                <div class=" col-md-4 col-lg-4 ">
                  <button type="submit" class="btn btn-theme02 pull-left" style="margin-left: 50px;margin-top: -80px;">حفظ</button>
                </div>
              </div>
            </form>
          </div>
          <!-- end information contract -->
          <!-- start all contract -->
          <div id="edit" class="tab-pane">
            <div class="row mb">
              <div class="col-lg-12 col-md-12">
                <!-- page start-->
                <div class="content-panel">
                  <div class="adv-table">
                    <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="example">
                      <thead>
                        <tr>
                          <th>كمية القطع </th>
                          <th>الوصف</th>
                          <th class="hidden-phone">تاريخ القبض</th>
                         
                          <th class="hidden-phone"></th>
                        </tr>
                      </thead>
                      <tbody>
                         @foreach($receipt  as $receipt)
                        <tr class="gradeU">
                        <td>{{$receipt->amount}} </td>
                        <td>{{$receipt->description}}</td>
                        <td>{{$receipt->receipt_date}}</td>
                          <td class="center hidden-phone">
                             <div class="col-lg-8 col-md-8">
                                <form action="{{ route('receipt.destroy', $receipt->id)}}" method="post">
                                  @csrf
                                  @method('DELETE')
                            <button class="btn btn-danger btn-xs" action="{{ route('receipt.destroy', $receipt->id)}}" method="post">
                              <i class="fa fa-trash-o "></i>
                            </button>
                             </form>
                              </div>
                            <a href="{{ route('receipt.edit',$receipt->id)}}" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#basicModal{{$receipt->id}}">
                                <i class=" fa fa-pencil"></i>
                              </a>
                              <!-- start madal edit notes -->
                              <div class="modal fade" id="basicModal{{$receipt->id}}" tabindex="-1" role="  dialog" aria-labelledby="basicModal{{$receipt->id}}" aria-hidden="true">
                                <div class="modal-dialog ">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title centered" id="myModalLabel">تعديل وصولة القبض </h4>
                                    </div>
                                    <form method="post" action="{{ route('receipt.update', $receipt->id) }}" enctype="multipart/form-data"  >
                                       @method('PATCH')
                                       @csrf
                                     
                                      <div class="modal-body">
                                        <div class="container-fluid">
                                          <div class="row ">
                                            <div class="col-md-12 col-lg-12">
                                             
                                            
                                              <div class="form-group">
                                                <label for="inputPassword6"style="position: relative;left: 220px;">كمية القطع :</label>
                                                <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="amount" value="{{$receipt->amount}}">
                                              </div>
                                              <div class="form-group ">
                                                <label for="inputPassword6" style="position: relative;left: 240px;">الوصف :</label>
                                                <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="description" value="{{$receipt->description}}">
                                              </div>
                                              <div class="form-group ">
                                                <label for="inputPassword6" style="position: relative;left: 210px;">تاريخ وصل القبض:</label>
                                                <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'"
                                                class="form-control" name="receipt_date" value="{{$receipt->receipt_date}}" />
                                              </div>
                                          
                                              
                                              
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer ">
                                        <button type="submit" class="btn btn-theme02 pull-left" data-dismiss="modal{{$receipt->id}}">تعديل </button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                          </td>
                        </tr>
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- page end-->
              </div>
            </div>
            <!-- /row -->
          </div>
          <!-- end all contract -->
        </div>
      </div>
      <!-- end panel-body -->

    </div>
  </div>
</div>
@endsection