@extends('master') @section('contents') <div class="wrapper style3 ">
  <div class="title ">
    <h2>
      <a href="index.html">إدارة المستخدمين</a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">
              <i class="fa fa-user" style="color: black;"></i> إضافة مستخدم </a>
          </li>
          <li>
            <a data-toggle="tab" href="#edit">الموظفين</a>
          </li>
          <li>
            <a data-toggle="tab" href="#all">مستخدمي التطبيق</a>
          </li>
        </ul>
      </div>
      <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information cotract -->
          <div id="overview" class="tab-pane active">
            <form style="padding-top: 30px;" method="POST" action="{{ route('users.store') }}" enctype="multipart/form-data">
              @csrf
              <div class="row mt">
                <div class="col-md-6 col-lg-6 col-md-offset-2">



                  <div class="form-group">
                    {!! Form::text('name', null, array('placeholder' => 'الاسم','class' => 'form-control mx-sm-3')) !!}
                  </div>

                  <div class="form-group">

                    {!! Form::text('email', null, array('placeholder' => 'الإيميل ','class' => 'form-control mx-sm-3')) !!}
                  </div>

                  <div class="form-group">

                    {!! Form::password('password', array('placeholder' => 'كلمة المرور ','class' => 'form-control mx-sm-3')) !!}
                  </div>

                  <div class="form-group">

                    {!! Form::password('confirm-password', array('placeholder' => 'تأكيد كلمة المرور ','class' => 'form-control mx-sm-3')) !!}
                  </div>

                  <div class="form-group" style="width: 470px;">
                    <div class="multi_select_box">

                      {!! Form::select('roles[]', $roles,[], array('class' => 'form-control mx-sm-3 multi_select w_100', 'multiple',' multiple data-selected-text-format="count >3"')) !!}

                    </div>
                  </div>
                  <div class="form-group col-lg-6 col-md-6" style="float: right;">
                    <div class=" fileUpload btn btn-light">
                      <label class="upload">
                        <input name="image_path" type="file" class="form-control mx-sm-3">تحميل صورة </label>
                    </div>
                  </div>



                </div>
                <div class="col-md-3 col-lg-3">
                  <img src="/images/265667.png" id="image">
                </div>
                <div class=" col-md-4 col-lg-4 ">
                  <button type="submit" class="btn btn-theme02 pull-left" style="position: relative;right: 600px;  margin-top: -80px;">حفظ</button>
                </div>
              </div>
            </form>
          </div>
          <!-- end information contract -->
          <!-- start all employ -->
          <div id="edit" class="tab-pane">
            <div class="row mb">
              <div class="col-lg-12 col-md-12">
                <!-- page start-->
                <div class="content-panel">
                  <div class="adv-table">
                    <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="example">
                      <thead>
                        <tr>
                          <th>الاسم </th>
                          <th>البريد الإلكتروني </th>

                          <th class="hidden-phone">الصلاحية</th>
                          <th class="hidden-phone">حظر</th>
                          <th class="hidden-phone"></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($data as $key => $user)
                                  <?php

              $userRoles = DB::Connection('mysql2')->table("model_has_roles")->where("model_has_roles.model_id",$user->id)
            ->pluck('model_has_roles.role_id','model_has_roles.role_id')
            ->all();
                   ?>
                       @if($userRoles)
                        <tr class="gradeU">
                          <td>{{ $user->name }}</td>
                          <td>{{ $user->email }}</td>

                          <td class="hidden-phone">
                           @if(!empty($user->getRoleNames()))
                            @foreach($user->getRoleNames() as $v)
                            <label class="badge badge-success">{{ $v }}</label>
                            @endforeach
                            @endif
                          </td>
                          <td class="text-center">
                            @if($user->status_bride==1)
                            <a class="btn btn-success btn-xs" href="{{ route('block', $user->id) }}">
                              <i class=" fa fa-ban  btn-xs"></i>
                            </a>
                            @else
                            <a class="btn btn-danger btn-xs" href="{{ route('unblock', $user->id) }}">
                              <i class=" fa fa-ban  btn-xs"></i>
                            </a>
                            @endif
                          </td>

                          <td class="center hidden-phone">

                            <a href="" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#Modal_reem{{$user->id}}" href="{{ route('users.edit',$user->id) }}">
                              <i class=" fa fa-pencil"></i>
                            </a>
                            <!-- start madal edit notes -->
                            <div class="modal fade" id="Modal_reem{{$user->id}}" tabindex="-1" role="  dialog" aria-labelledby="Modal_reem{{$user->id}}" aria-hidden="true">
                              <div class="modal-dialog ">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title centered" id="myModalLabel">تعديل المستخدم </h4>
                                  </div>
                                  <form method="post" action="{{ route('users.update', $user->id) }}" enctype="multipart/form-data">
                                    @method('PATCH')
                                    @csrf

                                    <div class="modal-body">
                                      <div class="container-fluid">
                                        <div class="row ">
                                          <div class="col-md-12 col-lg-12">
                                            <div class="form-group">
                                              <div>
                                                <label for="inputPassword6" style="margin:5px;float: right; ">إسم المستخدم :</label>
                                                <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="name" value="{{$user->name}}" />
                                              </div>
                                            </div>
                                            <div class="form-group">
                                              <label for="inputPassword6" style="margin:5px;float: right; ">البريد الإلكتروني :</label>
                                              <input type="email" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="email" value="{{$user->email}}">
                                            </div>

                                            <div class="form-group" style="width: 470px;">
                                              <label for="inputPassword6" style="margin:5px;float: right; ">الوظيفة :</label>
                                              <div class="multi_select_box">

                                                {!! Form::select('roles[]', $roles,[], array('class' => 'form-control mx-sm-3 multi_select w_100', 'multiple',' multiple data-selected-text-format="count >3"')) !!}
                                              </div>
                                            </div>



                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer ">
                                      <button type="submit" class="btn btn-theme02 pull-left" data-dismiss="modal{{$user->id}}">تعديل </button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                            <!-- end madal edit notes -->
                            <div class="col-lg-8 col-md-8">
                              <form action="{{ route('users.destroy', $user->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-xs " class="button" method="post" href="{{ route('users.destroy', $user->id)}}">
                                  <i class="fa fa-trash-o "></i>
                                </button>
                              </form>
                            </div>
                          </td>

                        </tr>
                         @endif
                        @endforeach

                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- page end-->
              </div>
            </div>
            <!-- /row -->
          </div>
          <!-- end all employ -->
           <!-- start all user -->
          <div id="all" class="tab-pane">
            <div class="row mb">
              <div class="col-lg-12 col-md-12">
                <!-- page start-->
                <div class="content-panel">
                  <div class="adv-table">
                    <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="example">
                      <thead>
                        <tr>
                          <th>الاسم </th>
                          <th>البريد الإلكتروني </th>

                          <th class="hidden-phone">حظر</th>
                       
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($data as $key => $user)
                                        <?php

$userRoles = DB::Connection('mysql2')->table("model_has_roles")->where("model_has_roles.model_id",$user->id)
            ->pluck('model_has_roles.role_id','model_has_roles.role_id')
            ->all();
?>
@if(!$userRoles)
                        <tr class="gradeU">
                          <td>{{ $user->name }}</td>
                          <td>{{ $user->email }}</td>

                        
                          <td class="text-center">
                            @if($user->status_bride==1)
                            <a class="btn btn-success btn-xs" href="{{ route('block', $user->id) }}">
                              <i class=" fa fa-ban  btn-xs"></i>
                            </a>
                            @else
                            <a class="btn btn-danger btn-xs" href="{{ route('unblock', $user->id) }}">
                              <i class=" fa fa-ban  btn-xs"></i>
                            </a>
                            @endif
                          </td>

                          
                        </tr>
                        @endif
                        @endforeach

                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- page end-->
              </div>
            </div>
            <!-- /row -->
          </div>
          <!-- end all user -->
        </div>
      </div>
      <!-- end panel-body -->
    </div>
  </div>
</div> @endsection