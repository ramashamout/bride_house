@extends('master')
@section('contents')
<div class="wrapper style3">
  <div class="title ">
    <h2>
      <a href="index.html">المواعيد قيد الانتظار </a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">

          <li>
            <a data-toggle="tab" href="#edit">موعد الزبونة {{$Appointment->title}}
            </a>
          </li>
        </ul>
      </div>
      <div class="panel-body">
        <div class="tab-content">
          <!-- start all contract -->
          <div id="edit" class="tab-pane  active">
            <div id="edit" class="tab-pane">
              <div class="row mb">
                <div class="col-lg-12 col-md-12">
                  <!-- page start-->
                  <div class="content-panel">
                    <div class="adv-table">
                      <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered">
                        <thead>
                          <tr>
                            <th>اسم الزبون </th>
                            <th>رقم الموبايل </th>
                            <th class="hidden-phone">نوع الموعد </th>
                            <th class="hidden-phone">تاريخ الموعد </th>
                            <th class="hidden-phone">توقيت الموعد </th>
                            <th class="hidden-phone">حالة الموعد </th>
                          </tr>
                        </thead>
                        <tbody>

                          <tr class="gradeU">
                            <td>{{$Appointment->title}}</td>
                            <td>{{$Appointment->mobile_number}}</td>
                            <td class="hidden-phone">{{$Appointment->type}}</td>
                            <td class="center hidden-phone">{{$Appointment->date}}</td>
                            <td class="center hidden-phone">{{$Appointment->time}}</td>
                            <td class="center hidden-phone">{{$Appointment->statue_app}}</td>
                          </tr>

                        </tbody>
                      </table>


                      <a href="{{ route('appointment.edit',$Appointment->id)}}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#basicModals{{$Appointment->id}}"> تثبيت الموعد </a>
                      <a href="{{ route('appointment.edit',$Appointment->id)}}" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#basicModal{{$Appointment->id}}"> إلغاء الموعد </a>
                      <!-- start madal edit notes -->
                      <div class="modal fade" id="basicModals{{$Appointment->id}}" tabindex="-1" role="  dialog" aria-labelledby="basicModals{{$Appointment->id}}" aria-hidden="true">
                        <div class="modal-dialog  modal-lg">
                          <div class="modal-content" style="width: 700px;">

                            <div class="modal-header">
                              <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title centered" id="myModalLabel"> إرسال إشعار </h4>
                            </div>

                            <form style="padding-top: 10px; " method="post" action="{{ route('sendnotifyappointment',$Appointment->id) }}" enctype="multipart/form-data">

                              @csrf
                              <div class="row mt">
                                <div class="col-md-9 col-lg-9">
                                  <br>

                                  <div class="form-group col-lg-9 col-md-9" style="float: right;margin-right: 100px;">
                                    <label for="inputPassword6" style="float: right;"> عنوان الإشعار :</label>
                                    <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="title" />

                                  </div>
                                  <br>

                                  <div class="form-group col-lg-9 col-md-9" style="float: right;margin-right: 100px;">
                                    <label for="inputPassword6" style="float: right;">مضمون الإشعار :</label>
                                    <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="body" />
                                  </div>
                                </div>
                                <div class="col-md-3 col-lg-3 ">
                                  <div style="margin-right: 20px;">
                                    <img src="images/set_dresses.jpg" id="image">
                                  </div>
                                </div>
                                <div class=" col-md-4 col-lg-4 ">
                                  <button type="submit" class="btn btn-theme02 pull-left" style="margin-left: 50px; margin-top: -45px;">حفظ </button>
                                </div>

                              </div>


                            </form>

                          </div>
                        </div>
                      </div>


                      <!-- end models -->
                      <!-- start madal edit notes -->
                      <div class="modal fade" id="basicModal{{$Appointment->id}}" tabindex="-1" role="  dialog" aria-labelledby="basicModal{{$Appointment->id}}" aria-hidden="true">
                        <div class="modal-dialog  modal-lg">
                          <div class="modal-content" style="width: 700px;">

                            <div class="modal-header">
                              <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                              <h4 class="modal-title centered" id="myModalLabel"> إرسال إشعار </h4>
                            </div>

                            <form style="padding-top: 10px; " method="post" action="{{ route('sendnotifyappointmentCancel',$Appointment->id) }}" enctype="multipart/form-data">

                              @csrf
                              <div class="row mt">
                                <div class="col-md-9 col-lg-9">
                                  <br>

                                  <div class="form-group col-lg-9 col-md-9" style="float: right;margin-right: 100px;">
                                    <label for="inputPassword6" style="float: right;"> عنوان الإشعار :</label>
                                    <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="title" />

                                  </div>
                                  <br>
                                  <div class="form-group col-lg-9 col-md-9" style="float: right;margin-right: 100px;">
                                    <label for="inputPassword6" style="float: right;">مضمون الإشعار :</label>
                                    <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="body" />
                                  </div>
                                </div>
                                <div class="col-md-3 col-lg-3 ">
                                  <div style="margin-right: 20px;">
                                    <img src="images/set_dresses.jpg" id="image">
                                  </div>
                                </div>
                                <div class=" col-md-4 col-lg-4 ">
                                  <button type="submit" class="btn btn-theme02 pull-left" style="margin-left: 50px; margin-top: -45px;">حفظ </button>
                                </div>

                              </div>


                            </form>

                          </div>
                        </div>
                      </div>


                      <!-- end models -->
                    </div>
                  </div>

                  <!-- page end-->
                </div>
              </div>
              <!-- /row -->
            </div>
          </div>
          <!-- end panel-body -->
        </div>
      </div>
    </div>
  </div>
</div>
@endsection