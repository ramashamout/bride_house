@extends('master')
@section('contents')
<div class="wrapper style3">
	<div class="title ">
		<h2>
			<a href="index.html">إدارة المواعيد</a>
		</h2>
	</div>
	<div class="container">
		<div class="panel panel-default">
			<!-- start panel heading -->
			<div class="panel-heading ">
				<ul class="nav nav-tabs nav-justified">

					<li>
						<a href="/app">جميع المواعيد </a>
					</li>
				</ul>
			</div>
			<div class="panel-body">
				<div class="tab-content">
					<!-- start all contract -->
					<div id="edit" class="tab-pane  active">
						<div class="row mt">
							<div class="col-md-12">
								<section class="task-panel tasks-widget">
									<div class="panel-body">
										<div class="task-content">
											<ul class="task-list">
												@foreach($app as $app)
												<li>

													<div class="task-title">
														<span class="task-title-sp">
															<i class="fa fa-circle" style="font-size:10px"></i>&nbsp;{{$app->title}}
														</span>
														<div class="pull-left hidden-phone">
															<a href="{{ route('appointment.edit',$app->id)}}" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#basicModal{{$app->id}}">
																<i class=" fa fa-pencil"></i>
															</a>


															<!-- start madal edit notes -->
															<div class="modal fade" id="basicModal{{$app->id}}" tabindex="-1" role="  dialog" aria-labelledby="basicModal{{$app->id}}" aria-hidden="true">
																<div class="modal-dialog ">
																	<div class="modal-content">
																		<div class="modal-header">
																			<button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
																			<h4 class="modal-title centered" id="myModalLabel">تعديل الموعد </h4>
																		</div>
																		<form method="post" action="{{ route('appointment.update', $app->id) }}" enctype="multipart/form-data">
																			@method('PATCH')
																			@csrf

																			<div class="modal-body">
																				<div class="container-fluid">
																					<div class="row ">
																						<div class="col-md-12 col-lg-12">

																							<div class="form-group">
																								<input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" placeholder="اسم الزبون " name="title" value="{{$app->title}}">
																							</div>
																							<div class="form-group ">
																								<input type="number" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" placeholder="رقم الموبايل " name="mobile_number" value="{{$app->mobile_number}}">
																							</div>
																							<div class="form-group ">
																								<input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" placeholder="نوع الموعد " name="type" value="{{$app->type}}">
																							</div>
																							<div class="form-group ">
																								<input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" placeholder="تاريخ الموعد :" class="form-control" name="start" value="{{$app->start}}" />
																							</div>
																		
																							<div class="form-group">

																								<input type="hidden" name="color" value="pink">

																							</div>


																						</div>
																					</div>
																				</div>
																			</div>
																			<div class="modal-footer ">
																				<button type="submit" class="btn btn-theme02 pull-left" data-dismiss="modal{{$app->id}}">تعديل </button>
																			</div>
																		</form>
																	</div>
																</div>
															</div>
															<!-- end madal edit notes -->

															<div class="col-lg-8 col-md-8">
																<form action="{{ route('appointment.destroy', $app->id)}}" method="post">
																	@csrf
																	@method('DELETE')

																	<button class="btn btn-danger btn-xs " class="button" method="post" href="#">
																		<i class="fa fa-trash-o "></i>
																	</button>
																</form>
															</div>

														</div>
													</div>
												</li>
												@endforeach
											</ul>
										</div>
									</div>
								</section>
							</div>

							<!-- end all contract -->
						</div>
					</div>
				</div>
			</div>
		</div>
		@endsection