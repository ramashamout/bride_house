@extends('master')
@section('contents')
<div class="wrapper style3">
  <div class="title ">
    <h2>
      <a href="index.html">المواعيد قيد الانتظار </a>
    </h2>
  </div>

  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">

          <li>
            <a data-toggle="tab" href="#edit">المواعيد قيد الانتظار
            </a>
          </li>
        </ul>
      </div>
      <div class="panel-body">
        <div class="tab-content">
          <!-- start all contract -->
          <div id="edit" class="tab-pane  active">
            <div class="row mt">
              <div class="col-md-12">
                <section class="task-panel tasks-widget">
                  <div class="panel-body">
                    <div class="task-content">
                      <ul class="task-list">
                        @foreach($Appointment as $app)

                        <li>

                          <div class="task-title">
                            <span class="task-title-sp">
                              <i class="fa fa-circle" style="font-size:10px"></i>&nbsp;{{$app->title}}&nbsp;{{$app->statue_app}}
                            </span>
                            <div class="pull-left hidden-phone">
                              <a href="/app_s/{{$app->id}}" class="btn btn-success btn-xs">
                                <i class=" fa fa-check"></i>
                              </a>


                              <div class="col-lg-8 col-md-8">
                                <form action="" method="post">


                                  <button class="btn btn-danger btn-xs " class="button" method="post" href="#">
                                    <i class="fa fa-trash-o "></i>
                                  </button>
                                </form>
                              </div>

                            </div>
                          </div>
                        </li>

                        @endforeach
                      </ul>
                    </div>
                  </div>
                </section>
              </div>

              <!-- end all contract -->
            </div>
          </div>
          <!-- end panel-body -->
        </div>
      </div>
    </div>
  </div>
</div>
@endsection