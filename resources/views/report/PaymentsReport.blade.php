@extends('master') @section('contents') <div class="wrapper style3 ">
  <div class="title ">
    <h2>
    <a href="index.html">تقارير البيع </a>
    </h2>
  </div>
  <div class="row ">
    <div class="pull-right" style="margin-right: 180px;">
      <form method="post" action="{{ route('filter_payment') }}" enctype="multipart/form-data">
        @csrf
        <div class=" col-md-4 col-lg-4 col-xs-4  ">

          <button type="submit" class="btn btn-primary" > بحث </button>
        </div>
        <div class=" col-md-4 col-lg-4  col-xs-4" >
          
          <div class="form-group" >
            
            <input type="text" class="form-control" name="month" placeholder="الشهر" />
          </div>
        </div>
        <div class=" col-md-4 col-lg-4 col-xs-4" >
          <div class="form-group" >
            
            <input type="text" class="form-control" name="year" placeholder="السنة" />
          </div>
        </div>
        
      </form>
    </div>
  </div>
  
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">
            المبيعات</a>
          </li>
         
        </ul>
      </div>
      @if ($message = Session::get('success'))
      <div class="alert alert-success">
        <p>{{ $message }}</p>
      </div>
      @endif
      <div class="panel-body">
        <div class="tab-content">
          <div id="overview" class="tab-pane active">
             <div class="col-lg-12 ">
              
              <div class="pull-right">
                
                <h5 style=" color: #717479;; font-size: 18px;" >تقارير الآجار : </h5>
                
              </div>
            </div>
            
            <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered">
              <thead>
                <tr>
                  <th>الرقم التسلسلي </th>
                  <th> رقم العقد </th>
                  <th> اسم القطعة </th>
                  <th>تاريخ الآجار </th>
                  <th> سعر الآجار </th>
                </tr>
              </thead>
              <tbody>
                @foreach ($piece_contract as $pc)
                <tr>
                  @if($pc->contract['contract_type']=='آجار')
                  
                  <td>{{ ++$i }}</td>
                  
                  <td> <a class="show" href="{{ route('contract.show',$pc->contract_id) }}">
                  {{ $pc->contract_id }}</a></td>
                  @if($pc->clothe!=null)
                  <td >{{ $pc->clothe['name'] }}</td>
                  @else
                  <td>{{ $pc->accessory['name'] }}</td>
                  @endif
                  <td>{{ $pc->contract['contract_date'] }}</td>
                  <td >{{ $pc->contract['total_price'] }}</td>
                  @endif
                </tr>
                @endforeach
                
                <tr>
                  
                  <td>{{ $i }}</td>
                  <td>__ </td>
                  <td>__</td>
                  <td>__</td>
                  <td>{{ __('العدد الإجمالي   ') }}</td>
                </tr>
                <tr>
                  
                  <td >{{$sum}}</td>
                  <td>__</td>
                  <td>__</td>
                  <td>__</td>
                  <td>{{ __('السعر الكلي  ') }}</td>
                </tr>
              </tbody>
            </table><br>
            <div class="pull-right">
                
                <h5 style=" color: #717479;; font-size: 18px;" >تقارير البيع : </h5>
                
              </div>
              <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered">
                      <thead>
                        <tr>
                          <th>الرقم التسلسلي </th>
                          <th> رقم العقد </th>
                          <th> اسم القطعة </th>
                          <th>تاريخ البيع </th>
                          <th> سعر البيع </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($piece_contract as $pc)
                        <tr>
                          @if($pc->contract['contract_type']=='بيع')
                          <td>{{ ++$j }}</td>
                          <td> <a class="show" href="{{ route('contract.show',$pc->contract_id) }}">
                          {{ $pc->contract_id }}</a></td>
                          @if($pc->clothe!=null)
                          <td>{{ $pc->clothe['name'] }}</td>
                          @else
                          <td>{{ $pc->accessory['name'] }}</td>
                          @endif
                          <td>{{ $pc->contract['contract_date'] }}</td>
                          <td>{{ $pc->contract['total_price'] }}</td>
                          @endif
                        </tr>
                        @endforeach
                        
                        <tr>
                          
                          <td>{{ $j }}</td>
                          <td> __ </td>
                          <td> __</td>
                          <td> __</td>
                          <td>{{ __('العدد الإجمالي   ') }}</td>
                        </tr>
                        <tr>
                          
                          <td>{{$sum1}}</td>
                          <td>__ </td>
                          <td>__ </td>
                          <td>__ </td>
                          <td>{{ __('السعر الكلي  ') }}</td>
                        </tr>
                      </tbody>
                    </table>
          </div>
          <div id="edit" class="tab-pane">
            <div class="row mb">
              <div class="col-lg-12 col-md-12">
                <!-- page start-->
               
                  
              </div>
            </div>
          </div>
          
            <div class="col-lg-12 ">
              
              <div class="pull-right">
                
                <h5 style="color: black; font-size: 16px;" >
                {{ __('سعر المبيعات الإجمالي  :') }}{{ $sum2 }}
                </h5>
                <h5 style="color: black; font-size: 16px;margin-top: 5px;">
                {{ __('عدد القطع الكلي  :') }}{{ $i+$j }}
                </h5>
              </div>
            </div>
         
 
        </div>
      </div>
    </div>
  </div>
</div>
@endsection