@extends('master') @section('contents') <div class="wrapper style3">
  <div class="title">
    <h2>
      <a href="index.html">فواتير المدخلات</a>
    </h2>
  </div>
    <div class="row ">
    <div class="pull-right" style="margin-right: 180px;">
      <form method="post" action="{{ route('filter_payment') }}" enctype="multipart/form-data">
        @csrf
        <div class=" col-md-4 col-lg-4 col-xs-4  ">

          <button type="submit" class="btn btn-primary" > بحث </button>
        </div>
        <div class=" col-md-4 col-lg-4  col-xs-4" >
          
          <div class="form-group" >
            
            <input type="text" class="form-control" name="month" placeholder="الشهر" />
          </div>
        </div>
        <div class=" col-md-4 col-lg-4 col-xs-4" >
          <div class="form-group" >
            
            <input type="text" class="form-control" name="year" placeholder="السنة" />
          </div>
        </div>
        
      </form>
    </div>
  </div>
  <!-- Content -->
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">فواتير قسم التنظيفات </a>
          </li>
          <li>
            <a data-toggle="tab" href="#status" class="contact-map">فواتير قسم المدفوعات</a>
          </li>
          <li>
            <a data-toggle="tab" href="#edit">فواتير قسم التصميمات </a>
          </li>
        </ul>
      </div>
      @if ($message = Session::get('success'))
    <div class="alert alert-success">
        <p>{{ $message }}</p>
    </div>
     @endif
      <div class="panel-body">
        <div class="tab-content">
            <div id="overview" class="tab-pane active">

              <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="example">
                    <thead>
     <tr>
     <th>الرقم التسلسلي</th>
      <th>رقم عملية التنظيف </th>
     <th> اسم القطعة </th>
     <th> اسم المصبغة </th>
     <th>سعر التنظيف </th>
     <th> تاريخ التنظيف </th>
    
  </tr>
     </thead>
         <tbody>
    @foreach ($Cleans as $clean)
    <tr>

        <td>{{ ++$i }}</td>
        <td> <a class="show" href="{{ route('clean.show',$clean->id) }}">
          {{ $clean->id }} </a></td>
        <td>{{ $clean->clothe['name'] }}</td>
        <td>{{ $clean->laundry}}</td>
        <td>{{ $clean->cleaning_price }}</td>
        <td>{{ $clean->end }}</td>

    </tr>
        @endforeach
    <tr>
        
        <td>{{ $i }}</td>
        <td>__</td>
        <td>__</td>
        <td>__</td>
        <td>__</td>
        <td>{{ __('العدد الإجمالي   ') }}</td>
    </tr>
    <tr>
        
        <td>{{ $price_clean }}</td>
        <td>__</td>
        <td>__</td>
        <td>__</td>
        <td>__</td>
        <td>{{ __('السعر الكلي  ') }}</td>
    </tr>
</tbody>
</table>

    </div>
     <div id="status" class="tab-pane">
            <div class="row mt">
              <div class="col-md-12">
                <!-- start section note to one dress-->
                <section id="unseen">
                <div class="adv-table">
              <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="example">
                    <thead>
                       <tr>
                       <th>الرقم التسلسلي</th>
                       <th> رقم الفاتورة </th>
                       <th> تاريخ الفاتورة </th>
                       <th>السعر الإجمالي </th>
                       <th> عدد القطع </th>
                      
                    </tr>
                    </thead>
                    <tbody>
                    

  
@foreach ($invoices as $invoice)
    <tr>
        <td>{{ ++$k }}</td>
        <td> <a class="show" href="{{ route('invoice.show',$invoice->id) }}">
           {{ $invoice->id }}</a></td>
        <td>{{ $invoice->invoice_date }}</td>
        <td>{{ $invoice->total_price }}</td>
         @foreach ($invoice->invoice_line as $invoice_line) 
         <div class="hiddentext" style="display: none;">
            {{$num_pieces=$num_pieces+$invoice_line->number_of_pieces;}}
            {{$total_pieces=$total_pieces+$invoice_line->number_of_pieces;}}
         </div>
     @endforeach
      <td>{{ $num_pieces }}</td>
     <div class="hiddentext" style="display: none;"> 
          {{ $num_pieces=0;}} 
      </div>

  @endforeach
       
    <tr>
        
        <td>{{ $total_pieces }}
        <td>__</td>
        <td>__</td>
        <td>__</td>
        <td>{{ __('العدد الإجمالي   ') }}</td>
    </tr>
    <tr>
       
 <td>{{  $price_invoice }}</td> 
        <td>__</td>
        <td>__</td>
        <td>__</td>
        <td>{{ __('السعر الكلي  ') }}</td>
    </tr>
                    </tbody>
                  </table>
                </div>
                </section>
                <!-- end section note to one dress-->
              </div>
            </div>
          </div>
     <div id="edit" class="tab-pane">
       <div class="row mb">
              <div class="col-lg-12 col-md-12">
                <!-- page start-->
                <div class="content-panel">
                  <div class="adv-table">
                     <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="example">
                       
                          <thead>
                            <tr>
                             <th>الرقم التسلسلي</th>
                              <th>رقم التصميم </th>
                             <th> اسم التصميم </th>
                             <th>سعر التصميم </th>
                             <th>تاريخ التصميم </th>
                            
                            
                          </tr>
                         </thead>
                            <tbody>
                               
      
    @foreach ($designs as $design)
    <tr>
        <td>{{ ++$j }}</td>
        <td> <a class="show" href="{{ route('design.show',$design->id) }}">
           {{ $design->id }}</a></td>
        <td>{{ $design->model}}</td>
        <td>{{ $design->designing_price }}</td>
        <td>{{ $design->delivery_date }}</td>


    </tr>
        @endforeach
    <tr>
        
        <td>{{ $j }}</td>
        <td>__</td>
        <td>__</td>
        <td>__</td>
        <td>{{ __('العدد الإجمالي   ') }}</td>
    </tr>
    <tr>
        
        <td>{{ $price_design }}</td>
        <td>__</td>
        <td>__</td>
        <td>__</td>
        <td>{{ __('السعر الكلي  ') }}</td>
    </tr>

                            </tbody>
                          </table>
                  </div>
              </div>
          </div>
      </div>
  </div>



    <div class="col-lg-12 ">
       
        <div class="pull-right">
        
             <h5 style="color: black; font-size: 16px;" >
            {{ __('سعر المبيعات الإجمالي  :') }}{{ $price_invoice+$price_design+$price_clean }}
       </h5>
                <h5 style="color: black; font-size: 16px;margin-top: 5px;">
            {{ __('عدد القطع الكلي  :') }}{{ $i+$j+$total_pieces }}
          </h5>



</div>
</div>




  </div>

   
</div>
@endsection