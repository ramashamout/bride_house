@extends('base')
@section('main')
    <div class="row">
        <div class="col-sm-12">
            <h1 class="display-3">Piece Notification</h1>
            <div>
                <a href="{{ route('pieceReminder.create')}}" class="btn btn-primary mb-3">Add piece Notification</a>
            </div>

            @if(session()->get('success'))
                <div class="alert alert-success">
                    {{ session()->get('success') }}
                </div>
            @endif
            <table class="table table-striped">
                <thead>
                <tr>
                    <td>ID</td>
                    <td> Full Name</td>
                    <td>Mobile Number</td>
                    <td>Address</td>
                </tr>
                </thead>
                <tbody>
                <h1>Piece </h1>
                @foreach($user->unreadNotifications as $pieceReminder)

                    @if($pieceReminder->data['type']=="piece")
                        <tr>
                            <td>{{$pieceReminder->data['id']}}</td>
                            <td>{{$pieceReminder->data['content']}} </td>
                            <td>{{$pieceReminder->data['date']}}</td>
                            <td>{{$pieceReminder->data['mobile_number']}}</td>
                            <td>{{$pieceReminder->data['customer_name']}}</td>
                            <td>
                                <a href="{{ route('markAsRead',$pieceReminder->id)}}" class="btn btn-primary">mark as read</a>
                            </td>
                        </tr>
                    @endif
                @endforeach


                </tbody>
            </table>
            <div>
            </div></div></div>
@endsection
