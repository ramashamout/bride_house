@extends('master') @section('contents') <div class="wrapper style3">
    <div class="title">
        <h2>
        <a href="index.html">إضافة سطر إلى الفاتورة</a>
        </h2>
    </div>
    <!-- Content -->
    <div class="container">
        <div class="panel panel-default">
            <!-- start panel heading -->
            <div class="panel-heading ">
                <ul class="nav nav-tabs nav-justified">
                    <li class="active">
                        <a data-toggle="tab" href="#overview">
                        clothe </a>
                    </li>
                    <li>
                        <a data-toggle="tab" href="#edit">accessory</a>
                    </li>
                </ul>
            </div>
            <!-- start panel-body -->
            <div class="panel-body">
                <div class="tab-content">
                    <!-- start information cotract -->
                    <div id="overview" class="tab-pane active">
                        <form style="padding-top: 50px;" method="post" action="{{ route('invoiceLine.store') }}"
                            enctype="multipart/form-data">
                            @csrf
                            <div class="row mt">
                                <div class="col-md-9 col-lg-9">
                                    
                                    <div class="form-group col-lg-4 col-md-4">
                                        <label for="inputPassword6">سعر الآجار :</label>
                                        <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"name="rent_price"/>
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4">
                                        <label for="inputPassword6">الوصف :</label>
                                        <input type="text"  id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"name="description" >
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4">
                                        
                                        <label for="inputPassword6">اسم القطعة :</label>
                                        <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="name"/>
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4" >
                                        <label for="inputPassword6">سعر القطعة الواحدة :</label>
                                        <input type="number" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"  name="unit_price" >
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4">
                                        <input type="hidden" name="type_general"  value= "clothe">
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4">
                                        <input type="hidden" name="invoice_id"  value= {{$invoice_id}} >
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-4" style="float: right;">
                                        <label for="inputPassword6">نوع المدخل  :</label>
                                        
                                        
                                        <select  class="multi_select" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"  name="type_clo"data-selected-text-format="count >3" >
                                            @foreach(\App\Models\Type::all() as $Type1)
                                            @if($Type1->type_piece =='clothe')
                                            <option value="{{$Type1->name}}">{{$Type1->name}}
                                            </option>
                                            @endif
                                            @endforeach
                                            
                                        </select>
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-4">
                                        <label for="inputPassword6">عدد القطع :</label>
                                        <input type="number" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"  name="number_of_pieces" >
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4">
                                        
                                        <label for="inputPassword6">سعر البيع :</label>
                                        <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="selling_price"/>
                                        
                                    </div>
                                    
                                    <div class="form-group col-lg-4 col-md-4">
                                        <label for="inputPassword6" >لون الفستان :</label>
                                        <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="color"/>
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4">
                                        <label for="inputPassword6">قمية الخصم:</label>
                                        <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="discount" value="0" />
                                    </div>
                                    <div class="form-group col-lg-4 col-md-4" style=" margin-top: 18px;">
                                        <div class=" fileUpload btn btn-light">
                                            <label class="upload">
                                                <input name="piece_img" type="file" class="form-control mx-sm-3">تحميل صورة </label>
                                            </div>
                                        </div>
                                        <div class="form-group col-lg-4 col-md-4">
                                            <label for="inputPassword6">اسم المصمم:</label>
                                            <input type="text"  class="form-control mx-sm-3" aria-describedby="passwordHelpInline"name="designer_name"/>
                                        </div>
                                        
                                        
                                        
                                        
                                        <div class="form-group col-lg-4 col-md-4"style="float: right;">
                                            <label for="inputPassword6">القياس :</label>
                                            <select  class="form-control mx-sm-3"  name="size" >
                                                
                                                @foreach(\App\Models\size_general::all() as $size_general)
                                                <option value="{{$size_general->value}}">{{$size_general->value}}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        
                                        
                                        <div class="form-group col-lg-4 col-md-4" style="
                                            margin-top: 18px;float: right;">
                                            <div class=" fileUpload btn btn-light" id="im">
                                                <label class="upload">
                                                    <input  name="other_image[]"  type="file" class="form-control mx-sm-3" multiple>
                                                صور آخرى للفستان</label>
                                            </div>
                                        </div>
                                        
                                        
                                    </div>
                                    <div class="col-md-3 col-lg-3">
                                        <img src="/images/invoice.jpg" id="image" style=" bottom: 50px;width: 300px;height: 400px;">
                                        <a href="{{ route('invoice_total',$invoice_id)}}"  class="btn btn-success  btn-sm"
                                            style="margin-right: 50px;">
                                            الانتهاء من إدخال الفاتورة
                                        </a>
                                    </div>
                                    <div class=" col-lg-4 col-md-4">
                                        <button type="submit" class="btn btn-theme04 pull-left" style="margin-bottom: 60px;margin-left: 30px;">حفظ</button>
                                    </div>
                                </div>
                                
                            </form>
                        </div>
                        <!-- end information contract -->
                        <!-- start all contract -->
                        <div id="edit" class="tab-pane">
                            <form style="padding-top: 50px;" method="post" action="{{ route('invoiceLine.store') }}"
                                enctype="multipart/form-data">
                                @csrf
                                <div class="row mt">
                                    <div class="col-md-9 col-lg-9">
                                        
                                        <div class="form-group col-lg-4 col-md-4">
                                            <label for="inputPassword6">سعر الآجار :</label>
                                            <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"name="rent_price"/>
                                        </div>
                                        <div class="form-group col-lg-4 col-md-4">
                                            <label for="inputPassword6">الوصف :</label>
                                            <input type="text"  id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"name="description" >
                                        </div>
                                        <div class="form-group col-lg-4 col-md-4">
                                            
                                            <label for="inputPassword6">اسم القطعة :</label>
                                            <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="name"/>
                                        </div>
                                        <div class="form-group col-lg-4 col-md-4">
                                            <label for="inputPassword6">سعر القطعة الواحدة :</label>
                                            <input type="number" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"  name="unit_price" >
                                        </div>
                                        <div class="form-group col-lg-4 col-md-4">
                                            <input type="hidden" name="type_general"  value= "accessory">
                                        </div>
                                        <div class="form-group col-lg-4 col-md-4">
                                            <input type="hidden" name="invoice_id"  value= {{$invoice_id}} >
                                        </div>
                                        
                                        <div class="form-group col-lg-4 col-md-4">
                                            <label for="inputPassword6">عدد القطع :</label>
                                            <input type="number" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"  name="number_of_pieces" >
                                        </div>
                                        <div class="form-group col-lg-4 col-md-4">
                                            
                                            <label for="inputPassword6">سعر البيع :</label>
                                            <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="selling_price"/>
                                            
                                        </div>
                                        
                                        <div class="form-group col-lg-4 col-md-4" style="float: right;">
                                            <label for="inputPassword6">نوع المدخل  :</label>
                                            
                                            
                                            <select  class="multi_select" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"  name="type_clo"data-selected-text-format="count >3" >
                                                @foreach(\App\Models\Type::all() as $Type2)
                                                @if($Type2->type_piece =='accessory')
                                                <option value="{{$Type2->name}}">{{$Type2->name}} </option>
                                                @endif
                                                @endforeach
                                                
                                            </select>
                                        </div>
                                        <div class="form-group col-lg-4 col-md-4" style="float: right;">
                                            
                                            <label for="inputPassword6">الفئة </label>
                                            
                                            <div class="multi_select_box" >
                                                <select  class="multi_select" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"  name="category"data-selected-text-format="count >3" >
                                                    <option value="فضي">فضي</option>
                                                    <option value="ذهبي">ذهبي</option>
                                                </select>
                                            </div>
                                            
                                        </div>
                                        
                                        
                                        
                                        <div class="form-group col-lg-4 col-md-4" style=" margin-top: 18px;">
                                            <div class=" fileUpload btn btn-light">
                                                <label class="upload">
                                                    <input name="piece_img" type="file" class="form-control mx-sm-3">تحميل صورة </label>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                            
                                            
                                            
                                            
                                        </div>
                                        <div class="col-md-3 col-lg-3">
                                            <img src="/images/invoice.jpg" id="image" style=" bottom: 50px;width: 300px;height: 400px;">
                                            <a href="{{ route('invoice_total',$invoice_id)}}"  class="btn btn-success btn-sm"
                                                style="margin-right: 50px;">
                                                الانتهاء من إدخال الفاتورة
                                            </a>
                                        </div>
                                        <div class=" col-lg-4 col-md-4">
                                            <button type="submit" class="btn btn-theme04 pull-left" style="margin-bottom: 60px;margin-left: 30px;">حفظ</button>
                                        </div>
                                    </div>
                                    
                                </form>
                            </div>
                            <!-- end all contract -->
                        </div>
                    </div>
                    <!-- end panel-body -->
                </div>
            </div>
        </div>
        @endsection