@extends('master') @section('contents')
<div class="wrapper style3 ">
  <div class="title ">
    <h2>
      <a href="index.html">إدارة الفواتير </a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">

          <li class="active">
            <a data-toggle="tab" href="#overview">الفاتورة النهائية </a>
          </li>
        </ul>
      </div>
      <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">

          <!-- start all contract -->
          <div id="edit" class="tab-pane active">
            <div class="row mb">
              <div class="col-lg-12 col-md-12">
                <!-- page start-->
                <div class="content-panel">
                  <div class="adv-table">
                    <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="example">
                      <thead>
                        <tr>
                          <th>رقم الفاتورة </th>
                          <th>الوصف</th>
                          <th>عدد القطع </th>
                          <th>سعر القطعة الواحدة </th>
                          <th>السعر الكلي </th>
                          <th></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($invoiceLine as $invoiceLine)
                        @if($invoiceLine->invoice_id == $invoice->id)
                        <tr class="gradeU">
                          <td> {{$invoiceLine->invoice_id}}</td>
                          <td> {{$invoiceLine->description}} </td>
                          <td>{{$invoiceLine->number_of_pieces}}</td>
                          <td>{{$invoiceLine->unit_price}}</td>
                          <td>{{$invoiceLine->total_price}}</td>
                          <td class="center hidden-phone">

                            <a href="{{ route('invoiceLine.edit',$invoiceLine->id)}}" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#basicModal{{$invoiceLine->id}}">
                              <i class=" fa fa-pencil"></i>
                            </a>
                            <!-- start madal edit notes -->
                            <div class="modal fade" id="basicModal{{$invoiceLine->id}}" tabindex="-1" role="dialog" aria-labelledby="basicModal{{$invoiceLine->id}}" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title centered" id="myModalLabel">تعديل سطر الفاتورة</h4>
                                  </div>
                                  <div class="modal-body">
                                    <form method="post" action="{{ route('invoiceLine.update', $invoiceLine->id) }}" enctype="multipart/form-data">
                                      @method('PATCH')
                                      @csrf

                                      <div class="form-group">
                                        <label for="name" style="float: right;">توصيف الفاتورة :</label>
                                        <input type="text" class="form-control" name="description" value="{{$invoiceLine->description}}" />
                                      </div>

                                      <div class="form-group">
                                        <label for="name" style="float: right;">عدد القطع :</label>
                                        <input type="number" class="form-control" name="number_of_pieces" value="{{$invoiceLine->number_of_pieces}}" />
                                      </div>
                                      <div class="form-group">
                                        <label for="name" style="float: right;">سعر القطعة الواحدة :</label>
                                        <input type="number" class="form-control" name="unit_price" value="{{$invoiceLine->unit_price}}" />
                                      </div>



                                      <input type="hidden" name="invoice_id" value="">




                                      <div class="modal-footer ">
                                        <button type="submit" class="btn btn-theme02 pull-left" data-dismiss="modal{{$invoiceLine->id}}">تعديل </button>
                                      </div>
                                    </form>
                                  </div>

                                </div>
                              </div>

                            </div>
                            <div class="pull-left">
                              <form action="{{ route('invoiceLine.destroy', $invoiceLine->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-xs " class="button" method="post" href="{{ route('invoiceLine.destroy', $invoiceLine->id)}}">
                                  <i class="fa fa-trash-o "></i>
                                </button>
                              </form>
                            </div>
                          </td>

                        </tr>


                        @endif
                        @endforeach
                      </tbody>
                    </table>



                    <div class="pull-left">
                      {{-- <a href="{{ route('invoice_total',$invoice->id)}}" class="btn btn-success btn-xs" >--}}
                      {{-- المجموع الكلي--}}
                      {{-- </a>--}}
                      <span> المجموع الكلي : {{$invoice->total_price}} </span>
                    </div>

                  </div>
                </div>
                <!-- page end-->
              </div>
            </div>
            <!-- /row -->
          </div>
          <!-- end all contract -->
        </div>
      </div>
      <!-- end panel-body -->
    </div>
  </div>
</div> @endsection