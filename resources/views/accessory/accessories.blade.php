@extends('master') @section('contents') <div class="wrapper style3 ">
  <div class="title ">
    <h2>
      <a href="index.html">إدارة الملحقات </a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#edit">جميع الملحقات </a>
          </li>
        </ul>
      </div>
      <?php

      use Carbon\Carbon;
      ?>
      <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information cotract -->
          <div id="overview" class="tab-pane ">
            <form style="padding-top: 10px; " method="post" action="{{ route('accessory.store') }}" enctype="multipart/form-data">
              @csrf
              <div class="row mt">
                <div class="col-md-9 col-lg-9">
                  <br>
                  <div class="form-group col-lg-6 col-md-6" style="float: right;">
                    <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" placeholder="اسم الملحق " name="name" />
                  </div>
                  <div class="form-group col-lg-6 col-md-6">
                    <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" placeholder="سعر البيع" name="selling_price" />
                  </div>
                  <div class="form-group col-lg-6 col-md-6">
                    <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" placeholder="سعر الايجار" name="rent_price" />
                  </div>
                  <div class="form-group col-lg-6 col-md-6">
                    <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" placeholder="المجموعة " name="category" />
                  </div>
                  <div class="form-group col-lg-6 col-md-6">
                    <div>
                      <input type="text" list="Type_accessory" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" placeholder="النوع" name="Type_accessory" />
                      <datalist id="Type_accessory">
                        @foreach($Type as $Type)
                        @if($Type->type_piece == 'accessory')
                        <option value="{{$Type->name}}">{{$Type->name}} </option>
                        @endif
                        @endforeach
                      </datalist>
                    </div>
                  </div>
                  <div class="form-group col-lg-6 col-md-6">
                    <div class=" fileUpload btn btn-light">
                      <label class="upload">
                        <input name="piece_img" type="file" class="form-control mx-sm-3">تحميل صورة </label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-lg-3 ">
                  <div style="margin-right: 20px;">
                    <img src="images/accessories.jpg" id="image">
                  </div>
                </div>
                <div class=" col-md-4 col-lg-4 ">
                  <button type="submit" class="btn btn-theme02 pull-left" style="margin-left: 50px; margin-top: -80px;">حفظ</button>
                </div>
              </div>
            </form>
          </div>
          <!-- end information contract -->
          <!-- start all contract -->
          <div id="edit" class="tab-pane active">
            <div class="row mb">
              <div class="col-lg-12 col-md-12">
                <!-- page start-->
                <div class="content-panel">
                  <div class="adv-table">
                    <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="example">
                      <thead>
                        <tr>
                          <th>اسم الملحق </th>
                          <th>سعر الآجار </th>
                          <th class="hidden-phone">سعر البيع </th>
                          <th class="hidden-phone">الصنف </th>
                          <th class="hidden-phone">النوع </th>
                          <th>الصورة </th>
                          <th>حالة القطعة </th>
                          <th class="hidden-phone"></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($accessory as $accessory)
                        @if($accessory->statue_acc !='مستودع' && $accessory->statue_acc !="مباع ")
                        <tr class="gradeU">
                          <td>{{$accessory->name}}</td>
                          <td class="hidden-phone">{{$accessory->rent_price}}</td>
                          <td class="center hidden-phone">{{$accessory->selling_price}}</td>
                          <td class="center hidden-phone">{{$accessory->category}}</td>
                          <td class="center hidden-phone">{{$accessory->Type_accessory}}</td>
                          <td class="center hidden-phone">
                            <img src="/images/accessory/{{$accessory->piece_img}}" id="image2">
                          </td>
                          <td class="center hidden-phone">
                            {{$accessory->statue_acc}}
                            @if($accessory->statue_acc != "متوفر ")
                            <form action="/update_statue_a/{{$accessory->id}}" method="GET">
                              <button class="btn btn-warning btn-xs" href="/update_statue_a/{{$accessory->id}}">
                                <i class="fa fa-minus-circle "></i>
                              </button>
                            </form>
                            @endif
                          </td>
                          <td class="center hidden-phone">
                            <div class="col-lg-8 col-md-8">
                              <form action="{{ route('accessory.destroy', $accessory->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-xs" method="post" href="{{ route('accessory.destroy', $accessory->id)}}">
                                  <i class="fa fa-trash-o "></i>
                                </button>
                              </form>
                            </div>
                            <a href="{{ route('accessory.edit',$accessory->id)}}" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#basicModal{{$accessory->id}}">
                              <i class=" fa fa-pencil"></i>
                            </a>
                            <!-- start madal edit notes -->
                            <div class="modal fade" id="basicModal{{$accessory->id}}" tabindex="-1" role="  dialog" aria-labelledby="basicModal{{$accessory->id}}" aria-hidden="true">
                              <div class="modal-dialog  modal-lg">
                                <div class="modal-content">
                                  <div class="modal-header">
                                    <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title centered" id="myModalLabel">تعديل الملحق :</h4>
                                  </div>
                                  <form method="post" action="{{ route('accessory.update', $accessory->id) }}" enctype="multipart/form-data">
                                    @method('PATCH')
                                    @csrf
                                    <div class="modal-body">
                                      <div class="container-fluid">
                                        <div class="row">
                                          <div class="col-md-10 col-lg-10">
                                            <div class="form-group col-lg-5 col-md-5 ">
                                              <label for="inputPassword6" style="margin:5px;float: right; ">اسم الملحق :</label>
                                              <input type="text" id="inputPassword6" class="form-control mx-sm-3" style="width:250px;" name="name" value="{{$accessory->name}}">
                                            </div>
                                            <div class="form-group col-lg-5 col-md-5">
                                              <label for="inputPassword6" style="margin:5px;float: right; ">الصنف :</label>
                                              <input type="text" id="inputPassword6" class="form-control mx-sm-3 " style="width:250px;" name="category" value="{{$accessory->category}}">
                                            </div>
                                            <div class="form-group col-lg-5 col-md-5">
                                              <label for="inputPassword6" style="margin:5px;float: right; ">سعر البيع :</label>
                                              <input type="text" id="inputPassword6" class="form-control mx-sm-3" style="width:250px;" name="selling_price" value="{{$accessory->selling_price}}">
                                            </div>
                                            <div class="form-group col-lg-5 col-md-5">
                                              <label for="inputPassword6" style="margin:5px;float: right; ">سعر الإيجار :</label>
                                              <input type="text" id="inputPassword6" class="form-control mx-sm-3" style="width:250px;" name="rent_price" value="{{$accessory->rent_price}}">
                                            </div>
                                            <div class="form-group col-lg-5 col-md-5" style="margin-top: 10px;">
                                              <div class=" fileUpload btn btn-light">
                                                <label class="upload">
                                                  <input name="piece_img" type="file" class="form-control mx-sm-3" style="display: none;">تحميل صورة </label>
                                              </div>
                                            </div>
                                            <div class="form-group col-lg-5 col-md-5 " style="float: right; margin-right: 110px;">
                                              <label for="inputPassword6" style="float: right;margin-right: 20px;">النوع</label>
                                              <div>
                                                <input type="text" list="Type_accessory" id="inputPassword6" class="form-control mx-sm-3" style="width:250px;" name="Type_accessory" value="{{$accessory->Type_accessory}}" />
                                                <datalist id="Type_accessory">
                                                  <option value="{{$Type->name}}">{{$Type['name']}} </option>
                                                </datalist>
                                              </div>
                                            </div>

                                          </div>
                                          <div class="form-group col-md-2 col-lg-2 ">
                                            <img src="/images/accessory/{{$accessory->piece_img}}" id="image">
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer ">
                                      <button type="submit" class="btn btn-theme02 pull-left" data-dismiss="modal{{$accessory->id}}">تعديل </button>
                                    </div>
                                  </form>
                                </div>
                              </div>
                            </div>
                          </td>
                        </tr>
                        @endif
                        @endforeach
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- page end-->
              </div>
            </div>
            <!-- /row -->
          </div>
          <!-- end all contract -->
        </div>
      </div>
      <!-- end panel-body -->
    </div>
  </div>
</div> @endsection