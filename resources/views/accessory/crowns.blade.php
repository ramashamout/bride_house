@extends('master') @section('contents')
<div class="wrapper style3 ">
  <div class="title ">
    <h2>
      <a href="index.html">عرض الملحقات </a>
    </h2>
  </div>
  <div class="container">
    <div class="row">

      @foreach($accessory as $accessory)
      @if ($accessory->statue_acc=="متوفر ")
      <div class=" col-md-3 col-lg-3">

        <div class="dress-box">

          <div class="icn-main-container">
            <img id="image" src="/images/accessory/{{$accessory->piece_img}}">
          </div>
          <table class="table">
            <!-- start name accessory  -->
            <br>
            <span class="name">{{$accessory->name}}</span>
            <br>
            <!-- end name accessory  -->
            <!-- start accessory category-->
            <tr>
              <td style="text-align: center;">الفئة </td>
              <td class="size">{{$accessory->category}}</td>
            </tr>
            <!-- end accessory category -->
            <!-- start selling price  -->
            <tr>
              <td style="text-align: center;">سعر البيع </td>
              <td class="price"> {{$accessory->selling_price}} </td>
            </tr>
            <!-- end selling price  -->
            <!-- start rent price -->
            <tr>
              <td style="text-align: center;">سعر الإيجار </td>
              <td class="price">{{$accessory->rent_price}}</td>
            </tr>
            <!-- end rent price -->
            <!-- start rent price -->
            <tr>
              <td style="text-align: center;">نوع الملحق </td>
              <td class="price">{{$accessory->Type_accessory}}</td>
            </tr>
            <!-- end rent price -->
          </table>

        </div>

      </div>
      @endif
      @endforeach

    </div>

    <!-- end card dress -->
    <!-- start pagination -->

  </div>
</div>
@endsection