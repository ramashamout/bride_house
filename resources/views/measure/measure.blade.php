@extends('master') @section('contents') <div class="wrapper style3 ">
  <div class="title ">
    <h2>
      <a href="index.html">القياسات التفصيلية </a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">
              <i class="fa fa-edit" style="color: black;"></i>معلومات القياسات التفصيلية للزبون {{$design->customer->full_name}}</a>
          </li>

        </ul>
      </div>
      <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information cotract -->
          <div id="overview" class="tab-pane active">
            <form style="padding-top: 10px; "  method="POST" action="{{ route('measure.store') }}" enctype="multipart/form-data" >
              @csrf
            <div class="row mt">
              <div class="col-md-9 col-lg-9">

                  <br>
                  <div class="form-group col-lg-4 col-md-4" style="float: right;">
                    <input type="text"class="form-control mx-sm-3" placeholder="محيط الصدر "  name="chest_circumference">
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <input type="text" class="form-control mx-sm-3" placeholder="محيط الخصر" name="waistline">
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <input type="text" class="form-control mx-sm-3" placeholder="محيط الورك "  name="hip_circumference">
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <input type="text"class="form-control mx-sm-3" placeholder="عرض الكتف "name="shoulder_width">
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <input type="text"  class="form-control mx-sm-3"placeholder="طول الكتف "  name="shoulder_length">
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <input type="text"  class="form-control mx-sm-3"placeholder="طول الظهر " name="back_length">
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <input type="text"  class="form-control mx-sm-3" placeholder="طول النهد "name="breast_length"/>
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <input  type="text"  class="form-control mx-sm-3" placeholder="المسافة بين النهدين "  name="distance_breasts">
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <input type="text" class="form-control mx-sm-3"  placeholder="طول الكم "name="Sleeve_Length">
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <input type="text"class="form-control mx-sm-3"  placeholder="محيط أسوارة الكم " name="cuff_bracelet">
                  </div>

                  <div class="form-group col-lg-4 col-md-4">
                    <input type="text"  class="form-control mx-sm-3"  placeholder="طول التنورة "  name="skirt_length"/>
                  </div>
{{--                    <div class="form-group" style=" position: relative; left: 100px;">--}}
{{--                                      <a type="button" class="btn btn-theme02 pull-left" href="/show_measure/{{$id}}">عرض المقاس التفصيلي </a>--}}
{{--                   </div>--}}
                  <div>
                    <input type="hidden" name="design_id" value="{{$id}}">
                  </div>

              </div>
              <div class="col-md-3 col-lg-3 ">
                <div style="margin-right: 20px;">
                  <img src="/images/16.jpg" id="image">
                </div>
              </div>
              <div class=" col-lg-4 col-md-4">
                <button type="submit" class="btn btn-theme02 pull-left" style="margin-left: 50px;margin-top: -50px;">حفظ</button>
              </div>
            </div>
            </form>
          </div>
          <!-- end information contract -->

        </div>
      </div>
      <!-- end panel-body -->
    </div>
  </div>
</div> @endsection
