 @extends('master') @section('contents') <div class="wrapper style3 ">
  <div class="title ">
    <h2>
      <a href="index.html">القياسات التفصيلية </a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
             <a data-toggle="tab" href="#edit">القياسات التفصيلية للزبون {{$design->customer->full_name}}</a>
          </li>

        </ul>
      </div>

  <div id="edit" class="tab-pane">
            <div class="row mt">
              <div class="col-md-12">
                <section class="task-panel tasks-widget">
                  <div class="panel-body">
                    <div class="task-content">
                      <ul class="task-list">


                        <li>

                          <div class="task-title">
                            <span class="task-title-sp">
                              <i class="fa fa-circle" style="font-size:10px"></i>&nbsp;&nbsp; {{$design->customer->full_name}} <a href="#" id="mytooltip" data-toggle="tooltip" title="" data-placement="left">
                                <i class="fa fa-calendar " style="font-size:15px ; color:black; "></i>
                              </a>
                            </span>
                            <div class="pull-left hidden-phone">
                               <a href="{{ route('measure.edit',$measure->id)}}" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#basicModal{{$measure->id}}">
                                <i class=" fa fa-pencil"></i>
                              </a>
                              <!-- start madal edit notes -->
                                  <div class="modal fade" id="basicModal{{$measure->id}}" tabindex="-1" role="  dialog" aria-labelledby="basicModal{{$measure->id}}" aria-hidden="true">
                                <div class="modal-dialog ">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title centered" id="myModalLabel">تعديل المقاسات التفصيلية </h4>
                                    </div>
                                          <form method="post" action="{{ route('measure.update', $measure->id) }}" enctype="multipart/form-data"  >
                                       @method('PATCH')
                                      @csrf

                                      <div class="modal-body">

                                      <div class="form-group">
                                        <label for="inputPassword6">محيط الصدر :</label>
                                        <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="chest_circumference" value="{{$measure->chest_circumference}}"/>
                                      </div>
                                      <div class="form-group">
                                         <label for="inputPassword6">محيط الخصر  :</label>
                                        <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="waistline" value="{{$measure->waistline}}"/>
                                      </div>
                                      <div class="form-group">
                                        <label for="inputPassword6">محيط الورك  :</label>
                                        <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="hip_circumference" value="{{$measure->hip_circumference}}"/>
                                      </div>
                                      <div class="form-group">
                                        <label for="inputPassword6">عرض الكتف :</label>
                                        <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="shoulder_width" value="{{$measure->shoulder_width}}"/>
                                      </div>
                                      <div class="form-group">
                                        <label for="inputPassword6">طول الكتف :</label>
                                        <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="shoulder_length" value="{{$measure->shoulder_length}}"/>
                                      </div>
                                      <div class="form-group">
                                        <label for="inputPassword6">طول الظهر :</label>
                                        <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="back_length" value="{{$measure->back_length}}"/>
                                      </div>
                                      <div class="form-group">
                                       <label for="inputPassword6">طول النهد :</label>
                                        <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="breast_length" value="{{$measure->breast_length}}" />
                                      </div>
                                      <div class="form-group">
                                       <label for="inputPassword6">المسافة بين النهدين :</label>
                                        <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"   name="distance_breasts" value="{{$measure->distance_breasts}}"/>
                                      </div>
                                      <div class="form-group">
                                        <label for="inputPassword6">طول الكم :</label>
                                        <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="Sleeve_Length" value="{{$measure->Sleeve_Length}}"/>
                                      </div>
                                      <div class="form-group">
                                          <label for="inputPassword6">محيط أسوارة الكم :</label>
                                        <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="cuff_bracelet" value="{{$measure->cuff_bracelet}}"/>
                                      </div>

                                       <div class="form-group">
                                      <div class="form-group">
                                       <label for="inputPassword6">طول التنورة :</label>
                                        <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="skirt_length" value="{{$measure->skirt_length}}"/>
                                      </div>

                                      </div>
                                      <div class="modal-footer ">
                                        <button type="submit" class="btn btn-theme02 pull-left" data-dismiss="modal{{$measure->id}}">تعديل </button>
                                      </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </div>

                              <!-- end madal edit notes -->

                              <div class="col-lg-8 col-md-8">
                                <form action="{{ route('measure.destroy', $measure->id)}}" method="post">
                                  @csrf
                                  @method('DELETE')
                                  <button class="btn btn-danger btn-xs " class="button"
                                  method="post" href="{{ route('measure.destroy', $measure->id)}}" style="margin-right:-5px; ">
                                  <i class="fa fa-trash-o "></i>
                                  </button>
                                </form>
                              </div>

                            </div>
                          </div>
                        </li>


                      </ul>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
            </div>
</div> @endsection
