<html>

<body>
    <div class="row">
        <div class="col-sm-8 offset-sm-2">
            <h1 class="display-3">Add Design</h1>
            <div>

                <form method="post" action="{{ route('search_piece') }}" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="name"> Name:*</label>
                        <input type="text" class="form-control" name="name" />
                    </div>
                    <button type="submit" class="btn btn-primary">Search</button>
                </form>
            </div>
        </div>
    </div>
</body>

</html>