@extends('master') @section('contents') <div class="wrapper style3 ">
  <div class="title ">
    <h2>
    <a href="index.html">إدارة الفساتين</a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          
          <li class="active">
            <a data-toggle="tab" href="#edit">جميع القطع</a>
          </li>
        </ul>
      </div> <?php
      use Carbon\Carbon;
      ?>
      <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information cotract -->
          <div id="overview" class="tab-pane ">
            <form style="padding-top: 10px; " method="post" action="{{ route('clothe.store') }}" enctype="multipart/form-data"> @csrf <div class="row mt">
              <div class="col-md-9 col-lg-9">
                <br>
                <div class="form-group col-lg-6 col-md-6" style="float: right;">
                  <label for="inputPassword6">اسم الفستان :</label>
                  <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="name" />
                </div>
                <div class="form-group col-lg-6 col-md-6">
                  <label for="inputPassword6">سعر البيع :</label>
                  <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="selling_price" />
                </div>
                <div class="form-group col-lg-6 col-md-6">
                  <label for="inputPassword6">سعر الآجار :</label>
                  <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="rent_price" />
                </div>
                <div class="form-group col-lg-6 col-md-6">
                  <label for="inputPassword6">اسم المصمم:</label>
                  <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="designer_name" />
                </div>
                <div class="form-group col-lg-6 col-md-6" style="float: right;">
                  <div>
                    <label for="inputPassword6">النوع :</label>
                    <input type="text" list="clothes_type" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="clothes_type" />
                    <datalist id="clothes_type"> @foreach($Type1 as $Type1) @if($Type1->type_piece == 'clothe') <option value="{{$Type1->name}}">{{$Type1->name}} </option> @endif @endforeach </datalist>
                  </div>
                </div>
                <div class="form-group col-lg-6 col-md-6">
                  <label for="inputPassword6">قمية الخصم:</label>
                  <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="discount" />
                </div>
                <script type="text/javascript">
                $(#clothes_type).select2({
                placeholder: "اسم الفستان ",
                allowClear: true
                });
                </script>
                <div class="form-group col-lg-6 col-md-6">
                  <label for="inputPassword6">لون الفستان :</label>
                  <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="color" />
                </div>
                <div class="form-group col-lg-4 col-md-4" style="float: right;">
                  <label for="inputPassword6">القياس :</label>
                  <div class="multi_select_box">
                    <select class="multi_select" class="form-control mx-sm-3" name="size" data-selected-text-format="count >3"> @foreach($si as $siz) <option value="{{$siz->value}}">{{$siz->value}}</option> @endforeach </select>
                  </div>
                </div>
                <script type="text/javascript">
                $(#size).select2({
                placeholder: "اسم الفستان ",
                allowClear: true
                });
                </script>
                <div class="form-group col-lg-6 col-md-6" style="margin-top:20px;">
                  <div class=" fileUpload btn btn-light">
                    <label class="upload">
                      <input name="piece_img" type="file" class="form-control mx-sm-3">تحميل صورة </label>
                    </div>
                    <div class=" fileUpload btn btn-light">
                      <label class="upload">
                      <input name="other_image[]" type="file" class="form-control mx-sm-3" multiple> صور آخرى للفستان </label>
                    </div>
                  </div>
                </div>
                <div class="col-md-3 col-lg-3 ">
                  <div style="margin-right: 20px;">
                    <img src="images/set_dresses.jpg" id="image">
                  </div>
                </div>
                <div class=" col-md-4 col-lg-4 ">
                  <button type="submit" class="btn btn-theme02 pull-left" style="margin-left: 50px; margin-top: -45px;">حفظ</button>
                </div>
              </div>
            </form>
          </div>
          <!-- end information contract -->
          <!-- start all contract -->
          <div id="edit" class="tab-pane active">
            <div class="row mb">
              <div class="col-lg-12 col-md-12">
                <!-- page start-->
                <div class="content-panel">
                  <div class="adv-table">
                    <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="hidden-table-info">
                      <thead>
                        <tr>
                          <th>إسم الفستان</th>
                          <!-- <th>إسم المصمم</th> -->
                          <th class="hidden-phone">سعر الإيجار</th>
                          <th class="hidden-phone">سعر البيع</th>
                          <th class="hidden-phone"> سعر البيع قبل الخصم</th>
                          <th class="hidden-phone"> قيمة الخصم </th>
                          <th class="hidden-phone"> القياس </th>
                          <th class="hidden-phone"> النوع </th>
                          <th class="hidden-phone"> الصورة </th>
                          <th class="hidden-phone">إرسال القطعة </th>
                          <th class="hidden-phone">حالة القطعة </th>
                          <th class="hidden-phone"></th>
                        </tr>
                      </thead>
                      <tbody>
                       @foreach($clothe as $clothe)
                        @if($clothe->statue!="مستودع" && $clothe->statue!="مباع")     
                        <tr class="gradeU">
                        <td>{{$clothe->name}}</td>
                        <!-- <td>{{$clothe->designer_name}}</td> -->
                        <td class="hidden-phone">{{$clothe->rent_price}}</td>
                        <td class="center hidden-phone">{{$clothe->selling_price}}</td>
                        <td class="center hidden-phone">{{$clothe->old_price}}</td>
                        <td class="center hidden-phone">{{$clothe->discount}}</td>
                        <td class="center hidden-phone">{{$clothe->size}}</td>
                        <td class="center hidden-phone">{{$clothe->clothes_type}}</td>
                        <td class="center hidden-phone">
                          <img src="/images/clothe/{{$clothe->piece_img}}" id="image2">
                        </td>
                        <td class="center hidden-phone">
                         @if($clothe->statue == 'متوفر ')
                          <a href="{{ route('gotomazad',$clothe->id)}}" class="btn btn-default btn-xs">
                          <i class=" fa fa-share"></i>
                        </a> @endif </td>
                        <td class="center hidden-phone">
                          {{$clothe->statue}}
                           @if($clothe->statue != "متوفر " && $clothe->statue!="في انتظار الزبون" && $clothe->statue!="مزاد")
                            <form action="/update_statue_c/{{$clothe->id}}" method="GET">
                            <button class="btn btn-warning btn-xs" href="/update_statue_c/{{$clothe->id}}">
                            <i class="fa fa-refresh "></i>
                            </button>
                          </form> @endif
                        </td>
                        
                        <td >
                          <div class="col-lg-4 col-md-4">
                            <a href="{{ route('clothe.edit',$clothe->id)}}" class="btn btn-success btn-xs" data-toggle="modal" data-target="#basicModals{{$clothe->id}}">
                              <i class=" fa fa-send"></i>
                            </a>
                          </div>
                          <div class="col-lg-4 col-md-4 ">
                            <form action="{{ route('clothe.destroy', $clothe->id)}}" method="post"> @csrf @method('DELETE') <button class="btn btn-danger btn-xs" method="post" href="{{ route('clothe.destroy', $clothe->id)}}">
                              <i class="fa fa-trash-o "></i>
                              </button>
                            </form>
                          </div>
                          <div class="col-lg-4 col-md-4">
                            <a href="{{ route('clothe.edit',$clothe->id)}}" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#basicModal{{$clothe->id}}">
                              <i class=" fa fa-pencil"></i>
                            </a>
                          </div>
                          
                          <!-- start madal edit notes -->
                          <div class="modal fade" id="basicModal{{$clothe->id}}" tabindex="-1" role="  dialog" aria-labelledby="basicModal{{$clothe->id}}" aria-hidden="true">
                            <div class="modal-dialog  modal-lg">
                              <div class="modal-content">
                                <div class="modal-header">
                                  <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  <h4 class="modal-title centered" id="myModalLabel"> تعديل الفستان </h4>
                                </div>
                                   <form method="post" action="{{ route('clothe.update', $clothe->id) }}" enctype="multipart/form-data"> @method('PATCH') @csrf <div class="modal-body">
                                      <div class="container-fluid">
                                        <div class="row">
                                          <div class="col-md-10 col-lg-10">
                                            <div class="form-group col-lg-5 col-md-5 ">
                                              <label for="inputPassword6" style="margin:5px;float: right; ">اسم الفستان</label>
                                              <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" style="width:250px;" name="name" value="{{$clothe->name}}">
                                            </div>
                                            <div class="form-group col-lg-5 col-md-5">
                                              <label for="inputPassword6" style="margin:5px;float: right; ">اسم المصمم</label>
                                              <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" style="width:250px;" name="designer_name" value="{{$clothe->designer_name}}">
                                            </div>
                                            <div class="form-group col-lg-5 col-md-5">
                                              <label for="inputPassword6" style="margin:5px;float: right; ">سعر البيع</label>
                                              <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" style="width:250px;" name="selling_price" value="{{$clothe->selling_price}}">
                                            </div>
                                            <div class="form-group col-lg-5 col-md-5">
                                              <label for="inputPassword6" style="margin:5px;float: right; ">سعر البيع قبل الخصم</label>
                                              <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" style="width:250px;" name="old_price" value="{{$clothe->old_price}}">
                                            </div>
                                            <div class="form-group col-lg-5 col-md-5">
                                              <label for="inputPassword6" style="margin:5px;float: right; ">قيمة الخصم</label>
                                              <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" style="width:250px;" name="discount" value="{{$clothe->discount}}">
                                            </div>
                                            <div class="form-group col-lg-5 col-md-5">
                                              <label for="inputPassword6" style="margin:5px;float: right; ">سعر الإيجار</label>
                                              <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" style="width:250px;" name="rent_price" value="{{$clothe->rent_price}}">
                                            </div>
                                            <div class="form-group col-lg-5 col-md-5">
                                              <label for="inputPassword6" style="margin:5px;float: right; ">لون الفستان </label>
                                              <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" style="width:250px;" name="color" value="{{$clothe->color}}">
                                            </div>
                                            <div class="form-group col-lg-5 col-md-5" style="float: right; margin-right: 93px;">
                                              <label for="inputPassword6" style="float: right;margin-right: 20px;">القياس</label>
                                              <div class="multi_select_box">
                                                <select class="multi_select" class="form-control mx-sm-3" name="size" multiple data-selected-text-format="count >3"> @foreach($si as $siz) <option value="{{$siz->value}}">{{$siz->value}}</option> @endforeach </select>
                                              </div>
                                            </div>
                                             <div class="form-group col-lg-5 col-md-5" style="margin-top: 20px;">
                                          <div class=" fileUpload btn btn-light">
                                            <label class="upload">
                                            <input name="other_image[]" type="file" class="form-control mx-sm-3" multiple> صور آخرى للفستان </label>
                                          </div>
                                        </div>
                                        <div class="form-group col-lg-5 col-md-5 "style="float: right; margin-right: 110px;">
                                          <label for="inputPassword6" style="float: right;margin-right: 20px;">النوع</label>
                                          <div>
                                            <input type="text" list="clothes_type" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="clothes_type" value="{{$clothe->clothes_type}}" />
                                            <datalist id="clothes_type">
                                            <option value="{{$Type1->name}}">{{$Type1['name']}} </option>
                                            </datalist>
                                          </div>
                                        </div>
                                          </div>
                                          <div class="form-group col-md-2 col-lg-2 ">
                                            <img src="/images/clothe/{{$clothe->piece_img}}" id="image">
                                            <div class=" fileUpload btn btn-light" style="margin-right:50px; margin-top: 15px;
                                              ">
                                              <label class="upload">
                                                <input name="piece_img" type="file" class="form-control mx-sm-3" style="display: none;">تحميل صورة </label>
                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="modal-footer ">
                                      <button type="submit" class="btn btn-theme02 pull-left" data-dismiss="modal{{$clothe->id}}">تعديل </button>
                                    </div>
                                  </form>
                              </div>
                            </div>
                          </div>
                          <!-- start madal edit notes -->
                          <div class="modal fade" id="basicModals{{$clothe->id}}" tabindex="-1" role="  dialog" aria-labelledby="basicModals{{$clothe->id}}" aria-hidden="true">
                            <div class="modal-dialog  modal-lg">
                              <div class="modal-content" style="width: 700px;">
                                <div class="modal-header">
                                  <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                                  <h4 class="modal-title centered" id="myModalLabel"> إرسال إشعار </h4>
                                </div>
                                <form style="padding-top: 10px; " method="get" action="{{ route('send.push-notification') }}" enctype="multipart/form-data"> @csrf <div class="row mt">
                                  <div class="col-md-9 col-lg-9">
                                    <br>
                                    <div class="form-group col-lg-9 col-md-9" style="float: right;margin-right: 100px;">
                                      <label for="inputPassword6" style="float: right;"> عنوان الإشعار :</label>
                                      <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="title" />
                                    </div>
                                    <br>
                                    <div class="form-group col-lg-9 col-md-9" style="float: right;margin-right: 100px;">
                                      <label for="inputPassword6" style="float: right;">مضمون الإشعار :</label>
                                      <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="body" />
                                    </div>
                                  </div>
                                  <div class="col-md-3 col-lg-3 ">
                                    <div style="margin-right: 20px;">
                                      <img src="images/set_dresses.jpg" id="image">
                                    </div>
                                  </div>
                                  <div class=" col-md-4 col-lg-4 ">
                                    <button type="submit" class="btn btn-theme04 pull-left" style="margin-left: 50px; margin-top: -45px;">حفظ </button>
                                  </div>
                                </div>
                              </form>
                            </div>
                          </div>
                        </div>
                      </td>
                      
                    </tr> @endif @endforeach </tbody>
                  </table>
                </div>
              </div>
              <!-- page end-->
            </div>
          </div>
          <!-- /row -->
        </div>
        <!-- end all contract -->
      </div>
    </div>
    <!-- end panel-body -->
  </div>
</div>
</div> @endsection