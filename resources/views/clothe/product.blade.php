<!DOCTYPE html>
<html lang="ar-sa" dir="rtl">
  <head>
    <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>Laravel</title>
        <link rel="stylesheet" type="text/css" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <!-- login css -->
    <link rel="stylesheet" href="login/style.css">
    <!-- Fonts and font-awesome-->
    <link rel="stylesheet" type="text/css" href="/fonts/DroidKufi/EARLY_ACCESS.css">
    <link rel="stylesheet" type="text/css" href="/css/fonts/AlexBrush/AlexBrush-Regular.ttf">
    <link href="/lib/font-awesome/css/font-awesome.css" rel="stylesheet" >
    <!-- bootstrap css -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">

    <!-- template css -->
    <link rel="stylesheet" href="/css/style.css" >
    <link rel="stylesheet" href="/css/style-desktop.css" >
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css">
    <!-- advance table -->
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.13.18/css/bootstrap-select.min.css">
    <link href="lib/advanced-datatable/css/demo_page.css" rel="stylesheet" />
    <link href="lib/advanced-datatable/css/demo_table.css" rel="stylesheet" />
    <link rel="stylesheet" href="lib/advanced-datatable/css/DT_bootstrap.css" />
    <!-- calendar css -->
    <link rel="stylesheet" href="/package/dist/fullcalendar.min.css" />
    <link rel="stylesheet" type="text/css" href="/lib/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="/lib/jquery.dataTables.min.css">

    <!--  -->
    <!--select accessory css -->
    <link rel="stylesheet" type="text/css" href="/css/bootstrap-select.css">

  </head>
  <?php
  use Carbon\Carbon;
  use App\Models\Appointment;
  use App\Notifications\ReminderNotification;
  use App\Models\Clothe;
   use Spatie\Permission\Models\Role;
  $Appointments=Appointment::where('statue_app', 'LIKE','قيد الانتظار')->get();
  foreach ($Appointments as $Appointment) {
  if($Appointment->statue_app=='قيد الانتظار' &&  $Appointment->read==0){
  $result=['type'=>'app','app'=>$Appointment];
  Auth::user()->notify(new ReminderNotification($result));
  $Appointment->read=1;
  $Appointment->save();
  }
  }

$roles=Role::all();
  ?>

  <body class="homepage">
  
    <div id="header-wrapper" class="wrapper">
      <div id="header">
        <!-- Logo -->
        <div id="logo">
          <h1><a href="index.html"> Bride House</a></h1>
        </div>
        <!-- Nav -->
        <!-- Nav -->
        <div class="col-lg-3 col-md-3" >
          <nav class="nav1 notify-row"   id="top_menu">
            <!--  notification start -->
            <ul >
              @auth
              @hasanyrole($roles)
              <li id="header_notification_bar" class="dropdown" > <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#" >
                <i class="fa fa-bell"></i>


                <span class="badge bg-danger num" >{{auth()->user()->unreadNotifications->count()}}</span>

                <ul class="dropdown-menu extended notification">
                  <div class="notify-arrow notify-arrow-yellow"></div>
                  <li>
                    <p class="yellow text-center">الإشعارات</p>

                    <script src="{{ asset('js/app.js') }}"></script>
                  </li>
                  <li>
                    <a  action="post" href=""></a>
                  </li>
                  <li>
                   @foreach(auth()->user()->unreadNotifications as $appRemider)
                    @if($appRemider->data['type']=="app"  )
                    @if($appRemider->data['app']['statue_app']=='قيد الانتظار')


                    <a href="/app_s/{{$appRemider->data['app']['id']}}" title="تذكير موعد ">
                      <span><i class="fa fa-bell-o  btn-xs" style="background-color: #DDA0DD;"></i></span>
                     تم حجز موعد من قبل {{$appRemider->data['app']['title']}} &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                     <br>التوقيت : {{$appRemider->data['app']['date']}}
                      والساعة {{$appRemider->data['app']['time']}}
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <br>
                     الرجاء التثبيت أو الحذف
                       &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>

                    @endif
                    @endif
                    @endforeach
                    @foreach(auth()->user()->unreadNotifications as $cleanRemider)
                      @if($cleanRemider->data['type']=="clean"  )





                    <a  href="{{ route('markAsCleanRead',$cleanRemider->id)}}" title="تذكير انتهاء عملية التنظيف ">
                      <span><i class="fa fa-bell-o  btn-xs" style="background-color: #DDA0DD;"></i></span>
                      لقد انتهت عملية تنظيف
                      {{$cleanRemider->data['clean']['name_product']}}
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


                    </a>

                    @endif

                    @endforeach

                    @foreach(auth()->user()->unreadNotifications as $pieceReminder)
                    @if($pieceReminder->data['type']=="action")
                    <a href="{{ route('markAsRead',$pieceReminder->id)}}"  >
                      <span><i class="fa fa-bell-o  btn-xs" style="background-color: #DDA0DD;"></i></span>
                      لقد رسى مزاد القطعة {{$pieceReminder->data['clothe']['name']}}
                        <br>
                        على الزبونة : {{\App\Models\User::find($pieceReminder->data['action']['user_id'])->name}}

                       بسعر : {{$pieceReminder->data['action']['price']}}

                    </a>
                    @endif
                    @endforeach

                    @foreach(auth()->user()->unreadNotifications as $mainRemider)
                    @if($mainRemider->data['type']=="maintenance"  )




                    <a  href="{{ route('markAsMainRead',$mainRemider->id)}}" title="تذكير انتهاء عملية الصيانة ">
                      <span><i class="fa fa-bell-o  btn-xs" style="background-color: #DDA0DD;"></i></span>
                      لقد انتهت عملية الصيانة
                      {{$mainRemider->data['maintenance']['name_product']}}
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;


                    </a>

                    @endif
                    @endforeach
                    @foreach(auth()->user()->unreadNotifications as $brideReminder)
                    @if($brideReminder->data['type'] =="bride")
                    <a href="{{ route('markAsRead',$brideReminder->id)}}" title="تذكير Bride">
                      <span ><i class="fa fa-bell-o  btn-xs" style="background-color: #DDA0DD;"></i></span>
                        تذكير تطريز للزبونة :  {{$brideReminder->data['customer_name']}}&nbsp;{{$brideReminder->data['date']}}&nbsp;
                        <br>
                     التطريز : {{$brideReminder->data['emboridery']}}
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                    @endif
                    @endforeach
                    @foreach(auth()->user()->unreadNotifications as $pieceReminder)
                    @if($pieceReminder->data['type']=="piece")
                    <a href="{{ route('markAsRead',$pieceReminder->id)}}" title="تذكير فستان " >
                      <span><i class="fa fa-bell-o  btn-xs" style="background-color: #DDA0DD;"></i></span>
                      تذكير قطعة للزبونة :   {{$pieceReminder->data['customer_name']}}{{$pieceReminder->data['date']}}&nbsp;
                        <br>
                      المحتوى : {{$pieceReminder->data['content']}}&nbsp;
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    </a>
                    @endif
                    @endforeach
                    @foreach(auth()->user()->unreadNotifications as $appRemider)
                    @if($appRemider->data['type']=="appointment"  )



                    <a href="{{ route('markAsRead',$appRemider->id)}}"title="تذكير حذف موعد من قبل الزبونة ">
                      &nbsp;&nbsp; <span><i class="fa fa-bell-o  btn-xs" style="background-color: #DDA0DD;"></i></span>&nbsp;لقد قامت الزبونة
                      {{$appRemider->data['appointment']['title']}} التوقيت : {{$appRemider->data['appointment']['date']}}
                      <br>
                      والساعة {{$appRemider->data['appointment']['time']}}بحذف موعدها
                      &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      <br>
                      </button> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                      &nbsp;

                    </a>





                    @endif
                    @endforeach
                    @foreach(auth()->user()->unreadNotifications as $contractReminder)
                    @if($contractReminder->data['type'] =="contract")


                    <a href="{{ route('markConAsRead',$contractReminder->id)}}" title="تذكير contract">
                      <span><i class="fa fa-bell-o  btn-xs" style="background-color: #DDA0DD;"></i></span>
                        لقد انتهى عقد آجار الفستان {{\App\Models\Piece::all()->where('id',$contractReminder->data['contract']['piece_id'])->first()->name}}


                    </a>

                    @endif
                    @endforeach
                    @foreach(auth()->user()->unreadNotifications as $contractReminder)
                    @if($contractReminder->data['type'] =="warning")


                    <a href="{{ route('markAsRead',$contractReminder->id)}}" title="تذكير contract">
                      <span><i class="fa fa-bell-o  btn-xs" style="background-color: #DDA0DD;"></i></span>
                        {{$contractReminder->data['content']}}


                    </a>

                    @endif
                    @endforeach
                    <div class="con">
                    </div>
                    <script>
                    window.Echo.private('contract.{{auth()->id()}}').listen('ContractEvent',(e)=>{
                    console.log(e.count)
                    document.getElementsByClassName('num')[0].innerHTML= e.count;
                    // document.getElementsByClassName('con')[0].innerHTML+=e.contracts
                    // e.contracts.forEach((element) => document.getElementsByClassName('con')[0].innerHTML=element );
                    const arraySparse = e.contracts;// ["Accounting", "Big Data", "Business", "Category", "Concept", "Consultant", "Consumer", "Corporate", "Cost", "Customer", "Development", "Digital", "Distribution", "Due Diligence", "Financial", "Global Sourcing", "Go-to-market", "Growth", "Improvement", "Information", "Technology", "Innovation", "Lean", "Management", "Manufacturing", "Marketing", "Merchandising", "Mergers & Acquisitions", "Operations", "Organization / Organizational", "Performance", "Portfolio", "Post-merger", "Pricing", "Procurement", "Product", "Profitability", "Purchasing", "Restructuring", "Retail", "Revenue", "Sales", "Strategy", "Supply Chain", "Sustainable", "Technology", "Transformation", "Turnaround", "Zero-based", "Budgeting"];
                    console.log(e.contracts);
                    function* iterate_object(o) {
                    var keys = Object.keys(o);
                    for (var i=0; i<keys.length; i++) {
                    yield [keys[i], o[keys[i]]];
                    }
                    }
                    for (var [key, val] of iterate_object(arraySparse)) {
                    console.log(key, val);
                    for (var [key, val] of iterate_object(val)) {
                    console.log(key, val);
                    if(key == 'id'){
                    // document.getElementsByClassName('con')[0].innerHTML+=val ;
                    document.getElementsByClassName('con')[0].innerHTML+=' <a href="" title="تذكير contract">\n' +
                      '                                        <span ><i class="fa fa-bell-o  btn-xs" style="background-color: #DDA0DD;"></i></span>\n' +
                      'contract number '+ val +
                    '                                    </a>'
                    }
                    // break;
                    }
                    }
                    // for (let [key, value] of example) { console.log(value); } // error
                    // console.log([...example]);
                    })
                    // const arraySparse = [1, 3,, 7];
                    // for (const element of arraySparse) {
                    //     document.getElementsByClassName('con')[0].innerHTML+={element} ;
                    // }
                    </script>
                  </li>
                </ul>
              </li>
               @endhasanyrole
              <li class="dropdown">
                <a data-toggle="dropdown" class="dropdown-toggle" href="index.html#">
                  <i class="glyphicon glyphicon-user"></i>
                </a>
                <ul class="dropdown-menu extended tasks-bar" >
                  <div class="notify-arrow notify-arrow-green"></div>
                  <li>
                    <p class="green text-center">معلومات الملف الشخصي</p>
                  </li>
                  <li class="text-center" style="margin-right: 60px;">
                    <img  class=" img-circle" src="images/33.png"   width="80">
                  </li>
                  <div class="name_profile">
                    <li class="text-center">{{auth()->user()->name}}</li>
                  </div>
                  <li class="text-right" >
                    <a href="#" data-toggle="modal" data-target="#Mouna{{auth()->user()->id}}">تعديل معلومات الملف الشخصي &nbsp;<button class="btn btn-primary btn-xs"><i class="fa fa-pencil"></i>
                      </button>
                    </a></li>
                    <!-- Modal -->
                  </ul>
                  <div class="modal fade" id="Mouna{{auth()->user()->id}}" tabindex="-1" role="  dialog" aria-labelledby="Mouna{{auth()->user()->id}}" aria-hidden="true">
                    <div class="modal-dialog ">
                      <div class="modal-content">
                        <div class="modal-header">
                          <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                          <h4 class="modal-title centered" id="myModalLabel">تعديل المستخدم </h4>
                        </div>
                        <form method="GET" action="/update_user/{{auth()->user()->id}}" enctype="multipart/form-data"  >
                          @csrf
                          <div class="modal-body">
                            <div class="container-fluid">
                              <div class="row ">
                                <div class="col-md-12 col-lg-12">
                                  <div class="form-group">
                                    <div >
                                      <label for="inputPassword6" >اسم المستخدم :</label>
                                      <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"  name="name" value="{{auth()->user()->name}}" />
                                    </div>
                                  </div>
                                  <div class="col-md-12 col-lg-12">
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="modal-footer ">
                            <button type="submit" class="btn btn-theme02 pull-left" data-dismiss="">تعديل </button>
                          </div>
                        </form>
                      </div>
                    </div>
                  </div>
                </li>
                <li>
                  <form method="POST" action="{{ route('logout') }}" >
                    @csrf
                    <div style="margin-top: 30px;">
                      <a class="logout" href="{{ route('logout') }}" onclick="event.preventDefault();
                        this.closest('form').submit(); " role="button" >
                        <i class="glyphicon glyphicon-log-out"></i>
                      </a></div>
                    </form></li>
                    @else
                    <li ><a class="login" href="logi"><i class="glyphicon glyphicon-log-in"></i> </a></li>
                    @endauth

                  </ul>
                  <!--  notification end -->
                </nav>
              </div>
              <div class="col-lg-9 col-md-9">
                @if (Auth::check())
                <nav id="nav">
                  <ul>
                    <li>
                      <a href="/
                        ">الرئيسية
                      </a>
                    </li>

                    <li>
                      <a href="">لوحة التحكم  <i class="fa fa-caret-down"></i>
                      </a>
                      <ul>
                        <li></li>
                        @can('إدارة الفساتين')
                        <li>
                          <a href="/add_dress">إدارة الفساتين
                          </a>

                        </li>
                        @endcan
                        @can('إدارة التقارير')
                        <li>
                          <a href="">عرض التقارير <i class="fa fa-caret-down"></i></a>
                          <ul>
                            <li>
                              <a href="/report">تقارير البيع </a>
                            </li>
                            <li>
                              <a href="/reports">تقارير الشراء</a>
                            </li>
                          </ul>



                        </li>
                        @endcan
                        @can('إدارة الملحقات ')
                        <li>
                          <a href="/Accessory">إدارة الملحقات
                          </a>


                        </li>
                        @endcan
                        @can('إدارة المستخدمين')
                        <li>
                          <a href="/users/create">إدارة المستخدمين
                          </a>

                        </li>
                        @endcan
                        @can('إدارة الفواتير ')
                        <li>
                          <a href="/biginvoice">إدارة الفواتير
                          </a>


                        </li>
                        @endcan
                        @can('إدارة التقارير ')
                        <li>
                          <a href="">عرض التقارير <i class="fa fa-caret-down"></i></a>
                          <ul>
                            <li>
                              <a href="/report">تقارير البيع </a>
                            </li>
                            <li>
                              <a href="/reports">تقارير الشراء</a>
                            </li>
                          </ul>



                        </li>
                        @endcan
                        @can('إدارة الصلاحيات ')
                        <li>
                          <a href="/roles/create">إدارة الصلاحيات
                          </a>
                        </li>
                        @endcan
                        @can('إدارة المواعيد ')
                        <li>
                          <a href="/ap">إدارة المواعيد
                          </a>
                        </li>
                        @endcan
                        @can('إدارة المستودع ')
                        <li>
                          <a href="/show_store">إدارة المستودع
                          </a>
                        </li>
                        @endcan
                        @can('إدارة الأنواع ')
                        <li>
                          <a href="/add_Type">إدارة الأنواع
                          </a>
                        </li>
                        @endcan
                        @can('إدارة التطبيق ')
                        <li>
                          <a href="/add_Banner">إدارة  التطبيق
                          </a>
                        </li>
                        @endcan
                      </ul>
                    </li>

                    <li>
                      <a href="">المتجر <i class="fa fa-caret-down"></i>
                      </a>
                      <ul>
                        <li></li>
                        <li>
                          <a href="">فساتين <i class="fa fa-caret-left"></i>
                          </a>
                          <ul>

                            @foreach($Type as $Typee)
                            @if($Typee['type_piece'] == 'clothe')
                            <li>
                              <a  href="{{ route('filter',$Typee->name)}}">{{$Typee['name']}}</a>
                            </li>
                            @endif
                            @endforeach

                          </ul>

                        </li>
                        <li>
                          <a href="">ملحقات <i class="fa fa-caret-left"></i>
                          </a>
                          <ul>

                            @foreach($Type as $Type)
                            @if($Type['type_piece'] == 'accessory')
                            <li>
                              <a  href="/crowns/{{$Type->name}}">{{$Type['name']}}</a>
                            </li>
                            @endif
                            @endforeach


                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="">العمليات <i class="fa fa-caret-down"></i>
                      </a>
                      <ul>
                        @can('التصميم')
                        <li>
                          <a href="/design_t">التصميم</a>
                        </li>
                        @endcan
                        @can('الصيانة')
                        <li>
                          <a href="/maintenance">الصيانة</a>
                        </li>
                        @endcan
                        @can('التنظيف' )
                        <li>
                          <a href="/addclean">التنظيف</a>
                        </li>
                        @endcan
                      </ul>
                    </li>
                    <li>
                      <a href="/all_customer">العملاء</a>
                    </li>

                    @can('التذكيرات' )
                    <li>
                      <a href="/notification">التذكيرات </a>
                    </li>
                    @endcan
                    @can('العقود')
                    <li>
                      <a href="/contract">العقود</a>
                    </li>
                    @endcan

                  </ul>
                </nav>
                @else
                <nav id="nav">
                  <ul>
                    <li>
                      <a href="/
                        ">الرئيسية
                      </a>
                    </li>

                    <li>
                      <a href="">لوحة التحكم  <i class="fa fa-caret-down"></i>
                      </a>
                      <ul>
                        <li></li>

                        <li>
                          <a href="/add_dress">إدارة الفساتين
                          </a>

                        </li>
                        <li>
                          <a href="">عرض التقارير <i class="fa fa-caret-down"></i></a>
                          <ul>
                            <li>
                              <a href="/report">تقارير البيع </a>
                            </li>
                          </ul>



                        </li>

                        <li>
                          <a href="/Accessory">إدارة الملحقات
                          </a>


                        </li>

                        <li>
                          <a href="/users/create">إدارة المستخدمين
                          </a>

                        </li>

                        <li>
                          <a href="/biginvoice">إدارة الفواتير
                          </a>


                        </li>

                        <li>
                          <a href="">عرض التقارير <i class="fa fa-caret-down"></i></a>
                          <ul>
                            <li>
                              <a href="/report">تقارير البيع </a>
                            </li>
                            <li>
                              <a href="/reports">تقارير الشراء</a>
                            </li>
                          </ul>



                        </li>

                        <li>
                          <a href="/roles/create">إدارة الصلاحيات
                          </a>
                        </li>

                        <li>
                          <a href="/ap">إدارة المواعيد
                          </a>
                        </li>
                        <li>
                          <a href="/show_store">إدارة المستودع
                          </a>
                        </li>
                        <li>
                          <a href="/add_Type">إدارة الأنواع
                          </a>
                        </li>


                      </ul>
                    </li>

                    <li>
                      <a href="">المتجر <i class="fa fa-caret-down"></i>
                      </a>
                      <ul>
                        <li></li>
                        <li>
                          <a href="">فساتين <i class="fa fa-caret-left"></i>
                          </a>
                          <ul>
                            @foreach($Type as $Typee)
                            @if($Typee['type_piece'] == 'clothe')
                            <li>
                              <a  href="{{ route('filter',$Typee->name)}}">{{$Typee['name']}}</a>
                            </li>
                            @endif
                            @endforeach

                          </ul>

                        </li>
                        <li>
                          <a href="">ملحقات <i class="fa fa-caret-left"></i>
                          </a>
                          <ul>

                            @foreach($Type as $Type)
                            @if($Type['type_piece'] == 'accessory')
                            <li>
                              <a  href="{{ route('filter',$Type->name)}}">{{$Type['name']}}</a>
                            </li>
                            @endif
                            @endforeach


                          </ul>
                        </li>
                      </ul>
                    </li>
                    <li>
                      <a href="">العمليات <i class="fa fa-caret-down"></i>
                      </a>
                      <ul>
                        <li>
                          <a href="/design_t">التصميم</a>
                        </li>

                        <li>
                          <a href="/maintenance">الصيانة</a>
                        </li>

                        <li>
                          <a href="/addclean">التنظيف</a>
                        </li>

                      </ul>
                    </li>
                    <li>
                      <a href="/all_customer">العملاء</a>
                    </li>

                    <li>
                      <a href="/notification">التذكيرات </a>
                    </li>

                    <li>
                      <a href="/contract">العقود</a>
                    </li>


                  </ul>
                </nav>
                @endif

              </div>
            </div>
          </div>
  <div class="wrapper style3 ">
    <div class="title ">
      <h2>
        <a href="index.html">{{$clothe->type}}</a>
      </h2>
    </div>
    <div class="container" style="margin-top: 50px;">

      <div class="row">
        <!-- start product gallery -->
        <div class="col-lg-5 col-md-6  ">
          <!--  start image product -->
          <div class="img_producto_container" data-scale="1.6">
            <img id="img-Box" class=" img_producto" src="/images/clothe/{{$clothe->piece_img}}">
          </div>
          <!--  end image product -->
          <!--  start images gallery -->

          <div class="myProducts">
            @foreach($clothe->image as $image_p)
            <img src="/images/clothe/{{$image_p->src}}" onclick="myImageFunction(this)">
            @endforeach


          </div>
          <!--  end images gallery -->
        </div>
        <!-- end product gallery -->
        <!-- start  details and calendar-->
        <div class="col-lg-7 col-md-6">
          <!-- start  details-->

          <section class="panel">
            <div class="panel-body">
              <a href="mail_compose.html" class="btn btn-compose">
                <i class="fa fa-pencil"></i> معلومات الفستان </a>
              <div class="col-md-6">
                <div class="room-box">
                  <h5>
                    الألوان المتوفرة
                  </h5>
                  <p>نهدي</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="room-box">
                  <h5>القياسات</h5>

                  <p class="box" style="text-align: center;">{{$clothe->size}}</p>


                </div>
              </div>
              <div class="col-md-6">
                <div class="room-box">
                  <h5>اسم الفستان </h5>
                  <p>{{$clothe->name}}</p>
                </div>
              </div>
              <div class="col-md-6">
                <div class="room-box">
                  <h5>اسم المصمم</h5>
                  <p>{{$clothe->designer_name}}</p>
                </div>
              </div>
            </div>
          </section>
          <!-- end  details -->

          <!-- start  calendar-->

          <div id="calendar">

            <div class="panel ">
              <div class="panel-body" style="color: black;background-color:
                  #fff;">

                <a href="mail_compose.html" class="btn btn-compose">

                  <i class="fa fa-calendar"></i> الجدولة </a>
                <br><br>
                <div id="date-popover" class="popover top">

                  <div class="arrow"></div>
                  <h3 class="popover-title"></h3>
                  <div id="date-popover-content" class="popover-content">

                  </div>
                </div>
                <div id="my-calendar"></div>
              </div>
            </div>
          </div>


          <!-- end  calendar-->
        </div>
        <!-- end details and calendar-->
      </div>
      <!-- end card dress -->
    </div>
  </div>
  <!-- js bootstrap -->
  <script src="/js/jquery.min.js"></script>
  <script src="/js/jquery-3.1.1.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <!-- js template -->
  <script src="/js/jquery.dropotron.min.js"></script>
  <script src="/js/skel.min.js"></script>
  <script src="/js/skel-layers.min.js"></script>
  <script src="/js/init.js"></script>
  <!-- js calendar -->
  <script src="/js/moment.min.js"></script>
  <script src="/package/dist/fullcalendar.js"></script>



  <!-- function image gallery -->
  <script>
    function myImageFunction(productSmallImg) {
      var productFullImg = document.getElementById("img-Box");
      productFullImg.src = productSmallImg.src;
    }
  </script>
  <!-- function image zoom -->
  <script>
    $(".img_producto_container")
      // tile mouse actions
      .on("mouseover", function() {
        $(this)
          .children(".img_producto")
          .css({
            transform: "scale(" + $(this).attr("data-scale") + ")"
          });
      })
      .on("mouseout", function() {
        $(this)
          .children(".img_producto")
          .css({
            transform: "scale(1)"
          });
      })
      .on("mousemove", function(e) {
        $(this)
          .children(".img_producto")
          .css({
            "transform-origin": ((e.pageX - $(this).offset().left) / $(this).width()) * 100 +
              "% " +
              ((e.pageY - $(this).offset().top) / $(this).height()) * 100 +
              "%"
          });
      });
  </script>


  <!-- / calendar -->

  <script type="application/javascript">
    $(document).ready(function() {

      var pro = @json($events);


      // For drawing and dividing the calendar
      $('#my-calendar').fullCalendar({
        header: {

          left: 'prev,next today',
          center: 'title',
          right: 'month'

        },
        events: pro,


      });

    });
  </script>
  <script>
    var expanded = false;

    function showCheckboxes() {
      var checkboxes = document.getElementById("checkboxes");
      if (!expanded) {
        checkboxes.style.display = "block";
        expanded = true;
      } else {
        checkboxes.style.display = "none";
        expanded = false;
      }
    }
  </script>

</body>

</html>