@extends('master') @section('contents')
<div class="wrapper style3 ">
  <div class="title ">
    <h2>
      <a href="index.html">المستودع </a>
    </h2>
  </div>
  <div class="container">
    <div class="row ">
      <div class="pull-right" style="margin-right: 180px;">
       
      </div>
    </div>

    <!-- end search -->
    <!-- start size filter -->

    <div class="row">
      @foreach($Store_Clothe as $Store_Clothe)

      <div class=" col-md-3 col-lg-3" style="float: right;">

        <div class="dress-box">
          @if($Store_Clothe->piece['type']=='clothe')
          <div class="icn-main-container">
            <img id="image" src="/images/clothe/{{$Store_Clothe->piece['piece_img']}}">
          </div>
          @else
          <div class="icn-main-container">
            <img id="image" src="/images/clothe/{{$Store_Clothe->piece['piece_img']}}">
          </div>
          @endif
          <table class="table">
            <!-- start name dress  -->
            <br>
            <span class="name">{{$Store_Clothe->piece['name']}}</span>
            <br>
            <!-- end name drees  -->
            <!-- start size -->
            @if($Store_Clothe->piece['type']=='clothe')
            <tr>
              <td style="text-align: center;">القياس </td>
              <td class="size">
                {{$Store_Clothe->piece['size']}}
              </td>
            </tr>
            @endif
            <!-- end size -->
            <!-- start selling price  -->
            <tr>
              <td style="text-align: center;">سعر البيع </td>
              <td class="price"> {{$Store_Clothe->piece['selling_price']}} </td>
            </tr>
            <!-- end selling price  -->
            <!-- start rent price -->
            <tr>
              <td style="text-align: center;">سعر الإيجار </td>
              <td class="price">{{$Store_Clothe->piece['rent_price']}}</td>

            </tr>
            <tr>
              <td style="text-align: center;">عدد القطع</td>
              <td class="price">{{$Store_Clothe->number_of_pieces}}</td>

            </tr>

            <!-- end rent price -->
          </table>
          <!-- button more info -->
          <form style="padding-top: 10px; " method="post" action="/find_store_clothe/{{$Store_Clothe->id}}" enctype="multipart/form-data">

            @csrf
            <button type="submit" class="btn btn-theme" style="margin-top: -10px;">إضافة إلى المتجر </button>
          </form>
          <!-- button more info -->
        </div>

      </div>

      @endforeach
    </div>

    <!-- end card dress -->
    <!-- start pagination -->
    <div class="pagination">
      <!--  <a href="#">&laquo;</a>
      <a href="#">1</a>
      <a href="#">2</a>
      <a href="#">3</a>
      <a href="#">4</a>
      <a href="#">5</a>
      <a href="#">&raquo;</a> -->
      <!-- end pagination-->
    </div>
  </div>
</div>
@endsection