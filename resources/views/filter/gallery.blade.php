@extends('master') @section('contents')
 <?php
   use Spatie\Permission\Models\Role;
$roles=Role::all();
  ?>
<div class="wrapper style3 ">
  <div class="title ">
    <h2>
      <a href="index.html">فساتين </a>
    </h2>
  </div>
  <div class="container">
    <div class="row  ">
      <!--start search -->
      <div class="col-lg-3 col-md-3 " style="float: right;">
        <form method="GET" action="/search_piece" enctype="multipart/form-data">
          @csrf
          <div class="input-group">
            <input type="text" class="form-control" placeholder="Search" id="txtSearch" style="border-radius: 0px 5px 5px 0px ;" name="name" />
            <div class="input-group-btn ">
              <button class="btn btn-theme" type="submit" style="border-radius: 5px 0px 0px 5px ; ">
                <span class="glyphicon glyphicon-search"></span>
              </button>
            </div>
          </div>

        </form>
      </div>
    </div>

    <!-- end search -->
    <!-- start size filter -->
    <form method="post" action="{{ route('filt') }}" enctype="multipart/form-data">
      @csrf
      <div class="row">
        <input type="hidden" name="ctype" value="{{$name}}">

        <div class="col-lg-3 col-md-3" style="float: right;">
          <div class="form-group ">
            <label for="pet-select" id="filtr">
              <h4>فلترة القياس </h4>
            </label>
            <select name="size" id="pet-select">
              <option value="non">non</option>
              @foreach($size_general as $size_general)
              <option value="{{$size_general->value}}">{{$size_general->value}}</option>
              @endforeach
            </select>
          </div>

        </div>
        <!-- end filter -->
        <!-- start  kind filter  -->
        <div class="col-lg-3 col-md-3" style="float: right;">
          <div class="form-group">
            <label for="selling_range" id="filtr">
              <h4>فلترة النوع </h4>
            </label>
            <select name="kind" id="clothes_type">
              <option value="non">non</option>
              <option value="selling_range">أسعار البيع </option>
              <option value="renting_range">أسعار الإيجار </option>
            </select>
          </div>
        </div>
        <!-- end filter -->
        <!-- start price filter -->
        <div class="col-lg-3 col-md-3" style="float: right;">
          <div class="form-group">
            <label for="renting_range" id="filtr">
              <h4>فلترة السعر </h4>
            </label>
            <select name="range" id="clothes_type">
              <option value="non">non</option>
              @foreach($range as $range)
              <option value="{{$range->id}}">{{$range->name}}</option>
              @endforeach
            </select>
          </div>
          <!-- end filter -->

        </div>
        <!-- start price filter -->
        <div class="col-lg-3 col-md-3" style="margin-top: 7px;">
          <button class="btn btn-primary" type="submit">
            فلترة
          </button>
          <!-- end filter -->

        </div>
      </div>

    </form>

    <div class="row">
      @foreach($clothe as $clothe)
      @if($clothe->statue!="مستودع" && $clothe->statue!="مباع" && $clothe->statue!="في انتظار الزبون" && $clothe->statue!="مزاد")
      <div class=" col-md-3 col-lg-3" style="float: right;">

        <div class="dress-box">

          <div class="icn-main-container">
            <img id="image" src="/images/clothe/{{$clothe->piece_img}}">
          </div>
          <table class="table">
            <!-- start name dress  -->
            <br>
            <span class="name">{{$clothe->name}}</span>
            <br>
            <!-- end name drees  -->
            <!-- start size -->
            <tr>
              <td style="text-align: center;">القياس </td>
              <td class="size">

                {{$clothe->size}}

              </td>

            </tr>
            <!-- end size -->
            <!-- start selling price  -->
            <tr>
              <td style="text-align: center;">سعر البيع </td>
              <td class="price"> {{$clothe->selling_price}} </td>
            </tr>
            <!-- end selling price  -->
            <!-- start rent price -->
            <tr>
              <td style="text-align: center;">سعر الإيجار </td>
              <td class="price">{{$clothe->rent_price}}</td>
            </tr>

            <!-- end rent price -->
          </table>
          <!-- button more info -->
           @hasanyrole($roles)
          <a class="btn btn-theme" href="/product/{{$clothe->id}}" style="margin-top: -10px;">عرض المزيد </a>
            @endhasanyrole
          <!-- button more info -->

        </div>

      </div>
      @endif
      @endforeach
    </div>

    <!-- end card dress -->
    <!-- start pagination -->
    <div class="pagination">
     
    </div>
  </div>
</div>
@endsection