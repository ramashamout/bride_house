@extends('master') @section('contents')
<div class="wrapper style3 ">
  <div class="title ">
    <h2>
      <a href="index.html">إدارة التطبيق</a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">
              <i class="fa fa-edit" style="color: black;"></i>إضافة صورة للتطبيق</a>
          </li>
          <li>
            <a data-toggle="tab" href="#edit">جميع صور الصفحة الرئيسية للتطبيق </a>
          </li>
        </ul>
      </div>
      <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information cotract -->
          <div id="overview" class="tab-pane active">
            <form style="padding-top: 10px; " method="post" action="{{ route('Banner.store') }}" enctype="multipart/form-data">

              @csrf
              <div class="row mt">
                <div class="col-md-9 col-lg-9">
                  <br>
                  <div class="form-group col-lg-6 col-md-6" style="margin-top: 50px;margin-left: 200px;">
                    <div class=" fileUpload btn btn-light">
                      <label class="upload">
                        <input name="image" type="file" class="form-control mx-sm-3">تحميل صورة </label>
                    </div>
                  </div>




                </div>

                <div class="col-md-3 col-lg-3 ">
                  <div style="margin-right: 20px;">
                    <img src="images/accessories.jpg" id="image">
                  </div>
                </div>

                <div class=" col-md-4 col-lg-4 ">
                  <button type="submit" class="btn btn-theme02 " style="margin-top: -80px;margin-right: 200px;">حفظ</button>
                </div>
              </div>


            </form>

          </div>
          <!-- end information contract -->
          <!-- start all contract -->
          <div id="edit" class="tab-pane">
            <div class="row mb">
              <div class="col-lg-12 col-md-12">
                <!-- page start-->
                <div class="content-panel">
                  <div class="adv-table">
                    <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="example">
                      <thead>
                        <tr>

                          <th class="hidden-phone">الصورة </th>

                          <th class="hidden-phone"></th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($Banner as $Banner)
                        <tr class="gradeU">

                          <td class="center hidden-phone">
                            <img src="/images/Banner/{{$Banner->image}}" id="image2">
                          </td>
                          <td class="center hidden-phone">
                            <div class="col-lg-8 col-md-8">
                              <form action="{{ route('Banner.destroy', $Banner->id)}}" method="post">
                               @csrf
                                  @method('DELETE')
                                <button class="btn btn-danger btn-xs" href="{{ route('Banner.destroy', $Banner->id)}}">
                                  <i class="fa fa-trash-o "></i>
                                </button>
                              </form>
                            </div>
                            <a href="{{ route('Banner.edit', $Banner->id)}}"  class="btn btn-primary btn-xs" data-toggle="modal" data-target="#basicModal{{$Banner->id}}">
                              <i class=" fa fa-pencil"></i>
                            </a>
                            <!-- start madal edit notes -->
                            <div class="modal fade" id="basicModal{{$Banner->id}}" tabindex="-1" role="  dialog" aria-labelledby="basicModal{{$Banner->id}}" aria-hidden="true">
                              <div class="modal-dialog  modal-lg">
                                <div class="modal-content">

                                  <div class="modal-header">
                                    <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title centered" id="myModalLabel">تعديل النوع:</h4>
                                  </div>
                                  <form method="post" action="{{ route('Banner.update', $Banner->id)}}" enctype="multipart/form-data">

                                    <div class="modal-body">
                                      <div class="container-fluid">
                                        <div class="row">
                                          <div class="form-group col-md-2 col-lg-2 " style="margin-left: 400px;">
                                            <img src="/images/Banner/{{$Banner->image}}" id="image">

                                          </div>
                                        </div>
                                        <div class="form-group col-lg-5 col-md-5" style="float: right;margin-right: 270px;margin-top: 10px;">
                                          <div class=" fileUpload btn btn-light">
                                            <label class="upload" >
                                              <input name="piece_img" type="file" class="form-control mx-sm-3" style="display: none;">تحميل صورة </label>
                                          </div>
                                        </div>

                                      </div>

                                    </div>
                                         <div class="modal-footer ">
                                <button type="submit" class="btn btn-theme02 pull-left" data-dismiss="modal">تعديل </button>
                              </div>
                                </div>

                              </div>
                           
                              </form>


                            </div>
                  </div>
                </div>

                </td>
                </tr>
                @endforeach
                </tbody>
                </table>
              </div>
            </div>
            <!-- page end-->
          </div>
        </div>
        <!-- /row -->
      </div>
      <!-- end all contract -->
    </div>
  </div>
  <!-- end panel-body -->
</div>
</div>
</div> @endsection