@extends('master') @section('contents') 

<div class="wrapper style3">
 <div class="title ">
    <h2>
    <a href="index.html">تعديل الصلاحيات </a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
 <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          
          <li>
            <a data-toggle="tab" href="#edit">تعديل الصلاحيات</a>
          </li>
        </ul>
      </div>
 <div class="panel-body">
        <div class="tab-content">
        <!-- start all contract -->
          <div id="edit" class="tab-pane  active">
            <div class="row mt">
              <div class="col-md-12">
                <section class="task-panel tasks-widget">
                  <div class="panel-body">
                    <div class="task-content">
                      <ul class="task-list">
                      
                    
                          
                       {!! Form::model($role, ['method' => 'PATCH','route' => ['roles.update', $role->id]]) !!}
                       <div class="col-md-9 col-lg-9">
                           <br>
                 
                              <div class="form-group col-lg-12 col-md-12" style="float: right;">
                                  <div >
                                                  <label for="inputPassword6">تعديل الصلاحية :</label>
                                                  <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"  name="name" value="{{$role->name}}" />
                                                </div>
                              </div>
                                <!--start for -->
                                @foreach($permission as $value)
                                <div class="form-group col-lg-4 col-md-4" style="float: right;">
                                 
                               {{ Form::checkbox('permission[]', $value->id, in_array($value->id, $rolePermissions) ? true : false, array('class' => 'name')) }}
                                                {{ $value->name }}
                                </div>
                                  @endforeach

                              <!--end for -->
                             
                            </div>
                               <div class="col-md-3 col-lg-3 ">
                              <div style="margin-right: 20px;">
                                <img src="/images/265667.png" id="image">
                              </div>
                            </div>
                                <div class=" col-md-4 col-lg-4 ">
                          <button type="submit" class="btn btn-theme04 pull-left" style="margin-left: 100px; margin-top: 0px;">تعديل</button>
                        </div>
                            {!! Form::close() !!}
                           
                         
                       
                   
                      </ul>
                    </div>
                  </div>
                </section>
              </div>
           
          <!-- end all contract -->
           </div>
          </div>
     <!-- end panel-body -->
    </div>
  </div>
</div>
@endsection