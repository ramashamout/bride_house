@extends('master') @section('contents') 

<div class="wrapper style3">
 <div class="title ">
    <h2>
    <a href="index.html">عرض الصلاحيات </a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
 <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          
          <li>
            <a data-toggle="tab" href="#edit">جميع الصلاحيات </a>
          </li>
        </ul>
      </div>
 <div class="panel-body">
        <div class="tab-content">
        <!-- start all contract -->
          <div id="edit" class="tab-pane  active">
            <div class="row mt">
              <div class="col-md-12">
                <section class="task-panel tasks-widget">
                  <div class="panel-body">
                    <div class="task-content">
                      <ul class="task-list">
                      
                        <li>
                          
                          <div class="task-title">
                      
                                
                            <div class="pull-right hidden-phone">
                                @foreach($rolePermissions as $v)
                           <div class="form-group col-lg-4 col-md-4" style="float: right;">
                        
                           <p>{{ $v->name }}</p>
                           
                          </div>
                           @endforeach
                     
                          
                              
                           
                          
                            </div>
                          </div>
                        </li>
                   
                      </ul>
                    </div>
                  </div>
                </section>
              </div>
           
          <!-- end all contract -->
           </div>
          </div>
     <!-- end panel-body -->
    </div>
  </div>
</div>
@endsection