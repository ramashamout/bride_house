@extends('master') @section('contents') <div class="wrapper style3 ">
  <div class="title ">
    <h2>
    <a href="index.html">إدارة الصلاحيات </a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">
            <i class="fa fa-user" style="color: black;"></i>إضافة صلاحية </a>
          </li>
          <li>
            <a data-toggle="tab" href="#edit">جميع الصلاحيات </a>
          </li>
        </ul>
      </div>
      <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information cotract -->
          <div id="overview" class="tab-pane active">
              {!! Form::open(array('route' => 'roles.store','method'=>'POST')) !!}
            <div class="row mt">
              <div class="col-md-9 col-lg-9">
                  <br>
                 
                  <div class="form-group col-lg-12 col-md-12" style="float: right;">
                     {!! Form::text('name', null, array('placeholder' => 'الاسم ','class' => 'form-control')) !!}
                  </div>
                  <!--start for -->
                   @foreach($permission as $value)
                  <div class="form-group col-lg-4 col-md-4" style="float: right;">
                   
                    {{ Form::checkbox('permission[]', $value->id, false, array('class' => 'name')) }}
                      {{ $value->name }}
                  </div>
                    @endforeach

                <!--end for -->
               
              </div>

              <div class="col-md-3 col-lg-3 ">
                <div style="margin-right: 20px;">
                  <img src="/images/265667.png" id="image">
                </div>
              </div>
                <div class=" col-md-4 col-lg-4 ">
                  <button type="submit" class="btn btn-theme04 pull-left" style="margin-left: 50px; margin-top: -80px;">حفظ</button>
                </div>

            </div>
             {!! Form::close() !!}
            
          </div>
          <!-- end information contract -->
          <!-- start all contract -->
          <div id="edit" class="tab-pane">
            <div class="row mb">
              <div class="col-lg-12 col-md-12">
                <!-- page start-->
                <div class="content-panel">
                  <div class="adv-table">
                    <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="example">
                      <thead>
                        
                        <tr>
                          <th>الاسم </th>
                          <th>الصلاحية </th>
                          
                          
                          
                          <th class="hidden-phone">العمليات </th>
                        </tr>
                      </thead>
                      <tbody>
                        @foreach ($roles as $key => $role)
                        <tr class="gradeU">
                          <td>{{ $role->name }}</td>
                       
                          <td>  <a href="{{ route('roles.show',$role->id) }}" class="btn btn-primary btn-xs">
                                <i class=" fa fa-eye"></i>
                              </a></td>
                        
                          
                          
                          
                          
                          <td >
                          
                         <!--   <a  class="btn btn-primary btn-xs" href="{{ route('roles.edit',$role->id) }}">
                                <i class=" fa fa-pencil"></i>
                              </a> -->
                              <!-- start madal edit notes -->
<div class="col-md-8 col-lg-8">
                              
                                      <form action="{{ route('roles.destroy',$role->id) }}" method="post">
                                        @csrf
                                        @method('DELETE')
                                        <button class="btn btn-danger btn-xs " class="button"
                                        method="post" href="{{ route('roles.destroy',$role->id) }}">
                                        <i class="fa fa-trash-o "></i>
                                        </button>
                                      </form>
                                
                              </div>
                                       <a  class="btn btn-primary btn-xs" href="{{ route('roles.edit',$role->id) }}">
                                <i class=" fa fa-pencil"></i>
                              </a>
                               
                          </td>
                        </tr>
                        @endforeach
                        
                      </tbody>
                    </table>
                  </div>
                </div>
                <!-- page end-->
              </div>
            </div>
            <!-- /row -->
          </div>
          <!-- end all contract -->
        </div>
      </div>
      <!-- end panel-body -->
    </div>
  </div>
</div> @endsection