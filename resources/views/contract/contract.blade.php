@extends('master') @section('contents')
<div class="wrapper style3">
  <div class="title">
    <h2>
    <a href="index.html">العقود</a>
    </h2>
  </div>
  <!-- Content -->
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">
            <i class="fa fa-edit" style="color: black;"></i> معلومات العقد </a>
          </li>
          <li>
            <a data-toggle="tab" href="#edit">جميع العقود </a>
          </li>
        </ul>
      </div>
      <?php
      use Carbon\Carbon;
      ?>
      <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information cotract -->
          <div id="overview" class="tab-pane active">
            <form style="padding-top: 10px; " method="POST" action="{{ route('contract.store') }}" enctype="multipart/form-data">
              @csrf
              <div class="row mt">
                <div class="col-md-9 col-lg-9">
                  <br>
                  <div class="form-group col-lg-4 col-md-4">
                    <label for="inputPassword6">رقم الهاتف</label>
                    <input type="number" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" name="mobile_number" />
                  </div>
                  
                  <div class="form-group col-lg-4 col-md-4"style="float: right;">
                    <label for="inputPassword6">نوع العقد</label>
                    <select id="mariatlStatus" onchange="changeStatus()" name="contract_type" style="position: relative;width:200px;">
                      <option value="آجار ">آجار </option>
                      <option value="بيع ">بيع </option>
                      
                    </select>
                  </div>
                  <script>
                  function changeStatus() {
                  var statue = document.getElementById("mariatlStatus")
                  if (statue.value == "بيع ") {
                  document.getElementById("EN").style.visibility = "hidden";
                  } else {
                  document.getElementById("EN").style.visibility = "visible";
                  }
                  }
                  </script>
                  
                  <div class="form-group col-lg-4 col-md-4" >
                    <label for="inputPassword6">الإسم الثلاثي</label>
                    <input type="text" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" name="full_name" />
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <label for="inputPassword6">العنوان</label>
                    <input type="text" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" name="address" />
                  </div>
                  
                  
                  <div class="form-group col-lg-4 col-md-4">
                    <label for="inputPassword6">اسم الفستان :</label>
                    <input type="text" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" list="clothe" name="clothe_name" />
                  </div>
                  <datalist id="clothe">
                  @foreach($clothe as $clothe)
                  @if($clothe->statue!="مباع " && $clothe->statue!="مستودع")
                  <option value="{{$clothe->name}}">{{$clothe->name}}</option>
                  <input type="hidden" name="piece_id" value="{{$clothe->id}}" >
                  @endif
                  @endforeach
                  </datalist>
                  
                  <div class="form-group col-lg-4 col-md-4">
                    <label for="inputPassword6">الملحقات :</label>
                    <div class="multi_select_box">
                      <select class="multi_select" class="form-control mx-sm-3" name="accessory_name[]" multiple data-selected-text-format="count >3">
                        @foreach($accessory as $accessory)
                        @if($accessory->statue_acc =="متوفر ")
                        <option value="{{$accessory->name}}">{{$accessory->name}}</option>
                        {{-- <input type="hidden" name="acc_id" value="{{$accessory->id}}" >--}}
                        @endif
                        @endforeach
                      </select>
                    </div>
                  </div>
                  <div class="form-group col-lg-4 col-md-4" style="float: right;">
                    <label for="inputPassword6">مبلغ التأمين</label>
                    <input type="number" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" name="insurance_price" />
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <label for="inputPassword6">الرعبون</label>
                    <input type="number" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" name="deposit" />
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <label for="inputPassword6">تسليم كامل المبلغ</label>
                    <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="event_date" />
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <label for="inputPassword6">دفعة أولى</label>
                    <input type="number" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" name="first_batch" />
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <label for="inputPassword6">دفعة ثانية</label>
                    <input type="number" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" name="second_batch" />
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <label for="inputPassword6">الصالة</label>
                    <input type="text" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" ى name="hall" />
                  </div>
                  <div class="form-group col-lg-4 col-md-4" id="EN">
                    <label for="inputPassword6">تاريخ الاستلام</label>
                    <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="end" />
                  </div>
                  
                  <div class="form-group col-lg-4 col-md-4">
                    <label>تاريخ العقد</label>
                    <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="contract_date" />
                  </div>
                  <div class="form-group col-lg-4 col-md-4">
                    <label>تاريخ التسليم</label>
                    <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="start" />
                  </div>
                  <div class="form-group">
                    <input type="hidden" name="title" value="آجار">
                  </div>
                  <div class="form-group">
                    <input type="hidden" name="color" value="green">
                  </div>
                  <div class="form-group">
                    <input type="hidden" name="is_check" value=0>
                  </div>
                  <div class="form-group">
                    <input type="hidden" name="is_check1" value=1>
                  </div>
                </div>
                <div class="col-md-3 col-lg-3  ">
                  <div style="margin-right: 20px;">
                    <img src="/images/31.png" id="image">
                  </div>
                </div>
                <div class=" col-lg-4 col-md-4">
                  <button type="submit" class="btn btn-theme04 pull-left" style="margin-left: 50px;margin-top: -50px;">حفظ</button>
                </div>
              </div>
              <div>
                @if($errors->any())
                <div class="alert alert-danger">
                  <ul>
                    <li>
                      {{$errors->first()}}
                    </li>
                  </ul>
                </div><br />
                @endif
              </div>
            </form>
          </div>
          <!-- end information contract -->
          <!-- start all contract -->
          <div id="edit" class="tab-pane">
            <div class="row mt">
              <div class="col-md-12">
                <section class="task-panel tasks-widget">
                  <div class="panel-body">
                    <div class="task-content">
                      <ul class="task-list">
                        @foreach($contract as $contract)
                        <li>
                          <div class="task-title">
                            <span class="task-title-sp">
                              <i class="fa fa-circle" style="font-size:10px"></i>&nbsp;{{$contract->customer->full_name}}&nbsp; <a href="#" id="mytooltip" data-toggle="tooltip" title="{{$contract->contract_date}}" data-placement="left">
                                <i class="fa fa-calendar " style="font-size:15px ; color:black; "></i>
                              </a>
                            </span>
                            <div class="pull-left hidden-phone">
                              
                              <a href="/cont/{{$contract->id}}" class="btn btn-primary btn-xs" id="mytooltip" data-toggle="tooltip" title="تعديل العقد" data-placement="left"style="margin-left: 5px;">
                                <i class=" fa fa-pencil"></i>
                              </a>
                              
                              <div class="pull-left hidden-phone">
                                @if ($contract->start>Carbon::now()->toDateString() || $contract->end>Carbon::now()->toDateString())
                                <form action="{{ route('contract.destroy', $contract->id)}}" method="post">
                                  @csrf
                                  @method('DELETE')
                                  <button class="btn btn-danger btn-xs " class="button" method="post" href="{{ route('contract.destroy', $contract->id)}}">
                                  <i class="fa fa-trash-o " ></i>
                                  </button>
                                </form>
                                @endif
                              </div>
                              <!--  <a href="/create_itemsDeliverd_with_id/{{$contract->id}}" class="btn btn-warning btn-xs" id="mytooltip" data-toggle="tooltip" title="الأغراض المستلمة" data-placement="left"style="margin-left: 3px;">
                                <i class=" fa fa-check"></i>
                              </a>
                              <a href="/create_receipt_with_id/{{$contract->id}}" class="btn btn-success btn-xs" style="margin-left: 3px;" id="mytooltip" data-toggle="tooltip" title="إيصالات القبض" data-placement="left">
                                <i class=" fa fa-plus"></i>
                              </a>
                              -->
                            </div>
                          </div>
                        </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
          <!-- end all contract -->
        </div>
      </div>
      <!-- end panel-body -->
    </div>
  </div>
</div>
@endsection
@section('javascript')
<script type="text/javascript">
$(#clothe_name).select2({
placeholder: "select clothe",
allowClear: true
});
$(#accessory_name).select2({
placeholder: "select accessory",
allowClear: true
});
</script>
@endsection