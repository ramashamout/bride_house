@extends('master')
@section('contents')
<div class="wrapper style3">
  <div class="title ">
    <h2>
      <a href="index.html">العقد رقم {{$contract->id}}</a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">

          <li>
            <a data-toggle="tab" href="#edit">عقد الزبونة {{$contract->customer->full_name}}
            </a>
          </li>
        </ul>
      </div>
      <div class="panel-body">
        <div class="tab-content">
          <!-- start all contract -->
          <div id="edit" class="tab-pane  active">
            <div id="edit" class="tab-pane">
              <div class="row mb">
                <div class="col-lg-12 col-md-12">
                  <!-- page start-->
                  <div class="content-panel">
                    <div class="adv-table">
                      <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered">
                        <thead>
                          <tr>
                            <th>الاسم الثلاثي </th>
                            <th>العنوان </th>
                            <th class="hidden-phone">رقم الهاتف </th>
                            <th class="hidden-phone">اسم الفستان </th>
                            <th class="hidden-phone">الملحقات </th>
                            <th class="hidden-phone">الصالة </th>
                            <th class="hidden-phone">مبلغ التأمين </th>
                            <th class="hidden-phone">الرعبون </th>
                            <th class="hidden-phone">الدفعة الأولى </th>
                            <th class="hidden-phone">الدفعة الثانية </th>
                            <th class="hidden-phone">المبلغ الكلي </th>
                            <th class="hidden-phone">تاريخ العقد </th>
                            <th class="hidden-phone">تاريخ الاستلام </th>
                            <th class="hidden-phone">تاريخ التسليم </th>
                          </tr>
                        </thead>
                        <tbody>

                          <tr class="gradeU">
                            <td>{{$contract->customer->full_name}}</td>
                            <td>{{$contract->customer->address}}</td>
                            <td class="hidden-phone">{{$contract->customer->mobile_number}}</td>
                            @foreach($contract->piece_contracts as $piece)
                            <td>{{$piece->piece->name}} </td>
                            @endforeach
                            <td class="hidden-phone">{{$contract->hall}}</td>
                            <td class="hidden-phone">{{$contract->insurance_price}}</td>
                            <td class="hidden-phone">{{$contract->deposit}}</td>
                            <td class="hidden-phone">{{$contract->first_batch}}</td>
                            <td class="hidden-phone">{{$contract->second_batch}}</td>
                            <td class="hidden-phone">{{$contract->total_price}}</td>
                            <td class="hidden-phone">{{$contract->contract_date}}</td>
                            <td class="hidden-phone">{{$contract->end}}</td>
                            <td class="hidden-phone">{{$contract->start}}</td>


                          </tr>

                        </tbody>
                      </table>
                      <a href="/contract" class="btn btn-success btn-xs" style="float: left;">العودة إلى صفحة العقود</a>
                    </div>
                  </div>
                  <!-- page end-->
                </div>
              </div>
              <!-- /row -->
            </div>
          </div>
          <!-- end panel-body -->
        </div>
      </div>
    </div>
  </div>
</div>
@endsection