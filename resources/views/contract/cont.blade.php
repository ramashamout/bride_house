 @extends('master') @section('contents')
 <div class="wrapper style3">
   <div class="title">
     <h2>
       <a href="index.html">العقود</a>
     </h2>
   </div>
   <!-- Content -->
   <div class="container">
     <div class="panel panel-default">
       <!-- start panel heading -->
       <div class="panel-heading ">
         <ul class="nav nav-tabs nav-justified">
           <li class="active">
             <a data-toggle="tab" href="#overview">
               <i class="fa fa-edit" style="color: black;"></i> معلومات العقد </a>
           </li>

         </ul>
       </div>

       <div class="panel-body">
         <div class="tab-content">
           <!-- start information cotract -->
           <div id="overview" class="tab-pane active">

             <form method="post" action="{{ route('contract.update', $contract->id) }}" enctype="multipart/form-data">
               @method('PATCH')
               @csrf
               <div class="row mt">
                 <div class="col-md-9 col-lg-9">
                   <br>

                   <div class="form-group col-lg-4 col-md-4" style="float: right;">
                     <label for="inputPassword6">الإسم الثلاثي</label>
                     <input type="text" id="inputPassword6" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" placeholder="الاسم الثلاثي" name="full_name" value="{{$contract->customer->full_name}}" />
                   </div>
                   <div class="form-group col-lg-4 col-md-4">
                     <label for="inputPassword6">رقم الهاتف</label>
                     <input type="number" id="inputPassword6" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" placeholder="رقم الهاتف" name="mobile_number" value="{{$contract->customer->mobile_number}}" />
                   </div>
                   <div class="form-group col-lg-4 col-md-4">
                     <label for="inputPassword6">العنوان</label>
                     <input type="text" id="inputPassword6" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" placeholder="العنوان" name="address" value="{{$contract->customer->address}}" />


                   </div>
                   <div class="form-group col-lg-4 col-md-4">
                     <label for="inputPassword6">الصالة</label>
                     <input type="text" id="inputPassword6" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" placeholder="الصالة" name="hall" value="{{$contract->hall}}" />
                   </div>
                   <div>

                     <div class="form-group col-lg-4 col-md-4">
                       <label for="inputPassword6">الفستان</label>
                       <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" placeholder="الفستان" list="clothe" name="clothe_name" value="{{$current_c->name}}" />
                     </div>
                     <datalist id="clothe">
                       @foreach($clothe as $clothe)
                       <option value="{{$clothe->name}}">{{$clothe->name}}</option>
                       <input type="hidden" name="piece_id" value="{{$clothe->id}}">
                       @endforeach
                     </datalist>


                   </div>
                   <div class="form-group col-lg-4 col-md-4">
                     <label for="inputPassword6">الملحقات</label>
                     {{-- <input  type="text"  id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" placeholder="الملحقات"  list="accessory" name="accessory_name"  value="{{$current_a->name}}" />--}}
                     <select class="multi_select" class="form-control mx-sm-3" name="accessory_name[]" multiple data-selected-text-format="count >3">

                       @foreach($accessory as $accessory)
                       @if($accessory->statue_acc =="متوفر ")
                       <option value="{{$accessory->name}}">{{$accessory->name}}</option>
                       {{-- <input type="hidden" name="acc_id" value="{{$accessory->id}}" >--}}
                       @endif
                       @endforeach

                     </select>

                   </div>

                   <div class="form-group col-lg-4 col-md-4" style="float: right;">
                     <label for="inputPassword6">مبلغ التأمين</label>
                     <input type="number" id="inputPassword6" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" placeholder="مبلغ التأمين" name="insurance_price" value="{{$contract->insurance_price}}" />
                   </div>

                   <div class="form-group col-lg-4 col-md-4">
                     <label for="inputPassword6">الرعبون</label>
                     <input type="number" id="inputPassword6" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" placeholder="الرعبون" name="deposit" value="{{$contract->deposit}}" />
                   </div>
                   <div class="form-group col-lg-4 col-md-4">
                     <label for="inputPassword6">دفعة أولى</label>
                     <input type="number" id="inputPassword6" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" placeholder="الدفعة الأولى" name="first_batch" value="{{$contract->first_batch}}" />
                   </div>
                   <div class="form-group col-lg-4 col-md-4">
                     <label for="inputPassword6">تسليم كامل المبلغ</label>
                     <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" class="form-control mx-sm-3" id="inputPassword6" aria-describedby="passwordHelpInline" name="event_date" placeholder="تاريخ تسليم كامل المبلغ" value="{{$contract->event_date}}" />
                   </div>
                   <div class="form-group col-lg-4 col-md-4">
                     <label for="inputPassword6">دفعة ثانية</label>
                     <input type="number" id="inputPassword6" aria-describedby="passwordHelpInline" class="form-control mx-sm-3" placeholder="الدفعة الثانية" name="second_batch" value="{{$contract->second_batch}}" />
                   </div>
                   <div class="form-group col-lg-4 col-md-4">
                     <label for="inputPassword6">تاريخ الاستلام</label>
                     <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" class="form-control mx-sm-3" id="inputPassword6" aria-describedby="passwordHelpInline" placeholder="تاريخ الاستلام " name="end" value="{{$contract->end}}" />
                   </div>
                   <div class="form-group col-lg-4 col-md-4">
                     <label for="inputPassword6">نوع العقد</label>
                     <select name="contract_type" style="position: relative;width:200px;" value="{{$contract->contract_type}}">
                     @if($contract->contract_type =="بيع")
                                                <option value="بيع">بيع </option>
                                                <option value="آجار">آجار</option>
                                                    @else
                                                    <option value="آجار">آجار</option>
                                                    <option value="بيع">بيع </option>
                                                @endif
                     </select>
                   </div>

                   <div class="form-group col-lg-4 col-md-4">
                     <label for="inputPassword6">تاريخ العقد</label>
                     <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" class="form-control mx-sm-3" id="inputPassword6" aria-describedby="passwordHelpInline" placeholder="تاريخ العقد" name="contract_date" value="{{$contract->contract_date}}" />
                   </div>
                   <div class="form-group col-lg-4 col-md-4">
                     <label for="inputPassword6">تاريخ التسليم</label>
                     <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" class="form-control mx-sm-3" id="inputPassword6" aria-describedby="passwordHelpInline" placeholder="تاريخ الاستلام " name="start" value="{{$contract->start}}" />
                   </div>
                   <div class="form-group">

                     <input type="hidden" name="title" value="آجار">

                   </div>
                   <div class="form-group">

                     <input type="hidden" name="color" value="green">

                   </div>
                   <div class="form-group">

                     <input type="hidden" name="is_check" value=0>

                   </div>
                   <div class="form-group">

                     <input type="hidden" name="is_check1" value=1>

                   </div>

                 </div>
                 <div class="col-md-3 col-lg-3  ">
                   <div style="margin-right: 20px;">
                     <img src="/images/31.png" id="image">
                   </div>
                 </div>
                 <div class=" col-lg-4 col-md-4">
@if ($contract->start>Carbon::now()->toDateString() || $contract->end>Carbon::now()->toDateString())
                   <button type="submit" class="btn btn-theme04 pull-left" style="margin-left: 50px;margin-top: -50px;">تعديل </button>
                   @endif
                 </div>
               </div>
               <div>
                @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    <li>
                                        {{$errors->first()}}
                                    </li>
                                </ul>
                            </div><br />
                        @endif
               </div>
             </form>
           </div>
         </div>
       </div>
     </div>
   </div>
 </div>
 @endsection
 @section('javascript')
 <script type="text/javascript">
   $(#clothe_name).select2({
     placeholder: "select clothe",
     allowClear: true
   });
   $(#accessory_name).select2({
     placeholder: "select accessory",
     allowClear: true
   });
 </script>
 @endsection