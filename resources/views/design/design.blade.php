@extends('master') @section('contents') <div class="wrapper style3 ">
  <div class="title ">
    <h2>
      <a href="index.html">عملية التصميم</a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">
              <i class="fa fa-edit" style="color: black;"></i> معلومات التصميم </a>
          </li>
          <li>
            <a data-toggle="tab" href="#edit">جميع عمليات التصميم</a>
          </li>
        </ul>
      </div>
         <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information cotract -->
          <div id="overview" class="tab-pane active">
                <form method="post" action="{{ route('design.store') }}" enctype="multipart/form-data">
                    @csrf
                      <div class="row mt">
                         <div class="col-md-9 col-lg-9">

                                 <br>
                   <div class="form-group col-lg-4 col-md-4" style="float: right;">
                       <label for="inputPassword6">اسم الزبون :</label>
                       <input type="text" class="form-control" name="full_name"  placeholder="اسم الزبون"/>

                    </div>
                  <div class="form-group col-lg-4 col-md-4">
                      <label for="inputPassword6">العنوان :</label>
                          <input type="text" class="form-control" name="address" placeholder="العنوان"/>

                    </div>
                     <div class="form-group col-lg-4 col-md-4">
                         <label for="inputPassword6">رقم الهاتف :</label>
                         <input type="number" class="form-control" name="mobile_number" placeholder="رقم الهاتف" />


                    </div>
                    <div class="form-group col-lg-4 col-md-4">

                        <label for="inputPassword6">القياس العام  :</label>
                        <select  class="form-control" name="general_size" placeholder="القياس العام " >
                        
                            @foreach(\App\Models\size_general::all() as $size_general)
                                <option value="{{$size_general->value}}">{{$size_general->value}}</option>

                            @endforeach

                        </select>
                    </div>
                    <div class="form-group col-lg-4 col-md-4">
                        <label for="inputPassword6">ملاحظة التصميم :</label>
                        <input type="text" class="form-control" name="design_notes" placeholder="ملاحظة التصميم"  />
                    </div>
                     <div class="form-group col-lg-4 col-md-4">
                         <label for="inputPassword6">تاريخ الاستلام من الورشة :</label>
                        <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'"class="form-control" name="received_date" placeholder="تاريخ الاستلام من الورشة"/>
                    </div>
                     <div class="form-group col-lg-4 col-md-4">
                         <label for="inputPassword6">تاريخ التسليم للورشة :</label>
                        <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" class="form-control" name="delivery_date" placeholder="تاريخ التسليم للورشة" />
                    </div>
                    <div class="form-group col-lg-4 col-md-4">
                        <label for="inputPassword6">سعر التصميم :</label>
                        <input type="number" class="form-control" name="designing_price"  placeholder="سعر التصميم" />
                    </div>
                     <div class="form-group col-lg-4 col-md-4">


                        <div>
                            <label for="inputPassword6">النوع :</label>
                            <select  class="form-control" name="type" placeholder="النوع " >

                                @foreach($type as $type)
                                    @if($type->type_piece=='clothe')
                                        <option value="{{$type->name}}">{{$type->name}} </option>
                                    @endif
                                @endforeach

                            </select>

                    </div>
                  </div>
                  <script type="text/javascript">
                   $(#type).select2({
                   placeholder:"اسم الفستان ",
                   allowClear:true
                    });

                   </script>

                    <div class="form-group col-lg-4 col-md-4">
                        <label for="inputPassword6">لون القماش :</label>
                       <input type="text" class="form-control" name="color" placeholder="لون القماش"  />

                    </div>
                     <div class="form-group col-lg-4 col-md-4">
                         <label for="inputPassword6">نوع القماش :</label>
                      <input type="text" class="form-control" name="fabric_type" placeholder="نوع القماش" />

                    </div>
                     <div class="form-group col-lg-4 col-md-4">
                         <label for="inputPassword6">الموديل :</label>
                     <input type="text" class="form-control" name="model" placeholder="الموديل" />
                    </div>
                       <div class="form-group col-lg-4 col-md-4" style="float: right;">
                    <div class=" fileUpload btn btn-light">
                      <label class="upload">

                        <input name='design_img' type="file" class="form-control mx-sm-3">تحميل صورة </label>
                    </div>
                  </div>


                      </div>
                          <div class="col-md-3 col-lg-3 ">
                            <div style="margin-right: 20px;">
                              <img src="/images/19.jpg" id="image">
                            </div>
                          </div>
                           <div class=" col-lg-4 col-md-4">
                            <button type="submit" class="btn btn-theme02 pull-left" style="margin-left: 50px;margin-top: -50px;">حفظ</button>
                          </div>

                        </div>
                    <div>
                        @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    <li>
                                        {{$errors->first()}}
                                    </li>
                                </ul>
                            </div><br />
                        @endif
                    </div>
                </form>
               </div>

                   <!-- start all contract -->
          <div id="edit" class="tab-pane">
            <div class="row mt">
              <div class="col-md-12">
                <section class="task-panel tasks-widget">
                  <div class="panel-body">
                    <div class="task-content">
                      <ul class="task-list">
                        @foreach($design as $design)
                        <li>
                          <div class="task-title">
                            <span class="task-title-sp">
                              <i class="fa fa-circle" style="font-size:10px"></i>&nbsp;{{$design->customer->full_name}}&nbsp; <a href="#" id="mytooltip" data-toggle="tooltip" title="{{$design->delivery_date}}" data-placement="left">
                                <i class="fa fa-calendar " style="font-size:15px ; color:black; "></i>
                              </a>
                            </span>

                            <div class="pull-left hidden-phone">

                                <a href="{{ route('design.edit',$design->id)}}" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#basicModal{{$design->id}}">
                                <i class=" fa fa-pencil"></i>
                              </a>
                              <!-- start madal edit notes -->
                              <div class="modal fade" id="basicModal{{$design->id}}" tabindex="-1" role="  dialog" aria-labelledby="basicModal{{$design->id}}" aria-hidden="true">
                                <div class="modal-dialog  modal-lg">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title centered" id="myModalLabel">تعديل عملية التصميم</h4>
                                    </div>
                                     <form method="post" action="{{ route('design.update', $design->id) }}" enctype="multipart/form-data"  >
                                       @method('PATCH')
                                      @csrf
                                    <div class="modal-body">
                                      <div class="container-fluid">

                                          <div class="row">
                                            <div class="col-md-9 col-lg-9">
                                              <div class="form-group col-lg-4 col-md-4" style="float: right;">
                                                <label for="inputPassword6">اسم الزبون</label>
                                                <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="full_name"
                                                value="{{$design->customer->full_name}}">
                                              </div>
                                              <div class="form-group col-lg-4 col-md-4">
                                                <label for="inputPassword6">العنوان</label>
                                                <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="address"
                                                value="{{$design->customer->address}}">
                                              </div>
                                              <div class="form-group col-lg-4 col-md-4">
                                                <label for="inputPassword6">رقم الهاتف</label>
                                                <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"
                                                name="mobile_number" value="{{$design->customer->mobile_number}}">
                                              </div>
                                              <div class="form-group col-lg-4 col-md-4">
                                                <label for="inputPassword6">الموديل</label>
                                                <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="model"
                                                value="{{$design->model}}"
                                                >
                                              </div>
                                              <div class="form-group col-lg-4 col-md-4">
                                                <label for="inputPassword6">نوع القماش</label>
                                                <input type="text" id="inputEmail4" class="form-control mx-sm-3"
                                                name="fabric_type" value="{{$design->fabric_type}}">
                                              </div>
                                              <div class="form-group col-lg-4 col-md-4">
                                                <label for="inputPassword6">لون القماش</label>
                                                <input type="text" id="inputEmail4" class="form-control mx-sm-3"
                                                name="color" value="{{$design->color}}">
                                              </div>
                                              <div class="form-group col-lg-4 col-md-4">
                                                <label for="inputPassword6">سعر الصميم</label>
                                                <input type="text" id="inputEmail4" class="form-control mx-sm-3"
                                                name="designing_price" value="{{$design->designing_price}}">
                                              </div>
                                              <div class="form-group col-lg-4 col-md-4">
                                                <label for="inputPassword6">القياس العام </label>

                                                  <select  class="form-control" name="general_size">
                                                      <option value="{{$design->general_size}}">{{$design->general_size}} </option>
                                                      @foreach(\App\Models\size_general::all() as $size)
                                                          @if($size->value !=$design->general_size)
                                                              <option value="{{$size->value}}">{{$size->value}} </option>
                                                          @endif
                                                      @endforeach

                                                  </select>

                                              </div>

                                                <div class="form-group col-lg-4 col-md-4">
                                                <label for="inputPassword6">النوع </label>
                                                   <div>


                                                       <select  class="form-control" name="type" placeholder="النوع " value="{{$design->type}}">
                                                           <option value="{{$design->type}}">{{$design->type}} </option>
                                                           @foreach(\App\Models\Type::all() as $type)
                                                               @if($type->type_piece=='clothe'&& $type->name!=$design->type)
                                                                   <option value="{{$type->name}}">{{$type->name}} </option>
                                                               @endif
                                                           @endforeach

                                                       </select>
                                                  </div>
                                              </div>


                                              <div class="form-group col-lg-4 col-md-4">
                                                <label for="inputPassword6">تاريخ الاستلام من الورشة</label>
                                                <input type="date" class="form-control mx-sm-3" name="received_date"
                                                value="{{$design->received_date}}">
                                              </div>
                                              <div class="form-group col-lg-4 col-md-4">
                                                <label for="inputPassword6">تاريخ التسليم للورشة</label>
                                                <input type="date" class="form-control mx-sm-3" name="delivery_date"
                                                value="{{$design->delivery_date}}">
                                              </div>

                                               <div class="form-group col-lg-4 col-md-4">
                                                 <div class=" fileUpload btn btn-light" style="margin:20px;">
                                                  <label class="upload">
                                                    <input name="design_img" value="{{$design->design_img}}" type="file" class="form-control mx-sm-3">تحميل صورة </label>
                                                </div>
                                              </div>



                                              <div class="form-group col-lg-12 col-md-12">
                                                <label for="inputPassword6">ملاحظة التصميم</label>
                                                <input type="text" id="inputEmail4" class="form-control mx-sm-3"
                                                name="design_notes" value="{{$design->design_notes}}">
                                              </div>
                                            </div>
                                            <div class="col-md-3 col-lg-3 ">
                                              <img src="/images/design/{{$design->design_img}}" id="image">
                                              <div class="form-group pull-left ">

                                              </div>
                                            </div>

                                          </div>

                                      </div>

                                    </div>

                                     <div class="modal-footer ">
                                      @if ($design->delivery_date>Carbon::now()->toDateString() || $design->received_date>Carbon::now()->toDateString())
                                            <button type="submit" class="btn btn-theme02 pull-left" data-dismiss="modal{{$design->id}}">تعديل </button>
                                          </div>
                                          @endif
                                         <div>
                                             @if($errors->any())
                                                 <div class="alert alert-danger">
                                                     <ul>
                                                         <li>
                                                             {{$errors->first()}}
                                                         </li>
                                                     </ul>
                                                 </div><br />
                                             @endif
                                         </div>
                                        </form>
                                  </div>
                                </div>
                              </div>
                              <!-- end madal edit notes -->
                                <div class="col-lg-8 col-md-8">
                                  @if ($design->delivery_date>Carbon::now()->toDateString() || $design->received_date>Carbon::now()->toDateString())
                              <form action="{{ route('design.destroy', $design->id)}}" method="post">
                                  @csrf
                                  @method('DELETE')
                              <button class="btn btn-danger btn-xs">
                                <i class="fa fa-trash-o "></i>
                               </button>
                                </form>
                                @endif
                                    <a href="{{ route('measure.edit',$design->id)}}" class="" data-toggle="modal" data-target="#basicModal{{$design->body_measurements->id}}">
                                      
                                        عرض القياسات
                                    </a>
                                    <!-- start madal edit notes -->
                                    <div class="modal fade" id="basicModal{{$design->body_measurements->id}}" tabindex="-1" role="  dialog" aria-labelledby="basicModal{{$design->body_measurements->id}}" aria-hidden="true">
                                        <div class="modal-dialog ">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                    <h4 class="modal-title centered" id="myModalLabel">تعديل المقاسات التفصيلية </h4>
                                                </div>
                                                <form method="post" action="{{ route('measure.update', $design->body_measurements->id) }}" enctype="multipart/form-data"  >
                                                    @method('PATCH')
                                                    @csrf

                                                    <div class="modal-body">

                                                        <div class="form-group">
                                                            <label for="inputPassword6">محيط الصدر :</label>
                                                            <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="chest_circumference" value="{{$design->body_measurements->chest_circumference}}"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputPassword6">محيط الخصر  :</label>
                                                            <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="waistline" value="{{$design->body_measurements->waistline}}"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputPassword6">محيط الورك  :</label>
                                                            <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="hip_circumference" value="{{$design->body_measurements->hip_circumference}}"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputPassword6">عرض الكتف :</label>
                                                            <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="shoulder_width" value="{{$design->body_measurements->shoulder_width}}"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputPassword6">طول الكتف :</label>
                                                            <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="shoulder_length" value="{{$design->body_measurements->shoulder_length}}"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputPassword6">طول الظهر :</label>
                                                            <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="back_length" value="{{$design->body_measurements->back_length}}"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputPassword6">طول النهد :</label>
                                                            <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="breast_length" value="{{$design->body_measurements->breast_length}}" />
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputPassword6">المسافة بين النهدين :</label>
                                                            <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"   name="distance_breasts" value="{{$design->body_measurements->distance_breasts}}"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputPassword6">طول الكم :</label>
                                                            <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="Sleeve_Length" value="{{$design->body_measurements->Sleeve_Length}}"/>
                                                        </div>
                                                        <div class="form-group">
                                                            <label for="inputPassword6">محيط أسوارة الكم :</label>
                                                            <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3"  aria-describedby="passwordHelpInline" name="cuff_bracelet" value="{{$design->body_measurements->cuff_bracelet}}"/>
                                                        </div>

                                                        <div class="form-group">
                                                            <div class="form-group">
                                                                <label for="inputPassword6">طول التنورة :</label>
                                                                <input type="text" list="piece" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="skirt_length" value="{{$design->body_measurements->skirt_length}}"/>
                                                            </div>

                                                        </div>
                                                        <div class="modal-footer ">
                                                          @if ($design->delivery_date>Carbon::now()->toDateString() || $design->received_date>Carbon::now()->toDateString())
                                                            <button type="submit" class="btn btn-theme02 pull-left" data-dismiss="modal{{$design->body_measurements->id}}">تعديل </button>
                                                            @endif
                                                        </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>

                                </div>

{{--                              <a class="btn btn-success btn-xs" href="/display/{{$design->id}}">--}}
{{--                                  <i class="fa fa-plus"></i>--}}
{{--                              </a>--}}

                            </div>
                          </div>
                        </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
          <!-- end all contract -->
            </div>
        </div>
    </div>
</div>
</div>
@endsection
