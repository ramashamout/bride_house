@extends('master') @section('contents')
<div class="wrapper style3 ">
  <div class="title ">
    <h2>
    <a href="index.html">وصولة الاستلام </a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">
            <i class="fa fa-edit" style="color: black;"></i>وصولة الاستلام </a>
          </li>
          <li>
            <a data-toggle="tab" href="#edit">جميع وصولة الاستلام </a>
          </li>
        </ul>
      </div>
      <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information cotract -->
          <div id="overview" class="tab-pane active">
            <form style="padding-top: 10px; " method="post" action="{{ route('itemsDeliverd.store') }}">
              @csrf
              <div class="row ">
                <div class="col-md-8 col-lg-8" style="margin-top: 20px;">
                <div class="col-md-4 col-lg-4">
                  
                  <input id="check" type="checkbox"  name="حلق" value="حلق">
                  <label for="earring">حلق </label>
                  
                </div>
                <div class="col-md-4 col-lg-4">   
                  <input id="check" type="checkbox" name="بدلة " value="بدلة ">
                <label for="suit">بدلة </label>
              </div>
                <div class="col-md-4 col-lg-4">  
                  <input id="check" type="checkbox" name="كاب" value="كاب">
                <label for="كاب">كاب</label>
              </div>
                <div class="col-md-4 col-lg-4">
                  <input id="check" type="checkbox" name="جبونة" value="جبونة ">
                  <label for="jobon">جبونة </label>
                </div>
                <div class="col-md-4 col-lg-4">
                  <input id="check" type="checkbox"  name="طرحة طويلة " value="طرحة طويلة ">
                  <label for="رحة طويلة ">رحة طويلة </label>
                </div>
                <div class="col-md-4 col-lg-4">
                  <input id="check" type="checkbox" name="طرحة قصيرة " value="طرحة قصيرة ">
                  <label for="طرحة قصيرة ">طرحة قصيرة </label>
                </div>
                <div class="col-md-4 col-lg-4">
                  <input id="check" type="checkbox"  name="bride" value="bride">
                  <label for="فستان Bride">فستان Bride</label>
                </div>
                <div class="col-md-4 col-lg-4">
                  <input id="check" type="checkbox" name="تاج" value="تاج">
                  <label for="تاج">تاج</label>
                </div>
                <div class="col-md-4 col-lg-4"> <input id="check" type="checkbox" name="belt" value="belt">
                <label for="حزام ">حزام</label></div>
                <div class="col-md-4 col-lg-4">
                  <input id="check" type="checkbox" name="أسوارة " value="أسوارة ">
                  <label for="أسوارة ">أسوارة </label>
                </div>
                <div class="col-md-4 col-lg-4">
                  <input id="check" type="checkbox" name="خاتم" value="خاتم">
                  <label for="خاتم">خاتم</label>
                </div>
                <div class="col-md-4 col-lg-4">
                  <input id="check" type="checkbox" name="باقة ورد" value="باقة ورد">
                  <label for="باقة ورد ">باقة ورد</label>
                </div>
                
                
                <input type="hidden" name="contract_id"  value= {{$contract_id}} >
                
                
                </div>
                <div class="col-md-3 col-lg-3  ">
                 
                    <img src="/images/31.png" id="image">
                 
                </div>
                <div class=" col-lg-4 col-md-4">
                  <button type="submit" class="btn btn-theme02 pull-left"style="margin-left: 50px;margin-top: -50px;">حفظ</button>
                </div>
              </div>
            </form>
          </div>
          <!-- start all contract -->
          <div id="edit" class="tab-pane">
            <div class="row mb">
              <div class="col-lg-12 col-md-12">
                <!-- page start-->
                <div class="content-panel">
                  <div class="adv-table">
                    <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="example">
                      <thead>
                        <tr>
                           <th >الأغراض المستلمة </th>
                          <th>قيمة الاستلام </th>
                          
                         
                          
                          
                        </tr>
                      </thead>
                      <tbody>
                        @foreach($itemsDeliverd  as $itemsDeliverd)
                        <tr class="gradeU">
                          
                          <td>{{$itemsDeliverd->name}}</td>
                          <td>{{$itemsDeliverd->is_delivered}} </td>
                          
                        </tr>
                        @endforeach
                        
                        
                        
                        
                        
                        
                      </tbody>
                    </table>
                    
                  </div>
                </div>
                <!-- page end-->
              </div>
            </div>
            <!-- /row -->
          </div>
          <!-- end all contract -->
        </div>
      </div>
    </div>
  </div>
</div>
@endsection