@extends('master') @section('contents') <div class="wrapper style3 ">
  <div class="title ">
    <h2>
    <a href="index.html">عملية الصيانة</a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">
            <i class="fa fa-edit" style="color: black;"></i> معلومات الصيانة </a>
          </li>
          <li>
            <a data-toggle="tab" href="#edit">جميع عمليات الصيانة</a>
          </li>
        </ul>
      </div>
       <?php
        use Carbon\Carbon;
       ?>
      <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information cotract -->
          <div id="overview" class="tab-pane active">
            <form style="padding-top: 30px;"  method="POST" action="{{ route('maintenance.store') }}" enctype="multipart/form-data">
              @csrf
              <div class="row mt">
                <div class="col-md-6 col-lg-6 col-md-offset-2">
                   <div class="form-group">

                    <input type="hidden" name="title"  value="صيانة " >

                  </div>
                  <div class="form-group">

                    <input type="hidden" name="color"  value="pink" >

                  </div>
                        <div class="form-group">

                    <input type="hidden" name="is_check"  value=0 >

                  </div>
                   <div class="form-group">

                    <input type="hidden" name="is_check1"  value=1>

                  </div>
                  <div class="form-group">

                    <input type="hidden" name="name_customer"  value="null">

                  </div>
                  <div class="form-group">

                    <input type="hidden" name="customer_id"  value="0">

                  </div>


                  <div class="form-group">
                        <div>

                        <input  list="clothe"  class="form-control mx-sm-3"  placeholder="اسم الفستان" name="name_product"  autocomplete="off" />
                        <datalist id="clothe">
                          @foreach($clothe as $clothe)
                             @if($clothe->statue!="مستودع" && $clothe->statue!="مباع" && $clothe->statue!="في انتظار الزبون" && $clothe->statue!="مزاد")
                            <option value="{{$clothe->name}}">{{$clothe->name}} </option>
                             <input type="hidden" name="piece_id"  value="{{$clothe->id}}" >
                             @endif
                         @endforeach
                        </datalist>

                    </div>
                  </div>
                  <script type="text/javascript">
                   $(#name_product).select2({
                   placeholder:"اسم الفستان ",
                   allowClear:true
                    });

                   </script>







                  <div class="form-group ">
                    <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" placeholder="الملاحظة" name="note_content"  autocomplete="off">
                  </div>
                   <div class="form-group">
                    <input type="text"  onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'"
                    placeholder="تاريخ التسليم " class="form-control" name="start"/>
                  </div>
                  <div class="form-group">
                    <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'"
                    placeholder="تاريخ الاستلام" class="form-control" name="end"/>
                  </div>


                </div>
                <!-- start image cleaning-->
                <div class="col-md-3 col-lg-3">
                  <img src="images/33.jpg" id="image">
                </div>
                <!-- end image customer-->
                <div class=" col-lg-4 col-md-4">
                  <button type="submit" class="btn btn-theme04 pull-left" style="margin-left: 50px;margin-top: -60px;">حفظ</button>
                </div>
              </div>
                <div>
                    @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    <li>
                                        {{$errors->first()}}
                                    </li>
                                </ul>
                            </div><br />
                        @endif
                </div>
            </form>
          </div>
          <!-- end information contract -->
          <!-- start all contract -->
          <div id="edit" class="tab-pane">
            <div class="row mt">
              <div class="col-md-12">
                <section class="task-panel tasks-widget">
                  <div class="panel-body">
                    <div class="task-content">
                      <ul class="task-list">
                        @foreach($maintenance as $maintenance)
                        <li>
                          <div class="task-title">
                            <span class="task-title-sp">
                              <i class="fa fa-circle" style="font-size:10px"></i>&nbsp;{{$maintenance->name_product}}&nbsp; <a href="#" id="mytooltip" data-toggle="tooltip" title="{{$maintenance->end}}" data-placement="left">
                                <i class="fa fa-calendar " style="font-size:15px ; color:black; "></i>
                              </a>
                            </span>
                            <div class="pull-left hidden-phone">

                              <a href="{{ route('maintenance.edit',$maintenance->id)}}" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#basicModal{{$maintenance->id}}">
                                @if ($maintenance->start>Carbon::now()->toDateString() || $maintenance->end>Carbon::now()->toDateString())
                                <i class=" fa fa-pencil"></i>
                                @else
                                <i class=" fa fa-eye"></i>
                                @endif
                              </a>

                              <!-- start madal edit notes -->
                              <div class="modal fade" id="basicModal{{$maintenance->id}}" tabindex="-1" role="  dialog" aria-labelledby="basicModal{{$maintenance->id}}" aria-hidden="true">
                                <div class="modal-dialog ">
                                  <div class="modal-content">
                                    <div class="modal-header">
                                      <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                                      <h4 class="modal-title centered" id="myModalLabel">تعديل عملية الصيانة </h4>
                                    </div>
                                    <form  method="post" action="{{ route('maintenance.update', $maintenance->id) }}" enctype="multipart/form-data" >
                                      @method('PATCH')
                                      @csrf
                                      <div class="modal-body">
                                        <div class="container-fluid">
                                          <div class="row ">
                                            <div class="col-md-12 col-lg-12">
                                               <div class="form-group">

                                                <input type="hidden" name="title"  value="صيانة" >

                                              </div>
                                              <div class="form-group">

                                                <input type="hidden" name="color"  value="pink" >

                                              </div>
                                                  <div class="form-group">

                                                        <input type="hidden" name="is_check"  value=0 >

                                                      </div>
                                                       <div class="form-group">

                                                        <input type="hidden" name="is_check1"  value=1>

                                                      </div>
                                              <div class="form-group">
                                                    <div>
                                                      <label for="inputPassword6">اسم الفستان </label>
                                                    <input type="text" list="clothe" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline"  name="name_product" value="{{$maintenance->name_product}}" autocomplete="off" />
                                                  <?php
                                                     $clothes=App\Models\Clothe::all();
                                                  ?>
                                      <datalist id="clothe">
                          @foreach($clothes as $clothe)
                             @if($clothe->statue!="مستودع" && $clothe->statue!="مباع" && $clothe->statue!="في انتظار الزبون" && $clothe->statue!="مزاد")
                            <option value="{{$clothe->name}}">{{$clothe->name}} </option>
                             <input type="hidden" name="piece_id"  value="{{$clothe->id}}" >
                             @endif
                         @endforeach
                        </datalist>
                                                </div>
                                              </div>

                                             <div class="form-group">

                                                <input type="hidden" name="name_customer"  value="null">

                                              </div>
                                              <div class="form-group">

                                                <input type="hidden" name="customer_id"  value="0">

                                              </div>
                                              <div class="form-group">
                                                <label for="inputPassword6">الملاحظة</label>
                                                <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="note_content" value="{{$maintenance->note_content}}" autocomplete="off">
                                              </div>
                                              <div class="form-group">
                                                <label for="inputPassword6"> تاريخ الاستلام من الورشة</label>
                                                <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'"
                                                placeholder="تاريخ الاستلام" class="form-control"name="end"  value="{{$maintenance->end}}" />
                                              </div>
                                              <div class="form-group">
                                                <label for="inputPassword6"> تاريخ التسليم للورشة </label>
                                                <input type="text"  onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'"
                                                placeholder="تاريخ التسليم " class="form-control" name="start"
                                                value="{{$maintenance->start}}" />
                                              </div>


                                            </div>
                                          </div>
                                        </div>
                                      </div>
                                      <div class="modal-footer ">
                                        @if ($maintenance->start>Carbon::now()->toDateString() || $maintenance->end>Carbon::now()->toDateString())
                                        <button type="submit"  class="btn btn-theme02 pull-left" data-dismiss="modal{{$maintenance->id}}">تعديل </button>
                                        @endif
                                      </div>
                                        <div>
                                           @if($errors->any())
                            <div class="alert alert-danger">
                                <ul>
                                    <li>
                                        {{$errors->first()}}
                                    </li>
                                </ul>
                            </div><br />
                        @endif
                                        </div>
                                    </form>
                                  </div>
                                </div>
                              </div>
                              <!-- end madal edit notes -->
                              <div class="col-lg-8 col-md-8">
                                  @if ($maintenance->start>Carbon::now()->toDateString() || $maintenance->end>Carbon::now()->toDateString())
                              <form action="{{ route('maintenance.destroy', $maintenance->id)}}" method="post">
                                @csrf
                                @method('DELETE')
                                <button class="btn btn-danger btn-xs" class="button"
                                method="post">
                                <i class="fa fa-trash-o "></i>
                                </button>
                                @endif
                              </form>
                            </div>



                            </div>
                          </div>
                        </li>
                        @endforeach
                      </ul>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
          <!-- end all contract -->
        </div>
      </div>
      <!-- end panel-body -->
    </div>
  </div>
</div> @endsection
