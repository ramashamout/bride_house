@extends('master') @section('contents') <div class="wrapper style3 ">
  <div class="title ">
    <h2>
      <a href="index.html">عملية الصيانة</a>
    </h2>
  </div>
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">
              <i class="fa fa-edit" style="color: black;"></i> معلومات الصيانة </a>
          </li>

        </ul>
      </div>
      <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information cotract -->
          <div id="overview" class="tab-pane active">
            <form style="padding-top: 30px;" method="POST" action="{{ route('maintenance.store') }}" enctype="multipart/form-data">
              @csrf
              <div class="row mt">
                <div class="col-md-6 col-lg-6 col-md-offset-2">
                  <div class="form-group">
                    <input type="hidden" name="title" value="صيانة ">
                  </div>
                  <div class="form-group">
                    <input type="hidden" name="color" value="pink">
                  </div>
                  <div class="form-group">
                    <input type="hidden" name="customer_id" value="{{$customer->id}}">
                  </div>
                  <div class="form-group">
                    <input type="hidden" name="name_customer" value="{{$customer->full_name}}">
                  </div>
                  <div class="form-group">

                    <input type="hidden" name="is_check" value=0>

                  </div>
                  <div class="form-group">

                    <input type="hidden" name="is_check1" value=1>

                  </div>
                  @foreach($customer->contractes as $cust)

                  @foreach($cust->piece_contracts as $p)
                  @if($p->clothe!=null)
                  <div class="form-group">
                    <input type="hidden" name="name_product" value="{{$p->clothe['name']}}">
                    <input type="hidden" name="piece_id" value="{{$p->clothe['id']}}">
                  </div>
                  @endif
                  @endforeach
                  @endforeach
                  <div class="form-group ">
                    <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" placeholder="الملاحظة" name="note_content">
                  </div>
                  <div class="form-group">
                    <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" placeholder="تاريخ الاستلام" class="form-control" name="end" />
                  </div>
                  <div class="form-group">
                    <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" placeholder="تاريخ التسليم " class="form-control" name="start" />
                  </div>

                </div>
                <!-- start image cleaning-->
                <div class="col-md-3 col-lg-3">
                  <img src="/images/33.jpg" id="image">
                </div>
                <!-- end image customer-->
                <div class=" col-lg-4 col-md-4">
                  <button type="submit" class="btn btn-theme02 pull-left" style="margin-left: 50px;margin-top: -60px;">حفظ</button>

                </div>
                <div class=" col-lg-4 col-md-4">
                  <a type="button" class="btn btn-success pull-left" style="margin-left: 50px;margin-top: -60px;" href="/customer/{{$customer->id}}/edit">صفحة الملاحظات الأساسية </a>
                </div>
              </div>
            </form>
          </div>
          <!-- end information contract -->

        </div>
      </div>
      <!-- end panel-body -->
    </div>
  </div>
</div> @endsection