@extends('master') @section('contents') <div class="wrapper style3">
  <div class="title">
    <h2>
      <a href="index.html"> العملاء </a>
    </h2>
  </div>
  <!-- Content -->
  <div class="container">
    <div class="panel panel-default">
      <!-- start panel heading -->
      <div class="panel-heading ">
        <ul class="nav nav-tabs nav-justified">
          <li class="active">
            <a data-toggle="tab" href="#overview">معلومات العميل </a>
          </li>
          <li>
            <a data-toggle="tab" href="#status" class="contact-map"> حالة السلعة </a>
          </li>
          {{-- <li>--}}
          {{-- <a data-toggle="tab" href="#edit">ملاحظات الصيانة </a>--}}
          {{-- </li>--}}
        </ul>
      </div>
      <!-- start panel-body -->
      <div class="panel-body">
        <div class="tab-content">
          <!-- start information customer -->
          <div id="overview" class="tab-pane active">
            <form style="padding-top: 50px;" method="post" action="{{ route('customer.update', $customer->id) }}" enctype="multipart/form-data">
              @method('PATCH')
              @csrf
              <div class="row">
                <div class="col-md-6 col-lg-6 col-md-offset-2">
                  <div class="form-group">
                    <input type="text" id="inputPassword6" class="form-control" aria-describedby="passwordHelpInline" placeholder="اسم الزبون" name="full_name" value="{{$customer->full_name}}">
                  </div>
                  <div class="form-group">
                    <input type="text" id="inputPassword6" class="form-control" aria-describedby="passwordHelpInline" placeholder="رقم الهاتف" name="mobile_number" value="{{$customer->mobile_number}}">
                  </div>
                  <div class="form-group ">
                    <input type="text" id="inputPassword6" class="form-control" aria-describedby="passwordHelpInline" placeholder="العنوان" name="address" value="{{$customer->address}}">
                  </div>
                </div>
                <!-- start image customer-->
                <div class="col-md-3 col-lg-3">
                  <img src="/images/images (1).png" id="image">
                </div>
                <!-- end image customer-->
                <div class=" col-md-4 col-lg-4 ">
                  <button type="submit" class="btn btn-theme02 pull-left" style="margin-left: 50px; margin-top: -80px;">تعديل</button>
                </div>
              </div>
            </form>
          </div>
          <!-- end information customer -->
          <!-- start status dress -->
          <div id="status" class="tab-pane">
            <div class="row mt">
              <div class="col-md-12">
                <!-- start section note to one dress-->
                <section id="unseen">
                  <table cellpadding="0" cellspacing="0" border="0" class="display table table-bordered" id="example">
                    <thead>
                      <tr>
                        <th>اسم الفستان </th>
                        <th>المجموعة</th>
                        <th>الملحقات </th>
                        <th>تاريخ البداية </th>
                        <th>تاريخ النهاية </th>
                        {{-- <th>مؤجر </th>--}}
                        {{-- <th>إرجاع مؤجر </th>--}}
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($customer->contractes as $cust)
                      @foreach($cust->piece_contracts as $p)
                      @if ($p->clothe!=null)
                      <tr>


                        <td>{{$p->clothe['name']}}</td>

                        <td>{{$p->clothe['clothes_type']}}</td>
                        @else
                        <td>
                          <p>
                            <a class="btn btn-primary btn-xs " data-toggle="collapse" href="#collapseExample{{$cust->start}}{{$p->clothe['name']}}" role="button" aria-expanded="false" aria-controls="collapseExample">
                              <i class=" fa fa-eye"></i>
                            </a>
                          </p>

                          <div class="collapse" id="collapseExample{{$cust->start}}{{$p->clothe['name']}}">
                            <ul class="task-list" style="float: right;  list-style: none;">
                              <li>
                                <input type="checkbox" class="checked ">
                                <a style="color: black;">{{$p->accessory['name']}}</a>
                              </li>

                            </ul>
                          </div>

                        </td>

                        <td style=" font-size:16px;">{{$cust->start}}</td>
                        <td style=" font-size:16px;">{{$cust->end}}</td>

                        {{-- <td>--}}

                        {{-- <input class="form-check-input" type="checkbox" name="flexRadioDefault"--}}
                        {{-- checked>--}}
                        {{-- </td>--}}

                        {{-- <td>--}}


                        {{-- <form method="GET" action="/contract/update_color/{{$cust->id}}" enctype="multipart/form-data">--}}
                        {{-- @csrf--}}
                        {{-- <a href="/contract/update_color/{{$cust->id}}" class="btn-xs " class="button">--}}
                        {{-- <input class="form-check-input"  name="flexRadioDefault"--}}
                        {{-- type="checkbox">--}}
                        {{-- </a>--}}

                        {{-- </form>--}}
                        {{-- </td>--}}

                      </tr>
                      @endif

                      @endforeach
                      @endforeach

                      @foreach($customer->designs as $design)
                      <tr>
                        <td> قطعة موديل {{$design->model}} </td>

                        <td>تصميم</td>
                        <td>--</td>

                        <td style=" font-size:16px;">{{$design->delivery_date}}</td>
                        <td style=" font-size:16px;">{{$design->received_date}}</td>
                      </tr>

                      @endforeach
                    </tbody>
                  </table>
                </section>
                <!-- end section note to one dress-->
              </div>
            </div>

          </div>
          <!-- end status dress -->
          <!-- start notes maintenace -->



          @foreach($customer->contractes as $cust)
          @foreach($cust->piece_contracts as $p)

          <div id="edit" class="tab-pane">


            <div class="row mt">



              <div class="col-md-12">
                <!-- start section notes to one dress-->


                <section class="task-panel tasks-widget ">

                  <div class="panel-heading">


                    <div class="pull-right" style="margin-top: -5px;">

                      <h5 style="padding: 8px 30px;
                          background: #e0e1e7;color: #5a5a5a;">

                        <i class="fa fa-scissors"></i>{{$p->clothe['name']}}

                      </h5>

                    </div>





                    <!-- start buttom edit -->
                    <a class="btn btn-success btn-sm pull-left" style="margin-right:5px;margin-top: -5px; " href="/customer_note/{{$customer->id}}">
                      <i class="fa fa-plus "></i> إضافة ملاحظة </a>
                    <!--end buttom edit -->

                    <!-- start buttom show all notes -->
                    <p>
                      <a class="btn btn-theme02 btn-sm  pull-left" data-toggle="collapse" href="#collapse3Example{{$p->clothe['id']}}" role="button" aria-expanded="false" aria-controls="collapse3Example{{$p->clothe['id']}}" style="margin-top: -5px;">
                        <i class="fa fa-eye "></i> عرض الملاحظات </a>
                    </p>
                    <!-- end buttom show all notes -->
                    <!-- start collapse show all notes -->
                    <div class="collapse" id="collapse3Example{{$p->clothe['id']}}">
                      <div class="panel-body">
                        <!-- start all notes to one dress -->
                        <div class="task-content">
                          <ul class="task-list">
                            <br>
                            <!-- start note to one dress -->
                            @foreach($customer->maintenance_notes as $cust)
                            <li>
                              <div class="task-title">
                                <span class="task-title-sp">
                                  <i class="fa fa-circle" style="font-size:10px"></i>&nbsp;{{$cust->note_content}} &nbsp; <a href="#" id="mytooltip" data-toggle="tooltip" title="{{$cust->end}}" data-placement="left">
                                    <i class="fa fa-calendar " style="font-size:15px ; color:black; "></i>
                                  </a>
                                </span>
                                <div class="pull-left hidden-phone">
                                  <a href="{{ route('maintenance.edit',$cust->id)}}" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#basicModal{{$cust->id}}">
                                    <i class=" fa fa-pencil"></i>
                                  </a>
                                  <!-- start madal edit notes -->
                                  <div class="modal fade" id="basicModal{{$cust->id}}" tabindex="-1" role="  dialog" aria-labelledby="basicModal{{$cust->id}}" aria-hidden="true">
                                    <div class="modal-dialog">
                                      <div class="modal-content">
                                        <div class="modal-header">
                                          <button type="button" class="close pull-left" data-dismiss="modal" aria-hidden="true">&times;</button>
                                          <h4 class="modal-title centered" id="myModalLabel">تعديل الملاحظة </h4>
                                        </div>
                                        <form method="post" action="{{ route('maintenance.update', $cust->id) }}" enctype="multipart/form-data">
                                          @method('PATCH')
                                          @csrf
                                          <div class="modal-body">
                                            <div class="form-group">

                                              <input type="hidden" name="title" value="صيانة">

                                            </div>
                                            <div class="form-group">

                                              <input type="hidden" name="color" value="pink">

                                            </div>

                                            <div class="form-group">

                                              <input type="hidden" name="name_product" value="{{$cust->name_product}}">
                                              <input type="hidden" name="piece_id" value="{{$cust->piece_id}}">
                                            </div>

                                            <div class="form-group">

                                              <input type="hidden" name="name_customer" value="{{$cust->name_customer}}">
                                            </div>
                                            <div class="form-group">
                                              <input type="hidden" name="customer_id" value="{{$cust->id}}">
                                            </div>
                                            <div class="form-group">
                                              <label for="inputPassword6">الملاحظة</label>
                                              <input type="text" id="inputPassword6" class="form-control mx-sm-3" aria-describedby="passwordHelpInline" name="note_content" value="{{$cust->note_content}}">
                                            </div>
                                            <div class="form-group">
                                              <label for="inputPassword6">تاريخ التسليم </label>
                                              <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" placeholder="تاريخ الاستلام" class="form-control" name="end" value="{{$cust->end}}" />
                                            </div>
                                            <div class="form-group">
                                              <label for="inputPassword6">تاريخ الاستلام </label>
                                              <input type="text" onfocus="(this.type='date')" onblur="if(!this.value)this.type='text'" placeholder="تاريخ التسليم " class="form-control" name="start" value="{{$cust->start}}" />
                                            </div>


                                          </div>
                                          <div class="modal-footer ">
                                            <button type="submit" class="btn btn-theme02 pull-left" data-dismiss="modal{{$cust->id}}">تعديل </button>
                                          </div>
                                        </form>
                                      </div>
                                    </div>
                                  </div>
                                  <!-- end madal edit notes -->
                                  <div class="col-lg-8 col-md-8">
                                    <form action="{{ route('maintenance.destroy', $cust->id)}}" method="post">
                                      @csrf
                                      @method('DELETE')
                                      <button class="btn btn-danger btn-xs" class="button" method="post">
                                        <i class="fa fa-trash-o "></i>
                                      </button>
                                    </form>
                                  </div>
                                </div>
                              </div>
                            </li>
                            @endforeach
                            <!-- end note to one dress -->
                          </ul>
                        </div>
                        <!-- end all notes to one dress  -->
                      </div>
                    </div>
                    <!-- end collapse show all notes -->
                    <br>

                  </div>



                </section>




                <!-- end section notes to one dress-->
              </div>



            </div>



          </div>
          @endforeach
          @endforeach









          <!-- end notes maintenace -->
        </div>
      </div>
      <!-- end panel-body -->
    </div>
  </div>
</div> @endsection