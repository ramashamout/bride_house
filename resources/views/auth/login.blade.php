<!doctype html>
<html lang="ar-sa" dir="rtl">
  <head>
  
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


   <link rel="stylesheet" type="text/css" href="/fonts/DroidKufi/EARLY_ACCESS.css">
   <link rel="stylesheet" href="/login1/css/style.css">

   </head>
 
<style type="text/css">
  .form-control:focus {
        border-color: #c3c3c3;
        box-shadow: 0 0 0 0.2rem rgba(40, 167, 69, 0.25);
    } 

</style>

   <body >

   <section class="ftco-section">
      <div class="container">
        
         <div class="row justify-content-center">
            <div class="col-md-12 col-lg-10">
               <div class="wrap d-md-flex">
                  <div class="img" style="background-image: url(/login1/images/2.jpg);">
               </div>
               
                <div class="login-wrap p-4 p-md-5">
                  <div class="d-flex">
                     <div class="w-100" style="margin-left:100px;">
                        <h3 class="mb-4">تسجيل الدخول</h3>
                     </div>
                       
                  </div>
                    <form  method="POST" action="{{ route('logi') }}" class="display signin-form"> @csrf
                    @if (session('error'))
                      <div class="alert alert-danger pull-right">
                       {{ session('error') }}
                      </div>
                          @endif
                     <div class="form-group mb-3">
                        <label class="label" for="name" style="margin-left:250px;">البريد الإلكتروني</label>
                      <input name="email" :value="old('email')"  class="form-control" type="text" placeholder="البريد الإلكتروني" required autofocus >
                     </div>
                  <div class="form-group mb-3">
                     <label class="label" for="password" style="margin-left:280px;">كلمة المرور</label>
                      <input class="form-control" type="password" name="password" autocomplete="current-password" placeholder="كلمة المرور" required >
                  </div>
                  <div class="form-group">
                     <button type="submit" class="form-control btn  rounded submit px-3" style="background: #c3c3c3;">تسجيل الدخول</button>
                  </div>
                  




                </form>
              
              </div>
              

            </div>
            </div>
         </div>
      </div>
   </section>

   <script src="js/jquery.min.js"></script>
  <script src="js/popper.js"></script>
  <script src="js/bootstrap.min.js"></script>
  <script src="js/main.js"></script>

   </body>
</html>

