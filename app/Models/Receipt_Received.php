<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Receipt_Received extends Model
{
    use HasFactory;
     protected $connection="mysql2";
     public function contract()
    {
        return $this->belongsTo(Contract::Class,'contract_id');
    }

    protected $fillable = [
        'contract_id',
        'amount',
        'description',
        'receipt_date'
    ];
}
