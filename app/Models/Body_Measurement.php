<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Body_Measurement extends Model
{
    use HasFactory;
     protected $connection="mysql2";
        public function design()
    {
        return $this->belongsTo(Design:: Class,'design_id');
    }
     protected $fillable = [
        'chest_circumference',
        'waistline',
        'hip_circumference',
        'shoulder_width',
        'shoulder_length',
        'back_length',
        'breast_length',
        'distance_breasts',
        'Sleeve_Length',
        'cuff_bracelet',
        'skirt_length',
        'design_id',
    ];
}
