<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class fcm_notify extends Model
{
    use HasFactory;
      protected $connection="mysql2";
        protected $fillable = [
        'user','title', 'body'

    ];
       public function user()
    {
        return $this->belongsTo('App\Models\User','user');
    }
}
