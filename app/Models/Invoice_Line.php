<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice_Line extends Model
{
    use HasFactory;
     protected $connection="mysql2";
     public function invoice()
    {
        return $this->belongsTo(Invoice::Class,'invoice_id');
    }
    public function piece()
    {
        return $this->belongsTo(Piece:: Class,'piece_id');
    }
    protected $fillable = [
        'invoice_id',
        'description',
        'number_of_pieces',
        'unit_price',
        'total_price'
    ];

    protected $table = 'invoice__lines';

    public static function boot() {
        parent::boot();

        static::deleting(function($clean) {
//            $clean->piece()->store_piece()->delete();
            $clean->piece()->delete();


        });
    }
}
