<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Maintenance_Note extends Model
{
     use HasFactory;
      protected $connection="mysql2";
     public function piece()
    {
        return $this->belongsTo(Piece:: Class,'piece_id');
    }
    public function customer()
    {
        return $this->belongsTo(Customer:: Class,'customer_id');
    }
    public function calendre()
    {
         return $this->belongsTo(Calendar:: Class,'piece_id');
    }
    protected $fillable = [
        'piece_id',
        'customer_id',
        'note_content',
        'name_product',
        'name_customer',
        'start',
        'end',
        'is_check',
        'is_check1',

    ];
      public static function boot() {
        parent::boot();

        static::deleting(function($maintenance) {
            $maintenance->calendre()->delete();
            
        });
    }
}
