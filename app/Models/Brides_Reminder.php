<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Brides_Reminder extends Reminder
{
    use HasFactory;
     protected $connection="mysql2";
      protected $fillable = [
        'customer_name',
        'date',
        'mobile_number',
        'emboridery',
          'content'
    ];
    protected $table = 'reminders';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('bride', function (Builder $builder) {
            $builder->where('type', 'bride');
        });

        static::creating(function ($bride) {
            $bride->type = 'bride' ;
        });
    }

}
