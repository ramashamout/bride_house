<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Pieces_Reminder extends Reminder
{
    use HasFactory;
     protected $connection="mysql2";
     protected $fillable = [
        'content',
        'date',
        'mobile_number',
        'customer_name'
    ];
    protected $table = 'reminders';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('piece', function (Builder $builder) {
            $builder->where('type', 'piece');
        });

        static::creating(function ($piece) {
            $piece->type = 'piece' ;
        });
    }
}
