<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Contract extends Model
{
    use HasFactory;
     protected $connection="mysql2";
     public function piece_contracts()
    {
        return $this->hasMany(Piece_Contract:: Class,'contract_id');
    }
    public function receipt_receiveds()
    {
        return $this->hasMany(Receipt_Received:: Class,'contract_id');
    }
    public function items_delivereds()
    {
        return $this->hasMany(Items_Delivered:: Class,'contract_id');
    }
    public function customer()
    {
        return $this->belongsTo(Customer:: Class,'customer_id');
    }
        public function calendre()
    {
         return $this->belongsTo(Calendar:: Class,'piece_id');
    }
    public function clothe()
    {
        return $this->belongsTo(Clothe:: Class,'piece_id');
    }
    protected $fillable = [
        'customer_id',
        'insurance_price',
        'total_price',
        'deposit',
        'first_batch',
        'second_batch',
        'hall',
        'start',
        'end',
        'contract_date',
        'event_date',
        'contract_type',
        'title',
        'acc_id',
        'color',
        'piece_id',
        'is_ckeck',
    ];
      public static function boot() {
        parent::boot();

        static::deleting(function($contract) {
            $contract->piece_contracts()->delete();
            $contract->receipt_receiveds()->delete();
            $contract->items_delivereds()->delete();
            $contract->calendre()->delete();
        });
    }
}
