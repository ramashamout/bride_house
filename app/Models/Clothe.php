<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;

class Clothe extends Piece
{
    use HasFactory;
     protected $connection="mysql2";
    public function image()
{
return $this->hasMany(Image:: Class,'id_product');
}
public function sizes()
{
return $this->hasMany(size:: Class,'product_id');
}
public function calendar()
{
return $this->belongsTo(Calendar:: Class,'piece_id');
}
  public function design()
    {
        return $this->hasOne(Design:: Class,'piece_id');
    }
    public function maintenance_notes()
    {
        return $this->hasMany(Maintenance_Note:: Class,'piece_id');
    }
    public function clean()
    {
        return $this->hasMany(Cleaning:: Class,'piece_id');
    }
    public function contracts()
    {
        return $this->hasMany(Contract:: Class,'piece_id');
    }
    public function clothe_type()
    {
        return $this->belongsTo(Type:: Class,'clothes_type');
    }
    protected $fillable = [
        'name',
        'piece_img',
        'selling_price',
        'old_price',
        'rent_price',
        'designer_name',
        'color',
        'discount',
        'size',
        'clothes_type',
        'other_image',
        'statue',
        'inFavorites',

    ];
    protected $table = 'pieces';

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('clothe', function (Builder $builder) {
            $builder->where('type', 'clothe');
        });

        static::creating(function ($clothe) {
            $clothe->type = 'clothe' ;
        });

        static::deleting(function($clothe) { // before delete() method call this
            $clothe->design()->delete();
            $clothe->maintenance_notes()->delete();
            $clothe->clean()->delete();
        });
    }
}









