<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class action extends Model
{
    use HasFactory;
    protected $connection="mysql2";
       protected $fillable = [
            'user_id',
            'price',
            'action_date'
    ];
      public function user()
    {
        return $this->belongsTo('App\Models\User' ,'user_id');
    }

}
