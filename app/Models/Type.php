<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Type extends Model
{
    use HasFactory;
     protected $connection="mysql2";
    public function clothe()
    {
        return $this->hasMany(Clothe:: Class,'clothes_type');
    }
  public function accessory()
    {
        return $this->hasMany(Accessory:: Class,'Type_accessory');
    }
    protected $fillable = [
        'name',
        'image',
        'type_piece'
    ];
}
