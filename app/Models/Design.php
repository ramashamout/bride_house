<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Design extends Model
{
    use HasFactory;
     protected $connection="mysql2";
     public function body_measurements()
    {
        return $this->hasOne(Body_Measurement:: Class,'design_id');
    }
    public function customer()
    {
        return $this->belongsTo(Customer:: Class,'customer_id');
    }
    public function piece()
    {
        return $this->belongsTo(Piece:: Class,'piece_id');
    }
    protected $fillable = [
        'customer_id',
        'piece_id',
        'color',
        'fabric_type',
        'model',
        'general_size',
        'design_notes',
        'received_date',
        'delivery_date',
        'designing_price',
        'type',
        'design_img'
    ];
    public static function boot() {
        parent::boot();

        static::deleting(function($measure) {
            $measure->body_measurements()->delete();
//            $measure->piece()->delete();
        });
    }
}
