<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Appointment extends Model
{
    use HasFactory;
    protected $connection="mysql2";
    
      protected $fillable = [
            'title',
            'color',
            'mobile_number',
            'type',
            'start',
            'end',
            'date',
            'time'
    ];
      public function user()
    {
        return $this->belongsTo('App\Models\User');
    }
    
}
