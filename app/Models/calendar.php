<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Calendar extends Model
{
    use HasFactory;
 protected $connection="mysql2";
     protected $fillable = [
        'title',
        'end',
        'start',
        'color',
       ' piece_id',
    ];
    public function maintenance()
    {
    	  return $this->hasMany(Maintenance_Note:: Class,'piece_id');
    }
    public function clean()
    {
    	return $this->hasMany(Cleaning:: Class,'piece_id');
    }
     public function contract()
    {
        return $this->hasMany(contract:: Class,'piece_id');
    }
}
