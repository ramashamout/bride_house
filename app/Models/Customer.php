<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    use HasFactory;
     protected $connection="mysql2";
     public function maintenance_notes()
    {
        return $this->hasMany(Maintenance_Note:: Class,'customer_id');
    }
    public function designs()
    {
        return $this->hasMany(Design:: Class,'customer_id');
    }
    public function contractes()
    {
        return $this->hasMany(Contract:: Class,'customer_id');
    }
    protected $fillable = [
        'full_name',
        'mobile_number',
        'address'
    ];
      public static function boot() {
        parent::boot();

        static::deleting(function($customer) {
            $customer->maintenance_notes()->delete();
            $customer->designs()->delete();
            $customer->contractes()->delete();
        });
    }
}
