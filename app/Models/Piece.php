<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Piece extends Model
{
    use HasFactory;
   protected $connection="mysql2";
    public function piece_contracts()
    {
        return $this->hasMany(Piece_Contract:: Class,'piece_id');
    }
    public function invoice_line()
    {
        return $this->hasOne(Invoice_Line:: Class,'piece_id');
    }
    public function store_piece()
    {
        return $this->hasOne(Store_Clothe:: Class,'piece_id');
    }
    public function image()
    {
        return $this->hasMany(Image:: Class,'id_product');
    }

    protected $fillable = [
        'name',
        'piece_img',
        'selling_price',
        'rent_price',
        'type',
        'designer_name',
        'size',
        'color',
        'clothes_type',
        'discount',
        'old_price',
        'other_image',
        'Type_accessory',
        'category',
        'statue_acc',
        'statue',
    ];


}
