<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Accessory extends Piece
{
    use HasFactory;
     protected $connection="mysql2";
    protected $fillable = [
          'name',
            'piece_img',
            'selling_price',
            'rent_price',
            'category',
            'Type_accessory',
            'statue_acc'
    ];
           protected $table = 'pieces';
  public function Type_accessory()
    {
        return $this->belongsTo(Type:: Class,'Type_accessory');
    }
    public function piece_contracts()
    {
        return $this->hasMany(Piece_Contract:: Class,'piece_id');
    }

    protected static function boot()
    {
        parent::boot();

        static::addGlobalScope('accessory', function (Builder $builder) {
            $builder->where('type', 'accessory');
        });

        static::creating(function ($accessory) {
            $accessory->type = 'accessory' ;
        });
        static::deleting(function($accessory) {
            $accessory->piece_contracts()->delete();
        });
    }
}
