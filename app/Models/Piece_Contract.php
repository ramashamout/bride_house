<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Piece_Contract extends Model
{
    use HasFactory;
     protected $connection="mysql2";
     public function contract()
    {
        return $this->belongsTo(Contract:: Class,'contract_id');
    }
     public function clothe()
    {
        return $this->belongsTo(Clothe:: Class,'piece_id');
    }
    public function accessory()
    {
        return $this->belongsTo(Accessory:: Class,'piece_id');
    }
    public function piece()
    {
        return $this->belongsTo(Piece:: Class,'piece_id');
    }
       protected $fillable = [
        'contract_id',
        'piece_id'
    ];
}
