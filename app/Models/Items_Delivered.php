<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Items_Delivered extends Model
{
    use HasFactory;
     protected $connection="mysql2";
     public function contract()
    {
        return $this->belongsTo(Contract:: Class,'contract_id');
    }

    protected $fillable = [
        'contract_id',
        'is_delivered',
        'name'
    ];
}
 