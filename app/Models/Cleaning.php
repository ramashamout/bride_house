<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cleaning extends Model
{
    use HasFactory;
     protected $connection="mysql2";
     public function clothe()
    {
        return $this->belongsTo(Clothe:: Class,'piece_id');
    }
      public function calendre()
    {
         return $this->belongsTo(Calendar:: Class,'piece_id');
    }
    protected $fillable = [
        'id',
        'piece_id',
        'end',
        'start',
        'cleaning_price',
        'laundry',
        'name_product',
        'title',
        'color',
    ];
       public static function boot() {
        parent::boot();

        static::deleting(function($clean) {
            $clean->calendre()->delete();
            
        });
    }
}
