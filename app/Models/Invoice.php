<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invoice extends Model
{
    use HasFactory;
     protected $connection="mysql2";
     public function invoice_line()
    {
        return $this->hasMany(Invoice_Line:: Class,'invoice_id');
    }
      public static function boot() {
        parent::boot();

        static::deleting(function($invoice) { // before delete() method call this
            $invoice->invoice_line()->delete();
            // do the rest of the cleanup...
        });
    }
    protected $fillable = [
        'invoice_date',
        'total_price'
    ];
}
