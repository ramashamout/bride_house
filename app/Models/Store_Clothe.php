<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Store_Clothe extends Model
{
    use HasFactory;
     protected $connection="mysql2";
    public function piece()
    {
        return $this->belongsTo(Piece:: Class,'piece_id');
    }
     protected $fillable = [
        'name',
        'piece_img',
        'selling_price',
        'rent_price',
        'designer_name',
        'color',
        'size',
        'clothes_type',
        'discount',
        'old_price',
        'type',
        'other_image',

    ];
  public function invoice_line()
  {
      return $this->belongsTo(Invoice_Line:: Class,'id');
  }
}
