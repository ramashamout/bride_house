<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\URL;

class TypeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this["name"],
             'image' => URL::to('/') . '/images/type/' . $this->image,
            'type_piece' => $this["type_piece"],
            
        ];

    }//end of to array

}//end of resource
