<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\URL;
class AppointmentResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
      return [
            'id' => $this->id,
            'title'=> $this->title,
            'color'=> $this->color,
            'mobile_number'=> $this->mobile_number,
            'type'=> $this->type,
            'date'=> $this->date,
            'time'=> $this->time,
            'statue_app'=>$this->statue_app,
        ];
     }
}
