<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\URL;
class AccessoryResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    { if($this->inFavorites==0)
        {
            $fav=false;
        }
        else
            $fav=true;
      return [
            'id' => $this->id,
            'name' => $this->name,
            'selling_price'=> $this->selling_price,
            'old_price'=> $this->old_price,
            'discount'=> $this->discount,
            'piece_img' => URL::to('/') . '/images/accessory/' . $this->piece_img,
            'rent_price'=>$this->rent_price,
              'inFavorites'=>$fav,
            'type'=>$this->type,
            'category'=>$this->category,
            'Type_accessory'=>$this->Type_accessory,
             // 'other_image'=>$this->other_image,

    
        ];
     }
}
