<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use Illuminate\Support\Facades\URL;
class ClotheResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        if($this->inFavorites==0)
        {
            $fav=false;
        }
        else
            $fav=true;
      return [
            'id' => $this->id,
            'name' => $this->name,
            'selling_price'=> $this->selling_price,
            'old_price'=> $this->old_price,
            'discount'=> $this->discount,
            'piece_img' => URL::to('/') . '/images/clothe/' . $this->piece_img,
            'inFavorites'=>$fav,
            'rent_price'=>$this->rent_price,
            'type'=>$this->type,
            'designer_name'=>$this->designer_name,
            'color'=>$this->color,
            'size'=>$this->size,
            'clothes_type'=>$this->clothes_type,
             // 'other_image'=>$this->other_image,

    
        ];
     }
}
