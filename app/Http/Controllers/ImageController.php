<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Image;

class ImageController extends Controller
{
	//
	public function store(Request $request)
	{
		$image = new Image;
		$image->id_product = $request->id_product;
		$image->src = $request->src;
		$image->save();
	}
	public function destroy($id)
	{
		$image = Image::find($id);
		$image->delete();
		return back();
	}
}
