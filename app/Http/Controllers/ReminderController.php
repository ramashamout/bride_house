<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ReminderController extends Controller
{
    public function delete_expired_notification()
    {
        $user = \App\Models\User::find(2);
        $date = today()->format('Y-m-d');
        foreach ($user->notifications as $notification) {
            if ($notification->data['date'] < $date)
                $notification->delete();
        }
    }
}
