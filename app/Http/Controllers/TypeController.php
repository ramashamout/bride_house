<?php

namespace App\Http\Controllers;

use App\Models\Type;
use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class TypeController extends Controller
{

    public function index()
    {
        $Type = Type::all();
        $user = Auth::user();

        return view('Type.add_Type', ['Type' => $Type, 'user' => $user]);
    }

    public function create()
    {
        return view('type.create');
    }

    public function store(Request $request)
    {

        $image = $request->file('piece_img');
        $extension = $image->getClientOriginalExtension();
        $imageName = time() . '.' . $extension;
        $path = public_path() . '/images/type/';
        $image->move($path, $imageName);
        $type = new Type;
        $type->name = $request->get('name');
        $type->type_piece = $request->get('type_piece');
        $type->image = $imageName;


        $type->save();
        return back();
    }

    public function show($id)
    {
        $selected_Item = Type::find($id);
    }

    public function edit($id)
    {
        $type = Type::find($id);
        return view('type.edit', compact('type'));
    }

    public function update(Request $request, $id)
    {
        $request->validate([
            'name' => 'required'
        ]);
        $image = $request->file('image');
        if ($image != null) {
            File::delete(public_path($type->image));
            $extension = $image->getClientOriginalExtension();
            $imageName = time() . '.' . $extension;
            $path = public_path() . '/images/type/';
            $image->move($path, $imageName);
        }
        $type = Type::find($id);
        $type->name =  $request->get('name');
        $type->image =  $imageName;

        $type->type_piece =  $request->get('type_piece');

        $type->save();

        return redirect('/type')->with('success', 'type updated.');
    }

    public function destroy($id)
    {
        $type = Type::find($id);
        $type->delete();

        return back();
    }
}
