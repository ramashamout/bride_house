<?php

namespace App\Http\Controllers;

use App\Models\Accessory;
use App\Models\Clothe;
use App\Models\Contract;
use App\Models\Customer;
use App\Models\Piece;
use App\Models\Piece_Contract;
use App\Models\Type;
use App\Models\User;
use App\Models\Calendar;
use Auth;
use App\Notifications\ReminderNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ContractController extends Controller
{

       function __construct()
    {


      $this->middleware('permission:إدارة التقارير ', ['only' => ['paymentReport']]);
       $this->middleware('permission:العقود', ['only' => ['create','store','edit','update','destroy']]);
    }


   public function create()
    {
        $Type=Type::all();
        $customer=customer::all();
        $accessory=Accessory::all();
        $clothe=Clothe::all();
        $contract=Contract::all();
        $piece_contract=Piece_Contract::all();
         $user=Auth::User();
         if(Auth::user()){

        return view('contract.contract',['Type'=>$Type , 'customer'=>$customer ,'accessory'=>$accessory ,'clothe'=>$clothe ,'contract'=>$contract,'user'=>$user,'piece_contract'=>$piece_contract]);}
        else{
             return redirect('/logi');
        }

    }

    public function store(Request $request)
    {


        if($request->end<$request->start || $request->start < Carbon::now()->toDateString()){
            return redirect()->back()->withErrors(['msg' =>'تاريخ غير صالح']);
    }
        if ($request->event_date > $request->end ) {
            return redirect()->back()->withErrors(['msg' =>'تاريخ الاستلام غير صالح']);
        }

        $c=Clothe::where('name',$request->get('clothe_name'))->first();
 if($c==null){
            return redirect()->back()->withErrors(['msg' => 'هذه القطعة غير موجودة']);
        }
        foreach ($c->clean as $process){
            if($process->start<=$request->start && $request->start <= $process->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($process->start<=$request->end && $request->end <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($request->start<=$process->start && $process->start <= $request->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
        }

        foreach ($c->maintenance_notes as $process){
            if($process->start<=$request->start && $request->start <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($process->start<=$request->end && $request->end <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($request->start<=$process->start && $process->start <= $request->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
        }

        foreach ($c->contracts as $process){
            if($process->contract_type =='بيع'){
                if($process->start<=$request->start || $process->start<=$request->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
            }else{
                if($process->start<=$request->start && $request->start <= $process->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
                elseif ($process->start<=$request->end && $request->end <= $process->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
                elseif ($request->start<=$process->start && $process->start <= $request->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
            }

        }



        if($request->contract_type=="بيع "){
            foreach ($c->clean as $process){
                if($request->start<=$process->start && $request->start <= $process->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
            }
            foreach ($c->maintenance_notes as $process){
                if($request->start<=$process->start && $request->start <= $process->end){
                        return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                    }
            }
            foreach ($c->contracts as $process){
                    if($process->contract_type =='بيع'){
                            return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);

                    }else{
                        if($request->start<=$process->start && $request->start <= $process->end){
                            return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                        }
                    }
                }
        }


        if($request->get('accessory_name')!=null){
        foreach ($request->get('accessory_name') as $accessory_name) {
            $accessory=Accessory::where('name', 'LIKE',"%{$accessory_name}%")->first();
            if($request->contract_type=="بيع "){
                foreach ($accessory->contracts as $process){
                    if($process->contract_type =='بيع'){
                        return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذا الملحق في التاريخ المدخل']);

                    }else{
                        if($request->start<=$process->start && $request->start <= $process->end){
                            return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذا الملحق في التاريخ المدخل']);
                        }
                    }
                }
            }
            foreach ($accessory->piece_contracts as $pc){
                    if($pc->contract->contract_type =='بيع'){
                        if($pc->contract->start<=$request->start || $pc->contract->start<=$request->end){
                            return redirect()->back()->withErrors(['msg' =>'يوجد عملية اخرى لهذا الملحق في التاريخ المدخل']);
                        }
                    }else{
                        if($pc->contract->start<=$request->start && $request->start <= $pc->contract->end){
                            return redirect()->back()->withErrors(['msg' =>'يوجد عملية اخرى لهذا الملحق في التاريخ المدخل']);
                        }
                        elseif ($pc->contract->start<=$request->end && $request->end <= $pc->contract->end){
                            return redirect()->back()->withErrors(['msg' =>'يوجد عملية اخرى لهذا الملحق في التاريخ المدخل']);
                        }
                        elseif ($request->start<=$pc->contract->start && $pc->contract->start <= $request->end){
                            return redirect()->back()->withErrors(['msg' =>'يوجد عملية اخرى لهذا الملحق في التاريخ المدخل']);
                        }
                    }
            }

        }}
        $customer =new CustomerController();
        $user=Auth::user();
//if customer exist then just update
        $custom=Customer::where('full_name', 'LIKE',"%{$request->get('full_name')}%")->first();
        if($custom!=null){
            $customer->update($request,$custom->id);
            $cust=$custom;
        }
        else{
            $cust=$customer->store($request);
        }
        $clothe=Clothe::where('name', 'LIKE',"%{$request->get('clothe_name')}%")->first();
        $contract = new Contract;
        $contract->customer_id= $cust->id;
        $contract->title=$request->title;
        $contract->color=$request->color;
        $contract->piece_id=$request->piece_id;
        $contract->acc_id=0;
        $contract->insurance_price=$request->insurance_price;
        $contract->deposit=$request->deposit;
        $contract->first_batch=$request->first_batch;
        $contract->second_batch=$request->second_batch;
        $contract->hall=$request->hall;
        $contract->start=$request->start;
        $contract->contract_type=$request->contract_type;
            if($contract->contract_type=='آجار' ){
        $contract->end=$request->end;
            }
        $contract->contract_date=$request->contract_date;
        $contract->event_date=$request->event_date;
        $contract->is_check=$request->is_check;
        $contract->is_check1=$request->is_check1;
        $contract->total_price=$request->deposit+$request->first_batch+$request->second_batch+$request->insurance_price;
        if($this->check($request)==true)
        {

            return redirect('/contract');
        }
        else{

            $contract->save();

            if($contract->contract_type=='آجار' ){
                $calendar =new CalenderController();
                $ca=$calendar->store($request);
            }
            else
            {
//                $clothe=Clothe::find($contract->piece_id);
//                $clothe->statue="مباع ";
//                $clothe->save();
//                $accessory=Accessory::find($contract->acc_id);
//                $accessory->statue_acc="مباع ";
//                $accessory->save();

            }


            $piece_contract = new Piece_Contract([
                'contract_id'=>$contract->id,
                'piece_id'=>$clothe->id
            ]);
            $piece_contract->save();



            if($request->get('accessory_name')!=null) {
                foreach ($request->get('accessory_name') as $accessory_name) {
                    $accessory = Accessory::where('name', 'LIKE', "%{$accessory_name}%")->first();
                    $piece_contract = new Piece_Contract([
                        'contract_id' => $contract->id,
                        'piece_id' => $accessory->id
                    ]);
                    $piece_contract->save();
                    $contract->acc_id = $accessory->id;
                    $contract->save();
                }
            }



            return redirect("/");}
    }


   public  function  check(Request $request):bool
   {
       $contract = Contract::all();

        $bool=false;
        foreach ($contract as $contract) {

            if($contract->piece_id==$request->get('piece_id'))
             {

            if(($contract->start <= $request->get('start'))&&($contract->end >= $request->get('start')))
                  {
                      if(($contract->start <= $request->get('end'))&&($contract->end >= $request->get('end')))


                        { $bool=true;}

                    }

             }

        }

           return $bool;

   }
  public function edit($id)
    {   $clothe=Clothe::all();
        $accessory=Accessory::all();
        $contract = Contract::find($id);
        $user=Auth::user();
        $Type=Type::all();
        $current_a='a';
        foreach ($contract->piece_contracts as $p_c){
            if($p_c->clothe!=null){
                $current_c =$p_c->clothe;
            }
            elseif ($p_c->accessory !=null && $current_a =='a'){
                $current_a =$p_c->accessory;
            }
           elseif ($p_c->accessory !=null && $current_a !='a'){
                $current_a->name =$current_a->name . $p_c->accessory->name;
            }
        }


        return view('contract.cont',['contract'=>$contract,'clothe'=>$clothe,'accessory'=>$accessory,'current_c'=>$current_c,'current_a'=>$current_a,'user'=>$user,'Type'=>$Type]); 
    }

    public function update(Request $request, $id)
    {
        $contract = Contract::find($id);

        $current_a = 'a';
        $my_piece_id=Clothe::where('name',$request->clothe_name)->first()->id;
 if($my_piece_id==null){
            return redirect()->back()->withErrors(['msg' => 'هذه القطعة غير موجودة']);
        }
        foreach ($contract->piece_contracts as $p_c) {
            if ($p_c->clothe != null) {
                $current_c = $p_c->clothe;
            } elseif ($p_c->accessory != null && $current_a == 'a') {
                $current_a = $p_c->accessory;
            } elseif ($p_c->accessory != null && $current_a != 'a') {
                $current_a->name = $current_a->name . $p_c->accessory->name;
            }
        }
        if($request->end<$request->start || $request->start < Carbon::now()->toDateString()){
            return redirect()->back()->withErrors(['msg' =>'تاريخ غير صالح']);
        }
        if ($request->event_date > $request->end) {
            return redirect()->back()->withErrors(['msg' =>'تاريخ الاستلام غير صالح']);
        }



        if($my_piece_id!=$contract->piece_id||$request->start!=$contract->start||$request->end!=$contract->end){
        $c = Clothe::find($my_piece_id);
        foreach ($c->clean as $process) {

            if ($process->start <= $request->start && $request->start <= $process->end) {
                return redirect()->back()->withErrors(['msg' =>'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            } elseif ($process->start <= $request->end && $request->end <= $process->end) {

                return redirect()->back()->withErrors(['msg' =>'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            } elseif ($request->start <= $process->start && $process->start <= $request->end) {
                return redirect()->back()->withErrors(['msg' =>'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
        }

        foreach ($c->maintenance_notes as $process) {

            if ($process->start <= $request->start && $request->start <= $process->end) {
                return redirect()->back()->withErrors(['msg' =>'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);

            } elseif ($process->start <= $request->end && $request->end <= $process->end) {
                return redirect()->back()->withErrors(['msg' =>'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);

            } elseif ($request->start <= $process->start && $process->start <= $request->end) {
                return redirect()->back()->withErrors(['msg' =>'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);

            }
        }
            $contract = Contract::find($id);
        foreach ($c->contracts as $process) {
            if($process->id !=$contract->id){
            if ($process->contract_type == 'بيع') {
                if ($process->start <= $request->start || $process->start <= $request->end) {
                    return redirect()->back()->withErrors(['msg' =>'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
            } else {
                if ($process->start <= $request->start && $request->start <= $process->end) {
                    return redirect()->back()->withErrors(['msg' =>'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                } elseif ($process->start <= $request->end && $request->end <= $process->end) {
                    return redirect()->back()->withErrors(['msg' =>'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                } elseif ($request->start <= $process->start && $process->start <= $request->end) {
                    return redirect()->back()->withErrors(['msg' =>'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
            }
        }
        }
    }
        if($request->get('accessory_name')==null){
            foreach ($contract->piece_contracts as $p_cc) {
                if($p_cc->id !=$contract->id){
                    if($p_cc->accessory != null ) {
                        foreach ($p_cc->accessory->piece_contracts as $pc) {
                            if ($pc->contract->id!=$contract->id){
                                if ($pc->contract->contract_type == 'بيع') {
                                    if ($pc->contract->start <= $request->start || $pc->contract->start <= $request->end) {
                                        return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذا الملحق في التاريخ المدخل']);
                                    }
                                } else {
                                    if ($pc->contract->start <= $request->start && $request->start <= $pc->contract->end) {
                                        return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذا الملحق في التاريخ المدخل']);

                                    } elseif ($pc->contract->start <= $request->end && $request->end <= $pc->contract->end) {
                                        return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذا الملحق في التاريخ المدخل']);

                                    } elseif ($request->start <= $pc->contract->start && $pc->contract->start <= $request->end) {
                                        return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذا الملحق في التاريخ المدخل']);

                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        if($request->get('accessory_name')!=null){
            foreach ($request->get('accessory_name') as $p_c) {
                $p_cc=Accessory::where('name', 'LIKE',"%{$p_c}%")->first();
                    if($p_cc->accessory != null ) {
                        foreach ($p_cc->accessory->piece_contracts as $pc) {
                            if ($pc->contract->id!=$contract->id){
                                if ($pc->contract->contract_type == 'بيع') {
                                    if ($pc->contract->start <= $request->start || $pc->contract->start <= $request->end) {
                                        return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذا الملحق في التاريخ المدخل']);
                                    }
                                } else {
                                    if ($pc->contract->start <= $request->start && $request->start <= $pc->contract->end) {
                                        return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذا الملحق في التاريخ المدخل']);

                                    } elseif ($pc->contract->start <= $request->end && $request->end <= $pc->contract->end) {
                                        return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذا الملحق في التاريخ المدخل']);

                                    } elseif ($request->start <= $pc->contract->start && $pc->contract->start <= $request->end) {
                                        return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذا الملحق في التاريخ المدخل']);

                                    }
                                }
                            }
                        }
                    }

            }
        }
        $customer =new CustomerController();
        $user=Auth::User();
        $custom=Customer::where('id', 'LIKE',"%{$request->get('id')}%")->first();

        $customer->update($request,$custom->id);

        $request->validate([
            'insurance_price'=>'required',
            'deposit'=>'required',
            'first_batch'=>'required',
            'second_batch'=>'required',
            'hall'=>'required',
//            'end'=>'required',
            'start'=>'required',
            'contract_date'=>'required',
            'event_date'=>'required',
            'contract_type'=>'required',
        ]);
        $calendar =new CalenderController();
        $cal=calendar::where('id', 'LIKE',"%{$request->get('id')}%")->first();
        if($cal!=null){
            $calendar->update($request,$cal->id);}

        foreach ($user->unreadNotifications as $appRemider ){

            if($appRemider->data['type']=="contract"  ){
                if($appRemider->data['contract']['id']==$id)
                {
                    $appRemider->delete();
                }
            }

        }
        $clothe=Clothe::where('name', 'LIKE',"%{$request->get('clothe_name')}%")->first();
        $contract = Contract::find($id);
        if($contract->start<= Carbon::now()->toDateString() && $request->start > Carbon::now()->toDateString()){
            $clo=Clothe::find($contract->clothe->id);
            $clo->statue="متوفر ";
            $clo->save();
        }
//        $myclothe =Clothe::find($contract->piece_id);
//        $myclothe->statue="متوفر ";
//        $myclothe->save();
        $contract->insurance_price =  $request->get('insurance_price');
        $contract->deposit =  $request->get('deposit');
//        $contract->piece_id=$request->piece_id;
        $contract->first_batch =  $request->get('first_batch');
        $contract->second_batch =  $request->get('second_batch');
        $contract->hall =  $request->get('hall');
        $contract->start =  $request->get('start');
        if($contract->contract_type=='آجار' ){
            $contract->end =  $request->get('end');
        }
        $contract->color="pink";
        $contract->contract_date =  $request->get('contract_date');
        $contract->event_date =  $request->get('event_date');
        $contract->contract_type =  $request->get('contract_type');
        $contract->is_check1=$request->get('is_check1');
        $contract->is_check=$request->get('is_check');
        $contract->acc_id=0;//$request->get('acc_id');
        $contract->total_price=$request->deposit+$request->first_batch+$request->second_batch+$request->insurance_price;
        $contract->save();
        if($contract->contract_type=='آجار'){
            $calendar =new CalenderController();
            $ca=$calendar->store($request);
        }

        foreach ($contract->piece_contracts as $p_c){
            if($p_c->clothe !=null){
                $p_c->piece_id=$clothe->id;
                $p_c->save();
                $contract->piece_id=$clothe->id;
                $contract->save();
            }elseif($request->get('accessory_name')!=null){
                $p_c->delete();
            }elseif ( $contract->start== Carbon::now()->toDateString() && $request->start > Carbon::now()->toDateString() && $p_c->accessory !=null ){
                $acces=Accessory::find($p_c->accessory->id);
                $acces->statue_acc=="متوفر ";
                $acces->save();

            }
        }
        if($request->get('accessory_name')!=null){

            foreach ($request->get('accessory_name') as $accessory_name) {
                $accessory=Accessory::where('name', 'LIKE',"%{$accessory_name}%")->first();
                $piece_contract = new Piece_Contract([
                    'contract_id'=>$contract->id,
                    'piece_id'=>$accessory->id
                ]);
                $piece_contract->save();
                $contract->acc_id=$accessory->id;
                $contract->save();
            }
        }


//        $myclothe =Clothe::find($contract->piece_id);
//        $myclothe->statue="متوفر ";
//        $myclothe->save();
        return redirect("/");
    }


    public function update_color($id)
    {

        $contract = Contract::find($id);
        $contract->color="palegreen";
        $contract->title="متوفر ";
        $contract->is_check=true;
         $contract->is_check1=false;
        $contract->save();
         $calendar =new CalenderController();
        $calendar->update_color($contract->piece_id);

         return back();
    }

    public function destroy($id)
    {
        $user=Auth::User();
        $contract = Contract::find($id);
//        if ($contract->start<Carbon::now()->toDateString() && $contract->end<Carbon::now()->toDateString()) {
//            return redirect()->back()->withErrors(['msg' =>'عملية مؤرشفة لا يمكن التعديل عليها']);
//        }
        if($contract->start<= Carbon::now()->toDateString() ){
            $clo=Clothe::find($contract->clothe->id);
            $clo->statue="متوفر ";
            $clo->save();
        }
        foreach ($contract->piece_contracts as $p_c){
            if($p_c->accessory !=null){
                if ($contract->start<= Carbon::now()->toDateString()){
                    $acces=Accessory::find($p_c->accessory->id);
                    $acces->statue_acc=="متوفر ";
                    $acces->save();
                }
            }

        }

        if($contract->contract_type == "آجار "){
//            $clothe = Clothe::find($contract->piece_id);
//            $clothe->statue="متوفر ";
//            $clothe->save();
//            $accessory=Accessory::find($contract->acc_id);
//            $accessory->statue_acc="متوفر ";
//            $accessory->save();

            $calendar =new CalenderController();
            $cal=calendar::where('piece_id', 'LIKE',$contract->piece_id)->first();
            $cal->delete();}
        foreach ($user->unreadNotifications as $appRemider ){

            if($appRemider->data['type']=="contract"  ){
                if($appRemider->data['contract']['id']==$id)
                {
                    $appRemider->delete();
                }
            }

        }
        $contract->delete();
        return redirect('/');
    }

public function paymentReport()
    {
        $Type=Type::all();
        $sum=0;
        $sum1=0;
        $sum2=0;
        $user=Auth::user();
       $piece_contract=Piece_Contract::with('contract','clothe')->get();
       $contract = Contract::all();

       foreach ($piece_contract as $pc)  {
       if ($pc->Clothe!=null) {


      if($pc->contract['contract_type']=='آجار'){
$sum=$sum+$pc->contract['total_price'];

       }if($pc->contract['contract_type']=='بيع'){
$sum1=$sum1+$pc->contract['total_price'];

       }
       $sum2=$sum2+$pc->contract['total_price'];
        }
   }

if(Auth::user()){
 return view('report.PaymentsReport',compact('piece_contract','contract','sum1','sum'),['Type'=>$Type,'user'=>$user,'sum2'=>$sum2]) ->with('i')->with('j'); }
 else
 {
     return redirect('/logi');
 }
    }

    // public function mark_as_read($id) {
    //     $user=\App\Models\User::find(1);
    //     $user->unreadNotifications->where('id', $id)->markAsRead();
    //     return redirect('/brideNotification');
    // }
    public function mark_as_read($id) {
        $user=\App\Models\User::find(1);
        $g=$user->unreadNotifications->where('id', $id)->first();
        $user->unreadNotifications->where('id', $id)->markAsRead();
       $piece_id= $g->data['contract']['piece_id'];
       $acc_id=$g->data['contract']['acc_id'];
//       $p=Piece::find($piece_id);
//         $clothe = Clothe::find($piece_id);
//          $clothe->statue="متوفر ";
//          $clothe->save();
//        $acc=Accessory::find($acc_id);
//        $acc->statue_acc="متوفر ";
//        $acc->save();

        return back();
    }
    public function show_information($id)
    {
      $contract=Contract::find($id);
      $Type=Type::all();
      $user=Auth::user();
      return view('contract.show',['contract'=>$contract,'Type'=>$Type,'user'=>$user]);
    }

}
