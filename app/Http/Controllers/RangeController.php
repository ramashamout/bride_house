<?php

namespace App\Http\Controllers;

use App\Models\Range;
use Illuminate\Http\Request;

class RangeController extends Controller
{

    public function index()
    {
        $range = Range::all();
        return view('range.index', compact('range')); 
    }

    public function create()
    {
        return view('range.create'); 
    }

    public function store(Request $request)
    {
       
        $request->validate([
            'from' => 'required',
            'to' => 'required',
            'name' => 'required'
        ]);
        $range = new Type([
            'name' => $request->get('name'),
            'from' => $request->get('from'),
            'to' => $request->get('to')
        ]);
        $range->save();
        return redirect('/range')->with('success', 'range saved.');   
    }

    public function show($id)
    {
        $selected_Item = Range::find($id);
    }

    public function edit($id)
    {
        $range = Range::find($id);
        return view('range.edit', compact('range')); 
    }

    public function update(Request $request, $id)
    {
    
        $request->validate([
            'from' => 'required',
            'to' => 'required',
            'name' => 'required'
        ]);
        $range = Range::find($id);
        $range->name =  $request->get('name');
        $range->from =  $request->get('from');
        $range->to =  $request->get('to');
        $range->save();

        return redirect('/range')->with('success', 'range updated.'); 
    }

    public function destroy($id)
    {
        $range = Range::find($id);
        $range->delete();

        return redirect('/range')->with('success', 'range removed.');  
    }
}
