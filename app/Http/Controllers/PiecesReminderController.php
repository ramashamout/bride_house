<?php

namespace App\Http\Controllers;

use App\Models\Brides_Reminder;
use App\Models\Reminder;
use App\Models\Type;
use App\Models\User;
use App\Notifications\ReminderNotification;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Pieces_Reminder;
use Auth;

class PiecesReminderController extends Controller
{
    function __construct()
    {

        $this->middleware('permission:التذكيرات', ['only' => ['destroy', 'store', 'piece_notification']]);
    }
    public function store(Request $request)
    {
        $user = Auth::user();
        $brideReminders = Brides_Reminder::all();
        $pieceReminder = Pieces_Reminder::all();
        $c = Carbon::today()->toDateString();
        $Type = Type::all();

        if ($request->get('date') < Carbon::now()->toDateString()) {
            return view('brideReminder.notification', ['brideReminder' => $brideReminders, 'pieceReminder' => $pieceReminder, 'user' => $user, 'Type' => $Type, 'c' => $c, 'bride_errors' => null, 'piece_errors' => 'تاريخ غير صالح']);
        }
        $request->validate([
            'content' => 'required',
            'date' => 'required',
            'mobile_number' => 'required',
            'customer_name' => 'required'
        ]);

        $pieceReminder = new Pieces_Reminder([
            'content' => $request->get('content'),
            'date' => $request->get('date'),
            'mobile_number' => $request->get('mobile_number'),
            'customer_name' => $request->get('customer_name')

        ]);
        $pieceReminder->save();
        Auth::user()->notify(new ReminderNotification($pieceReminder));

        return back();
    }




    public function destroy($id)
    {
        $pieceReminder = Pieces_Reminder::find($id);
        $pieceReminder->delete();
        foreach (Auth::user()->unreadNotifications as $noti) {
            if ($noti->data['type'] == 'piece') {
                if ($noti->data['id'] == $id) {
                    $noti->markAsRead();
                }
            }
        }
        return back();
    }
    public function piece_notification()
    {
        $user = Auth::User();
        $Type = Type::all();
        return view('master', ['user' => $user, 'Type' => $Type]);
    }
    public function mark_as_read($id)
    {
        $user = Auth::user();
       
        $user->unreadNotifications->where('id', $id)->markAsRead();
        return back();
       
    }
}
