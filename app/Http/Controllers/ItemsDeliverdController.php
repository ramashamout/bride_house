<?php

namespace App\Http\Controllers;

use App\Models\Items_Delivered;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Type;
use Auth;

class ItemsDeliverdController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:وصولة القبض ', ['only' => ['index', 'show', 'store', 'edit', 'update']]);
    }
    public function index($contract_id)
    {
        $itemsDeliverd = Items_Delivered::all();
        $Type = Type::all();
        $user = Auth::user();
        return view('itemsDeliverd.item', ['itemsDeliverd' => $itemsDeliverd, 'contract_id' => $contract_id, 'Type' => $Type, 'user' => $user]);
    }



    public function store(Request $request)
    {


        if ($isChecked = $request->earring != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd = new Items_Delivered([
            'contract_id' => $request->get('contract_id'),
            'is_delivered' => $is_delivered,
            'name' => 'حلق'
        ]);
        $itemsDeliverd->save();

        if ($isChecked = $request->suit != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd = new Items_Delivered([
            'contract_id' => $request->get('contract_id'),
            'is_delivered' => $is_delivered,
            'name' => 'بدلة '
        ]);
        $itemsDeliverd->save();
        if ($isChecked = $request->jobon != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd = new Items_Delivered([
            'contract_id' => $request->get('contract_id'),
            'is_delivered' => $is_delivered,
            'name' => 'جبونة '
        ]);
        $itemsDeliverd->save();

        if ($isChecked = $request->tall_veil != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd = new Items_Delivered([
            'contract_id' => $request->get('contract_id'),
            'is_delivered' => $is_delivered,
            'name' => 'طرحة طويلة '
        ]);
        $itemsDeliverd->save();
        if ($isChecked = $request->short_veil != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd = new Items_Delivered([
            'contract_id' => $request->get('contract_id'),
            'is_delivered' => $is_delivered,
            'name' => 'طرحة طويلة '
        ]);
        $itemsDeliverd->save();
        if ($isChecked = $request->bride != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd = new Items_Delivered([
            'contract_id' => $request->get('contract_id'),
            'is_delivered' => $is_delivered,
            'name' => 'bride'
        ]);
        $itemsDeliverd->save();

        if ($isChecked = $request->crown != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd = new Items_Delivered([
            'contract_id' => $request->get('contract_id'),
            'is_delivered' => $is_delivered,
            'name' => 'تاج'
        ]);
        $itemsDeliverd->save();

        if ($isChecked = $request->belt != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd = new Items_Delivered([
            'contract_id' => $request->get('contract_id'),
            'is_delivered' => $is_delivered,
            'name' => 'حزام '
        ]);
        $itemsDeliverd->save();

        if ($isChecked = $request->bracelet != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd = new Items_Delivered([
            'contract_id' => $request->get('contract_id'),
            'is_delivered' => $is_delivered,
            'name' => 'أسوارة '
        ]);
        $itemsDeliverd->save();

        if ($isChecked = $request->ring != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd = new Items_Delivered([
            'contract_id' => $request->get('contract_id'),
            'is_delivered' => $is_delivered,
            'name' => 'خاتم'
        ]);
        $itemsDeliverd->save();

        if ($isChecked = $request->cap != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd = new Items_Delivered([
            'contract_id' => $request->get('contract_id'),
            'is_delivered' => $is_delivered,
            'name' => 'كاب'
        ]);
        $itemsDeliverd->save();

        if ($isChecked = $request->roses != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd = new Items_Delivered([
            'contract_id' => $request->get('contract_id'),
            'is_delivered' => $is_delivered,
            'name' => 'باقة ورد'
        ]);
        $itemsDeliverd->save();

        return back();
    }

    public function show($id)
    {
        $selected_Item = Items_Delivered::find($id);
    }

    public function edit($id)
    {
        $itemsDeliverd = Items_Delivered::find($id);
        return view('itemsDeliverd.item', compact('itemsDeliverd')); 
    }

    public function update(Request $request, $id)
    {


        if ($isChecked = $request->earring != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd =  Items_Delivered::find($id);
        $itemsDeliverd->contract_id = $request->get('contract_id');
        $itemsDeliverd->is_delivered = $is_delivered;
        $itemsDeliverd->name = 'حلق ';

        $itemsDeliverd->save();

        if ($isChecked = $request->suit != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd =  Items_Delivered::find($id);
        $itemsDeliverd->contract_id = $request->get('contract_id');
        $itemsDeliverd->is_delivered = $is_delivered;
        $itemsDeliverd->name = 'بدلة ';

        $itemsDeliverd->save();
        if ($isChecked = $request->jobon != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd =  Items_Delivered::find($id);
        $itemsDeliverd->contract_id = $request->get('contract_id');
        $itemsDeliverd->is_delivered = $is_delivered;
        $itemsDeliverd->name = 'جبونة ';

        $itemsDeliverd->save();

        if ($isChecked = $request->tall_veil != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd =  Items_Delivered::find($id);
        $itemsDeliverd->contract_id = $request->get('contract_id');
        $itemsDeliverd->is_delivered = $is_delivered;
        $itemsDeliverd->name = 'طرحة طويلة ';

        $itemsDeliverd->save();
        if ($isChecked = $request->short_veil != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd =  Items_Delivered::find($id);
        $itemsDeliverd->contract_id = $request->get('contract_id');
        $itemsDeliverd->is_delivered = $is_delivered;
        $itemsDeliverd->name = 'طرحة قصيرة ';

        $itemsDeliverd->save();
        if ($isChecked = $request->bride != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd =  Items_Delivered::find($id);
        $itemsDeliverd->contract_id = $request->get('contract_id');
        $itemsDeliverd->is_delivered = $is_delivered;
        $itemsDeliverd->name = 'فستان Bride';

        $itemsDeliverd->save();

        if ($isChecked = $request->crown != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd =  Items_Delivered::find($id);
        $itemsDeliverd->contract_id = $request->get('contract_id');
        $itemsDeliverd->is_delivered = $is_delivered;
        $itemsDeliverd->name = 'تاج';

        $itemsDeliverd->save();

        if ($isChecked = $request->belt != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd =  Items_Delivered::find($id);
        $itemsDeliverd->contract_id = $request->get('contract_id');
        $itemsDeliverd->is_delivered = $is_delivered;
        $itemsDeliverd->name = 'حزام';
        $itemsDeliverd->save();

        if ($isChecked = $request->bracelet != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd =  Items_Delivered::find($id);
        $itemsDeliverd->contract_id = $request->get('contract_id');
        $itemsDeliverd->is_delivered = $is_delivered;
        $itemsDeliverd->name = 'أسوارة ';

        $itemsDeliverd->save();

        if ($isChecked = $request->ring != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd =  Items_Delivered::find($id);
        $itemsDeliverd->contract_id = $request->get('contract_id');
        $itemsDeliverd->is_delivered = $is_delivered;
        $itemsDeliverd->name = 'خاتم';

        $itemsDeliverd->save();

        if ($isChecked = $request->cap != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd =  Items_Delivered::find($id);
        $itemsDeliverd->contract_id = $request->get('contract_id');
        $itemsDeliverd->is_delivered = $is_delivered;
        $itemsDeliverd->name = 'كاب';

        $itemsDeliverd->save();

        if ($isChecked = $request->roses != null) {
            $is_delivered = true;
        } else {
            $is_delivered = false;
        }
        $itemsDeliverd =  Items_Delivered::find($id);
        $itemsDeliverd->contract_id = $request->get('contract_id');
        $itemsDeliverd->is_delivered = $is_delivered;
        $itemsDeliverd->name = 'باقة ورد ';

        $itemsDeliverd->save();

        return back();
    }



    public function create_itemsDeliverd_with_id($contract_id)
    {
        $itemsDeliverd = Items_Delivered::all();
        $Type = Type::all();
        $user = Auth::user();
        return view('itemsDeliverd.item', ['itemsDeliverd' => $itemsDeliverd, 'contract_id' => $contract_id, 'Type' => $Type, 'user' => $user]);
    }
    public function display_itemsDeliverd($contract_id)
    {
        $itemsDeliverd = Items_Delivered::all();
    
        return view('itemsDeliverd.index', compact('itemsDeliverd'), compact('contract_id'));
    }
}
