<?php

namespace App\Http\Controllers;

use App\Models\Invoice_Line;
use App\Models\Piece;
use Illuminate\Http\Request;
use App\Models\Type;
use App\Models\User;
use App\Models\Invoice;
use App\Models\Clothe;
use App\Models\size_general;
use App\Models\Accessory;
use App\Models\Store_Clothe;
use App\Models\Image;
use Auth;

class InvoiceLineController extends Controller
{

    public function index($id)
    {
        $Type = Type::all();
        $invoiceLine = Invoice_Line::all();
        $invoice = Invoice::find($id);
        $user = Auth::user();
        $Type1 = Type::all();
        $Type = Type::all();
        $size_general = size_general::all();
        return view('invoiceLine.invoice', ['Type1' => $Type1, 'invoiceLine' => $invoiceLine, 'invoice' => $invoice, 'user' => $user, 'size_general' => $size_general, 'Type' => $Type]);
    }

    public function store(Request $request)
    {


        $image = $request->file('piece_img');
        $extension = $image->getClientOriginalExtension();
        $imageName = time() . '.' . $extension;


        $type_general = $request->type_general;
        if ($type_general == 'clothe') {
            $path = public_path() . '/images/clothe/';
            $image->move($path, $imageName);
        } else {
            $path = public_path() . '/images/accessory/';
            $image->move($path, $imageName);
        }

        $request->old_price = $request->selling_price;






        if ($type_general == 'clothe') {


            $clothe = new Clothe();
            //        $clothe->id=$request->id;
            $clothe->name = $request->name;
            $clothe->piece_img = $imageName;
            $request->old_price = $request->selling_price;
            $clothe->rent_price = $request->rent_price;
            $clothe->designer_name = $request->designer_name;
            $clothe->color = $request->color;
            $clothe->discount = $request->discount;
            $clothe->statue = "مستودع";

            if ($request->discount == 0) {

                $clothe->selling_price = $request->selling_price;
            } else {
                $clothe->selling_price = $request->selling_price - ($request->selling_price * ($request->discount / 100));
            }
            $request->old_price = $request->selling_price;
            $clothe->old_price = $request->old_price;
            $clothe->clothes_type = $request->type_clo;
            $clothe->size = $request->size;
            $clothe->save();
            if ($files = $request->file('other_image')) {
                foreach ($files as $image) {
                    $extension = $image->getClientOriginalExtension();
                    $imageName = time(). '.' . $extension;
                    $path = public_path() . '/images/clothe/';
                    $image->move($path, $imageName);
                    $img = new Image;
                    $img->id_product = $clothe->id;
                    $img->src = $imageName;
                    $img->save();
                }
            }

            $invoiceLine = new Invoice_Line();
       
            $invoiceLine->invoice_id = $request->invoice_id;
            $invoiceLine->piece_id = $clothe->id;
            $invoiceLine->description = $request->description;
            
            $invoiceLine->number_of_pieces = $request->number_of_pieces;
            $invoiceLine->unit_price = $request->unit_price;
            $invoiceLine->total_price = $request->unit_price * $request->number_of_pieces;
            $invoiceLine->save();

            $Store_Clothe = new Store_Clothe;
            $Store_Clothe->piece_id = $clothe->id;
            $Store_Clothe->number_of_pieces = $request->number_of_pieces;
            $Store_Clothe->save();
        }
        if ($type_general == 'accessory') {

            $accessory = new Accessory;
            $accessory->name = $request->name;
            $accessory->piece_img = $imageName;
            $accessory->selling_price = $request->selling_price;
            $accessory->rent_price = $request->rent_price;
            $accessory->category = $request->category;
            $accessory->old_price = $request->selling_price;
           
            $accessory->statue_acc = "مستودع";
            $accessory->Type_accessory = $request->type_acc;
            $accessory->save();
            $invoiceLine = new Invoice_Line;
            $invoiceLine->invoice_id = $request->invoice_id;
            $invoiceLine->piece_id = $accessory->id;
            $invoiceLine->description = $request->description;
            $invoiceLine->number_of_pieces = $request->number_of_pieces;
            $invoiceLine->unit_price = $request->unit_price;
            $invoiceLine->total_price = $request->unit_price * $request->number_of_pieces;
            $invoiceLine->save();

            $Store_Clothe = new Store_Clothe;
            $Store_Clothe->piece_id = $accessory->id;
            $Store_Clothe->number_of_pieces = $request->number_of_pieces;
            $Store_Clothe->save();
       

        }




        return redirect('/create_invoiceLine_with_id/' . $request->invoice_id);
        
    }


    public function edit($id)
    {
        $invoiceLine = Invoice_Line::find($id);
        return view('invoiceLine.invoice', compact('invoiceLine'));
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'description' => 'required',
            'number_of_pieces' => 'required',
            'unit_price' => 'required',
        ]);
        $invoiceLine = Invoice_Line::find($id);
        $invoiceLine->description =  $request->get('description');
        $invoiceLine->number_of_pieces =  $request->get('number_of_pieces');
        $invoiceLine->unit_price =  $request->get('unit_price');
        $invoiceLine->total_price = $request->get('unit_price') * $request->get('number_of_pieces');
        $invoiceLine->save();
        $p = $invoiceLine->piece;
        $store = $p->store_piece;
        $store->number_of_pieces = $invoiceLine->number_of_pieces;
        $store->save();
        $invoice_id = $invoiceLine->invoice_id;
        return redirect(route('invoice_total', $invoice_id));
    }

    public function destroy($id)
    {
        $invoiceLine = Invoice_Line::find($id);
        $invoice_id = $invoiceLine->invoice_id;
        $p = $invoiceLine->piece;
        $store = $p->store_piece;
        $store->delete();
        $invoiceLine->delete();
        $in = Invoice::find($invoice_id);
        if ($in->invoice_line->count() == 0) {
            $in->delete();
            return redirect('/biginvoice');
        }
        return redirect(route('invoice_total', $invoice_id));
     
    }



    public function create_invoiceLine_with_id($invoice_id)
    {
        $Type1 = Type::all();
        $user = Auth::user();
        $Type = Type::all();
        $size_general = size_general::all();
        return view('invoiceLine.add_invoice_line', ['Type1' => $Type1, 'Type' => $Type, 'invoice_id' => $invoice_id, 'user' => $user, 'size_general' => $size_general]);
    }
}
