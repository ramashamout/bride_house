<?php

namespace App\Http\Controllers;

use App\Models\Appointment;
use Illuminate\Http\Request;
use App\Models\Type;
use App\Models\User;
use App\Notifications\ReminderNotification;
use Auth;
use Carbon\Carbon;
class AppointmentController extends Controller
{

  function __construct()
  {
    $this->middleware('permission:إدارة المواعيد ', ['only' => ['index', 'store', 'show', 'edit', 'all', 'update', 'destroy']]);
  }
  public function index(Request $request)
  {
    $events = array();
    $app = Appointment::all();

    foreach ($app as $app) {

      $events[] = [
        'title' => $app->title,
        'start' => $app->start,
        'end' => $app->end,
        'color' => $app->color,
      ];
    }
    $user = Auth::user();
    $Type = Type::all();
    if (Auth::user()) {
      return view('appointment.appointment', ['Type' => $Type, 'events' => $events, 'user' => $user]);
    } else return redirect('/logi');
  }


  public function store(Request $request)
  {

    $app = new appointment;
    $app->title = $request->title;
    $app->mobile_number = $request->mobile_number;
    $app->type = $request->type;
    $app->start = $request->start;
    $timestamps = (strtotime($request->start));
    $date = date('Y.n.j', $timestamps);
    $time = date('H:i:s', $timestamps);
    $tt = Carbon::parse($time)
            ->addMinutes(30)
            ->format('H:i:s');
    $app->end=date('Y-m-d H:i:s', strtotime("$date $tt"));
    $app->date = $date;
    $app->time = $time;
    $app->color = 'green';
    $app->statue_app = "مثبت";
    $app->save();
    
    return redirect('/ap');
  }


  public function show($id)
  {
    $selected_Item = Appointment::find($id);
  }

  public function edit($id)
  {
    $app = Appointment::find($id);

    return view('appointment.all_appointment', compact('app')); 
  }
  public function all()
  {
    $Type = Type::all();
    $app = appointment::all();
    $user = Auth::user();
    return view('appointment.all_appointment', ['Type' => $Type, 'app' => $app, 'user' => $user]);
  }
  public function update(Request $request, $id)
  {
    $request->validate([
      'title' => 'required',
      'color' => 'required',
      'mobile_number' => 'required',
      'type' => 'required',
      'start' => 'required',
      
    ]);
    $appointment = Appointment::find($id);
    
   
    $appointment->title = $request->get('title');
    $appointment->mobile_number = $request->get('mobile_number');
    $appointment->type = $request->get('type');
    $appointment->start = $request->get('start');
    $timestamps = (strtotime($request->get('start')));
    $date = date('Y.n.j', $timestamps);
    $time = date('H:i:s', $timestamps);
     $tt = Carbon::parse($time)
            ->addMinutes(30)
            ->format('H:i:s');
      $appointment ->end=date('Y-m-d H:i:s', strtotime("$date $tt"));
    $appointment->date = $date;
    $appointment->time = $time;
    $app->statue_app = "مثبت";
    $appointment->color = 'pink';

    $appointment->save();

    return redirect('/ap');
  }


  public function destroy($id)
  {
    $appointment = Appointment::find($id);
    $appointment->delete(); 
    $result = ['type' => 'appointment', 'appointment' => $appointment];

    Auth::user()->notify(new ReminderNotification($result));
    return redirect('/appointment');
  }
  public function mark_as_read($id)
  {
    $user = Auth::User();
    $user->unreadNotifications->where('id', $id)->markAsRead();
    return back();
  }

  public function save_s($id)
  {
    $Appointment = Appointment::find($id);
    $user = Auth::User();
    $Type = Type::all();

    return view('appointment.save_s', ['Appointment' => $Appointment, 'Type' => $Type, 'user' => $user]);
  }
  public function update_statue_Installations($id)
  {
    $app = Appointment::find($id);

    $app->statue_app = "مثبت ";
    $app->color = "green";


    $app->save();
    $user = Auth::User();
    foreach ($user->unreadNotifications as $appRemider) {
      if ($appRemider->data['type'] == "app") {
        if ($appRemider->data['app']['id'] == $id) {
          $appRemider->markAsRead();
        }
      }
    }

    return back();
  }
  public function update_statue_delete($id)
  {
    $app = Appointment::find($id);

    $app->statue_app = "ملغى ";
    $app->save();
    $user = Auth::User();
    foreach ($user->unreadNotifications as $appRemider) {
      if ($appRemider->data['type'] == "app") {
        if ($appRemider->data['app']['id'] == $id) {
          $appRemider->markAsRead();
        }
      }
    }
    return redirect('/dashboard');
  }
}
