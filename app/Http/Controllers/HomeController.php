<?php

namespace App\Http\Controllers;
use App\Models\Accessory;
use App\Models\Clothe;
use App\Models\Contract;
use App\Models\Customer;
use App\Models\Cleaning;
use App\Models\Piece;
use App\Models\Piece_Contract;
use App\Models\Maintenance_Note;
use App\Models\Reminder;
use App\Models\Type;
use App\Models\User;
use App\Models\action;
use App\Notifications\ReminderNotification;
use App\Models\Calendar;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Resources\ClotheResource;

use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Notification;

class HomeController extends Controller
{

     public function direct()
    {
  	return view('auth.login');
    }

    public function send_piece($id)
    {
        $clothe = Clothe::find($id);
           foreach ($clothe->image as $img){
         }
        $clothe->statue="مزاد";
        $clothe->save();
        return $clothe;
    }
    public function send_description($id)
    {
        $clothe = Clothe::find($id);
           foreach ($clothe->image as $img){
         }
        return $clothe;
    }
    public function send_info($id,$price)
    {
        $action=new action;
        $action->user_id=$id;
        $action->price=$price;
        $action->action_date=Carbon::now()->addDays(3)->toDateString();
        $action->save();
        $clothe=Clothe::all()->where('statue','مزاد')->firstOrFail();
        $clothe->statue="في انتظار الزبون";
        $clothe->save();
        $result = ['type' => 'action', 'action' => $action,'clothe'=>$clothe];
        User::all()->where('type','admin')->firstOrFail()->notify(new ReminderNotification($result));

        return;
    }
    public function no_winner(){
        $clothe=Clothe::all()->where('statue','مزاد')->firstOrFail();
        $clothe->statue="متوفر";
        $clothe->save();
    }

    public function gotomazad($id)
    {
        $clothe = Clothe::find($id);

        foreach ($clothe->clean as $process){

            if ($process->start>=Carbon::now()->toDateString()){
                return redirect()->back()->withErrors(['msg' => ' يوجد عملية اخرى لهذه القطعة فلا يمكن ارسالها للمزايدة']);
            }
        }

        foreach ($clothe->maintenance_notes as $process){
            if ($process->start>=Carbon::now()->toDateString()){
                return redirect()->back()->withErrors(['msg' => ' يوجد عملية اخرى لهذه القطعة فلا يمكن ارسالها للمزايدة']);
            }
        }

        foreach ($clothe->contracts as $process){
                if($process->start>=Carbon::now()->toDateString()){
                    return redirect()->back()->withErrors(['msg' => ' يوجد عملية اخرى لهذه القطعة فلا يمكن ارسالها للمزايدة']);
                }
        }

        return redirect('http://localhost:8080/add/'.$id);


    }

    public function home()
    {

        $user = Auth::User();
        $Type = Type::all();
    //     $clean = Cleaning::all();
    //     $contract = Contract::all();
    //     $users=User::all()->whereIn('type',['admin','employ']);
    //     if ($user != null) {

    //     foreach ($user->unreadNotifications as $noti) {
    //         if ($noti->data['type'] == "bride" || $noti->data['type'] == "piece") {
    //             if ($noti->data['date'] < Carbon::today()->toDateString()) {
    //                 $noti->markAsRead();

    //             }
    //         }

    //     }
    //     $r = Reminder::all();
    //     foreach ($r as $r) {
    //         if ($r->date < Carbon::today()->toDateString()) {
    //             $r->delete();
    //         }
    //     }
    //     $bool = 1;


    //     if ($bool == 1) {

    //         $maintenance = Maintenance_Note::all();

    //         foreach ($clean as $clean) {
    //             if ($clean->start == Carbon::today()->toDateString()) {
    //                 $clothe = Clothe::find($clean->piece_id);
    //                 $clothe->statue = "تنظيف  ";
    //                 $clothe->save();
    //             }
    //             if ($clean->end == Carbon::today()->toDateString()) {
    //                 $cc=1;
    //                 foreach ($user->unreadNotifications as $noti){
    //                     if ($noti->data['type'] == "clean" && $noti->data['clean']['id'] == $clean->id ) {
    //                         $cc = 0;
    //                     }
    //                 }
    //                 if($cc==1){
    //                     $result = ['type' => 'clean', 'clean' => $clean];
    //                     Notification::send($users, new ReminderNotification($result));

    //                 }

    //             }

    //         }
    //         foreach ($maintenance as $maintenance) {

    //             if ($maintenance->start == Carbon::today()->toDateString()) {
    //                 $clothe = Clothe::find($maintenance->piece_id);
    //                 $clothe->statue = "صيانة ";
    //                 $clothe->save();
    //             }
    //             if ($maintenance->end == Carbon::today()->toDateString()) {
    //                 $mm=1;
    //                 foreach ($user->unreadNotifications as $noti){
    //                     if ($noti->data['type'] == "maintenance" && $noti->data['maintenance']['id'] == $maintenance->id ) {
    //                         $mm = 0;
    //                     }
    //                 }
    //                 if($mm==1){
    //                     $result = ['type' => 'maintenance', 'maintenance' => $maintenance];
    //                     Notification::send($users, new ReminderNotification($result));
    //                 }

    //             }


    //         }


    //         foreach ($contract as $contract) {


    //             if ($contract->start == Carbon::today()->toDateString()) {
    //                 $clothe = Clothe::find($contract->piece_id);
    //                 if ($clothe->statue !='متوفر ') {
    //                     $co=1;
    //                     foreach ($user->unreadNotifications as $noti){
    //                         if ($noti->data['type'] == "warning" && $noti->data['con_id'] == $contract->id ) {
    //                             $co = 0;
    //                         }
    //                     }
    //                     if($co==1){
    //                         $con='يوجد عقد للقطعة '.$contract->clothe->name.' و لم تتوفر بالمتجر بعد';
    //                         $result = ['type' => 'warning', 'content' =>$con ,'con_id'=>$contract->id ];
    //                         Notification::send($users, new ReminderNotification($result));

    //                     }

    //                 }
    //                 else{
    //                 if ($contract->contract_type == "بيع") {
    //                     $clothe = Clothe::find($contract->piece_id);
    //                     $clothe->statue = "مباع ";
    //                     $clothe->save();

    //                     foreach ($contract->piece_contracts as $p_con){

    //                         if($p_con->accessory != null){
    //                             $ac=Accessory::find($p_con->piece_id);
    //                             $ac->statue_acc=  "مباع ";
    //                             $ac->save();

    //                         }
    //                     }
    //                 } else {
    //                     $clothe = Clothe::find($contract->piece_id);
    //                     $clothe->statue = "مؤجر ";
    //                     $clothe->save();

    //                     foreach ($contract->piece_contracts as $p_con){
    //                         if($p_con->accessory != null){
    //                             $ac=Accessory::find($p_con->piece_id);
    //                             $ac->statue_acc= "مؤجر ";
    //                             $ac->save();
    //                         }
    //                     }

    //                 }
    //             }
    //             }
    //             if ($contract->end != null && $contract->end == Carbon::today()->toDateString()) {
    //                 $co=1;
    //                 foreach ($user->unreadNotifications as $noti){
    //                     if ($noti->data['type'] == "contract" && $noti->data['contract']['id'] == $contract->id ) {
    //                         $co = 0;
    //                     }
    //                 }
    //                 if($co==1){
    //                     $result = ['type' => 'contract', 'contract' => $contract];
    //                     Notification::send($users, new ReminderNotification($result));

    //                 }

    //             }



    //         }
    //     }

    //      }
    // $action=action::all()->last();
    // $clothes=Clothe::All();
    // if($action){

    //     if(Carbon::now()->toDateString()==$action->action_date)
    //     {
    //         foreach ($clothes as $clothe) {
    //             if($clothe->statue=="في انتظار الزبون")
    //             {
    //                 $clothe->statue="متوفر";
    //                 $clothe->save();
    //                 foreach ($user->unreadNotifications as $noti){
    //                     if ($noti->data['type'] == "action" ) {
    //                         $noti->markAsRead();
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }





        return view('home',['Type'=>$Type]);

    }



}
