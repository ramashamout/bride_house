<?php

namespace App\Http\Controllers;

use App\Models\Maintenance_Note;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Type;
use App\Models\Clothe;
use App\Models\Calendar;
use App\Models\Customer;
use App\Models\User;
use App\Notifications\ReminderNotification;
use Auth;
class MaintenanceNoteController extends Controller
{
       function __construct()
    {

       $this->middleware('permission:الصيانة', ['only' => ['display','store','edit','update','destroy']]);
    }
    public function display()
    {
       $Type=Type::all();
       $maintenance=Maintenance_Note::all();
       $clothe=Clothe::all();
       $customer=Customer::all();
          $user=Auth::user();
          if(Auth::user()){
       return view('maintenance.maintenance',['Type'=>$Type,'maintenance'=>$maintenance,'clothe'=>$clothe,'customer'=>$customer,'user'=>$user]);}
       else return redirect('/logi');
    }

    public function store(Request $request)
    {
        if($request->end<$request->start || $request->start < Carbon::now()->toDateString()){
            return redirect()->back()->withErrors(['msg' => 'تاريخ غير صالح']);
        }



        $c=Clothe::where('name', $request->name_product)->first();
        if($c==null){
            return redirect()->back()->withErrors(['msg' => 'هذه القطعة غير موجودة']);
        }
        foreach ($c->clean as $process){
            if($process->start<=$request->start && $request->start <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($process->start<=$request->end && $request->end <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($request->start<=$process->start && $process->start <= $request->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);            }
        }

        foreach ($c->maintenance_notes as $process){
            if($process->start<=$request->start && $request->start <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($process->start<=$request->end && $request->end <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($request->start<=$process->start && $process->start <= $request->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
        }

        foreach ($c->contracts as $process){
            if($process->contract_type =='بيع'){
                if($process->start<=$request->start || $process->start<=$request->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
            }else{
                if($process->start<=$request->start && $request->start <= $process->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
                elseif ($process->start<=$request->end && $request->end <= $process->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
                elseif ($request->start<=$process->start && $process->start <= $request->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
            }

        }
        $request->validate([
            'piece_id'=>'required',
            'customer_id'=>'required',
            'name_product'=>'required',
            'name_customer'=>'required',
            'note_content'=>'required',
            'end'=>'required',
            'start'=>'required',
            'is_check'=>'required',
            'is_check1'=>'required',
        ]);
        $calendar =new CalenderController();


        $ca=$calendar->store($request);
        $maintenance = new Maintenance_Note([
            'piece_id'=>$request->get('piece_id'),
            'customer_id'=>$request->get('customer_id'),
            'name_product' =>$request->get('name_product'),
            'name_customer' =>$request->get('name_customer'),
            'note_content'=>$request->get('note_content'),
            'end' =>$request->get('end'),
            'start'=> $request->get('start'),
            'is_check'=>$request->get('is_check'),
            'is_check1'=>$request->get('is_check1'),
        ]);
        $maintenance->save();

        return redirect('/');



    }




    public function edit($id)
    {
        $maintenance = Maintenance_Note::find($id);
        return view('maintenance.maintenance', compact('maintenance'));
    }

    public function update(Request $request, $id)
    {

        if($request->end<$request->start || $request->start < Carbon::now()->toDateString()){
             return redirect()->back()->withErrors(['msg' => 'تاريخ غير صالح']);
        }

        $c=Clothe::where('name',$request->name_product)->first();
           if($c==null){
            return redirect()->back()->withErrors(['msg' => 'هذه القطعة غير موجودة']);
        }
        foreach ($c->clean as $process){
            if($process->start<=$request->start && $request->start <= $process->end){
                 return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);

            }
            elseif ($process->start<=$request->end && $request->end <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($request->start<=$process->start && $process->start <= $request->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
        }
        $maintenance = Maintenance_Note::find($id);
        foreach ($c->maintenance_notes as $process){
            if($process->id !=$maintenance->id){
            if($process->start<=$request->start && $request->start <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($process->start<=$request->end && $request->end <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($request->start<=$process->start && $process->start <= $request->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            }
        }

        foreach ($c->contracts as $process){
            if($process->contract_type =='بيع'){
                if($process->start<=$request->start || $process->start<=$request->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
            }else{
                if($process->start<=$request->start && $request->start <= $process->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
                elseif ($process->start<=$request->end && $request->end <= $process->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
                elseif ($request->start<=$process->start && $process->start <= $request->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
            }

        }

        $user=Auth::user();
        $calendar =new CalenderController();
        $cal=calendar::where('id', 'LIKE',"%{$request->get('id')}%")->first();
        if($cal!=null){
            $calendar->update($request,$cal->id);
        }

        foreach ($user->unreadNotifications as $appRemider ){

            if($appRemider->data['type']=="maintenance"  ){

                if($appRemider->data['maintenance']['id']==$id)
                {

                    $appRemider->delete();
                }

            }

        }
        $maintenance = Maintenance_Note::find($id);
        if($maintenance->start<= Carbon::now()->toDateString() && $request->start > Carbon::now()->toDateString()){
            $clo=Clothe::find($maintenance->piece->id);
            $clo->statue="متوفر ";
            $clo->save();
        }

        $maintenance->note_content =  $request->note_content;
        $maintenance->piece_id =  $request->piece_id;
        $maintenance->name_product = $request->name_product;
        $maintenance->note_content=$request->note_content;
        $maintenance->name_customer=$request->name_customer;
        $maintenance->customer_id=$request->customer_id;
        $maintenance->end = $request->end;
        $maintenance->start= $request->start;
        $maintenance->is_check=$request->is_check;
        $maintenance->is_check1=$request->is_check1;
        $maintenance->save();


        return redirect('/');
    }


    public function destroy($id)
    {
        $user=Auth::user();
        $maintenance = Maintenance_Note::find($id);
        if($maintenance->start<= Carbon::now()->toDateString() ){
            $clo=Clothe::find($maintenance->piece->id);
            $clo->statue="متوفر ";
            $clo->save();
        }

        $calendar =new CalenderController();
        $cal=calendar::where('piece_id', 'LIKE',$maintenance->piece_id)->first();
        $cal->delete();
        foreach ($user->unreadNotifications as $appRemider ){

            if($appRemider->data['type']=="maintenance"  ){

                if($appRemider->data['maintenance']['id']==$id)
                {

                    $appRemider->delete();
                }

            }

        }
        $maintenance->delete();

        return back();
    }


    public function mark_as_read($id) {
          $user=Auth::user();
        $g=$user->unreadNotifications->where('id', $id)->first();
        $user->unreadNotifications->where('id', $id)->markAsRead();
       $piece_id= $g->data['maintenance']['piece_id'];

        return back();
    }



}
