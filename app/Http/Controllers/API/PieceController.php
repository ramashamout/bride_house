<?php
namespace App\Http\Controllers\API;
use App\Models\Clothe;
use App\Models\Piece;
use App\Models\Type;
use Illuminate\Support\Facades\DB;
use App\Models\Accessory;
use App\Http\Controllers\Controller;
use App\Http\Resources\ClotheResource;
use App\Http\Resources\AccessoryResource;
use Illuminate\Http\Request;
class PieceController extends Controller
{

public function show_all_piece(Request $request)
{
$Accessory=Accessory::All();
foreach ($Accessory as $Accessory) {
if($Accessory->Type_accessory==$request->name)
{
$accessory=Accessory::where([['Type_accessory', 'LIKE','%'.$request->name.'%'],['statue_acc','=','متوفر']])
->orWhere([['Type_accessory', 'LIKE','%'.$request->name.'%'],['statue_acc','=','مؤجر']])

->get();

$data=AccessoryResource::collection($accessory);
return response()->api(true, '',$data);

}
}
$Clothe=Clothe::All();
foreach ($Clothe as $Clothe) {
	
if($Clothe->clothes_type==$request->name)
{
$Clothe=Clothe::where([['clothes_type', 'LIKE','%'.$request->name.'%'],['statue','=','متوفر']])
->orWhere([['clothes_type', 'LIKE','%'.$request->name.'%'],['statue','=','صيانة']])
->orWhere([['clothes_type', 'LIKE','%'.$request->name.'%'],['statue','=','مؤجر']])
->orWhere([['clothes_type', 'LIKE','%'.$request->name.'%'],['statue','=','تنظيف']])

->get();

$data=ClotheResource::collection($Clothe);
return response()->api(true, '',$data);

}}
return response()->api(false, 'لا يوجد داتا',null);
}
public function getfavorite()
{
$allItems = new \Illuminate\Database\Eloquent\Collection; 
$pieces= DB::Connection('mysql2')->table('pieces')->get();
foreach ($pieces as $piece) {

if ($piece->type=='accessory' && $piece->inFavorites==1) {
$Accessory= Accessory::where([['inFavorites','=','1'],['statue_acc','=','متوفر']])
->orWhere([['inFavorites','=','1'],['statue_acc','=','مؤجر']])

->get();
$collection1= AccessoryResource::collection($Accessory);
if ($collection1!=null) {
 $allItems = $allItems->merge($collection1);
}

}
if ($piece->type=='clothe' && $piece->inFavorites==1) {
$Clothe= Clothe::where([['inFavorites','=','1'],['statue','=','متوفر']])
->orWhere([['inFavorites','=','1'],['statue','=','صيانة']])
->orWhere([['inFavorites','=','1'],['statue','=','مؤجر']])
->orWhere([['inFavorites','=','1'],['statue','=','تنظيف']])

->get();
$collection2=ClotheResource::collection($Clothe);
if ($collection2!=null) {
 $allItems = $allItems->merge($collection2);
}

}
$data ['products']=$allItems;
}

return response()->api(true, '',$data);
}
public function addfavorite(Request $request)
{
$piece= DB::Connection('mysql2')->table('pieces')->where('id', '=', $request->id)->first();
if ($piece->type=='accessory') {
$Accessory  = Accessory::find($request->id);
if($Accessory->inFavorites==1)
{
$Accessory->inFavorites = 0;
$Accessory->save();
$data ['products']= new AccessoryResource($Accessory);
return response()->api(true, 'تم الحذف بنجاح',$data);
}
else{
$Accessory->inFavorites = 1;
$Accessory->save();
$data ['products']=new AccessoryResource($Accessory);
}
}
if ($piece->type=='clothe') {
$Clothe  = Clothe::find($request->id);
if($Clothe->inFavorites==1)
{
$Clothe->inFavorites = 0;
$Clothe->save();
$data ['products']= new ClotheResource($Clothe);
return response()->api(true, 'تم الحذف بنجاح',$data);
}
else{
$Clothe->inFavorites = 1;
$Clothe->save();
$data ['products']=new ClotheResource($Clothe);
}
}
return response()->api(true, 'تمت الاضافة بنجاح ',$data);
}
//  public function deletefavorite()
// {
//    $data ['products']=  Clothe::where('inFavorites','=','1')->get();
//       return response()->api(true, '',$data);
// }
}