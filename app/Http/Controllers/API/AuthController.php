<?php
namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Auth;
use Validator;
use App\Models\User;
use App\Http\Resources\UserResource;
use App\Helpers\Messages;
use NextApps\VerificationCode\VerificationCode; 

use Illuminate\Auth\Events\Registered;
class AuthController extends Controller
{
  
    public function login(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->api( false, $validator->getMessageBag()->first(),null);
        }

        $credentials = $request->only('email', 'password');

        if (Auth::attempt($credentials)) {
 try {
            $user = Auth::user();
            $data['user'] = new UserResource($user);
            $data['token'] = $user->createToken('my-app-token')->plainTextToken;

            return response()->api(true,Messages::getMessage('LOGGED_IN_SUCCESSFULLY'),$data);
  } catch (\Throwable $th) {
                return response()->api(false, Messages::getMessage('UNAUTHORISED'),null);
            }
        } else {

            return response()->api( false, Messages::getMessage('LOGIN_IN_FAILED'),null);

        }//end of else

    }//end of login

    public function register(Request $request)
    {
         $validator = Validator($request->all(), [
            'name' => 'required|string|min:3|max:45',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|min:6|confirmed',
            'mobile' => 'numeric|unique:users,mobile|digits:10',
        ]);

        if ($validator->fails()) {
            return response()->api(false , $validator->errors()->first(),null);
        }

        $request->merge([
            'password' => Hash::make($request->password),
            'type' => 'user',
            'image_path'=>'images/33.png'
        ]);

        $user = User::create($request->all());

        $request->remember_token= $user->createToken('my-app-token')->plainTextToken;
       $isSaved = $user->save();
        $data['user'] = new UserResource($user);
        $data['token'] =$request->remember_token;
VerificationCode::send( $user->email);

        return response()->api(true,Messages::getMessage($isSaved ? 'REGISTERED_SUCCESSFULLY' : 'REGISTRATION_FAILED'),$data);

    }//end of register

    public function user()
    {
        $data['user'] = new UserResource(auth()->user('sanctum'));

        return response()->api(true,Messages::getMessage('GET_PROFILE_SUCCESS'),$data);

    }

     public function Update_profile(Request $request)
    {
          $validator = Validator::make($request->all(), [
            
             'name' => 'string|min:3|max:45',
            'email' => 'email|unique:users,email',
             'password' => 'string|min:3',
            'mobile' => 'numeric|unique:users,mobile|digits:10',
            'profile_photo_path' => '',
        ]);
 if ($validator->fails()) {
            return response()->api(false , $validator->errors()->first(),null);
        }

        $request->merge([
            'password' => Hash::make($request->password),
            'type' => 'user'
        ]);
         $user = Auth::user();
        $user->update($request->all());
        $data['token'] =$request->Authorization;
        $data['user'] = new UserResource($user);
        $isSaved = $user->save();
        return response()->api(true,Messages::getMessage($isSaved ? 'USER_UPDATED_SUCCESS' : 'USER_UPDATED_FAILED'),$data);
 
    }
  // method for user logout and delete token
    public function logout()
    {
        $data['user'] =auth()->user()["id"];
        $data['token']=auth()->user()->currentAccessToken()["token"];

      $isDelete =auth()->user()->tokens()->delete();

        return response()->api(false, Messages::getMessage($isDelete ? 'LOGGED_OUT_SUCCESSFULLY' : 'LOGGED_OUT_FAILED'),$data);

       
    }
}//end of controller
