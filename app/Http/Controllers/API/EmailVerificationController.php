<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Verified;
use Illuminate\Foundation\Auth\EmailVerificationRequest;
use App\Helpers\Messages;
use NextApps\VerificationCode\VerificationCode; 
use Auth;
class EmailVerificationController extends Controller
{

    public function sendVerificationEmail(Request $request)
    {
        if ($request->user()->hasVerifiedEmail()) {
         return response()->api(True, Messages::getMessage('VERIFIED_BOFORE'),null);

        }

        VerificationCode::send( $request->user()->email);
         return response()->api(True, Messages::getMessage('VERIFIED_SENT'),null);

    }



    public function verify(Request $request)
    {
         $user = Auth::user();
        if ($user->hasVerifiedEmail()) {
           return response()->api(True, Messages::getMessage('VERIFIED_BOFORE'),null);

        }
         
         $verify=VerificationCode::verify($request->code, $user->email,false);

        if ($verify) {
            $user->markEmailAsVerified();
           return response()->api(True, Messages::getMessage('VERIFIED_OK'),null);

        }


         return response()->api(false, Messages::getMessage('VERIFIED_FAILED'),null);
    
    }

    // public function verify(EmailVerificationRequest $request)
    // {
    //     if ($request->user()->hasVerifiedEmail()) {
    //        return response()->api(True, Messages::getMessage('VERIFIED_BOFORE'),null);

    //     }

    //     if ($request->user()->markEmailAsVerified()) {
    //         event(new Verified($request->user()));
    //     }
    //      return response()->api(True, Messages::getMessage('VERIFIED_OK'),null);
    // }
}
