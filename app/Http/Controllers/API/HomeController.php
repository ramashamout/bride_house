<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
 use App\Models\banner;
 use App\Http\Resources\BannerResource;
  use App\Http\Resources\ClotheResource;
  use App\Http\Resources\TypeResource;
   use App\Http\Resources\AccessoryResource;
 use App\Models\Type;
  use App\Models\Clothe;
 use App\Models\Accessory;


class HomeController extends Controller
{
        public function getbanner()
    {
         $banner=banner::all();
        $data = BannerResource::collection($banner);
          return response()->api(true, '',$data);
    }
    public function getallpic()
    {$allItems = new \Illuminate\Database\Eloquent\Collection; 

      $Accessory= Accessory::where('statue_acc','=','متوفر')
          ->orWhere('statue_acc','=','مؤجر')
           ->get();
          $collection1= AccessoryResource::collection($Accessory);
          $allItems = $allItems->merge($collection1);

      
          $Clothe=Clothe::where('statue','=','متوفر')
          ->orWhere('statue','=','صيانة')
          ->orWhere('statue','=','مؤجر')
          ->orWhere('statue','=','تنظيف')
          ->get();
          $collection2=ClotheResource::collection($Clothe);
          $allItems = $allItems->merge($collection2);
          $data ['products']=$allItems;
          return response()->api(true, '',$data);
    }
        public function getHomeData()
    {
          $banner=banner::all();
          $data ['banners']=BannerResource::collection($banner);
          $Clothe=Clothe::where([['discount','>','0'],['statue','=','متوفر']])
          ->orWhere([['discount','>','0'],['statue','=','صيانة']])
          ->orWhere([['discount','>','0'],['statue','=','مؤجر']])
          ->orWhere([['discount','>','0'],['statue','=','تنظيف']])
          ->get();
          $data ['products']=ClotheResource::collection($Clothe);

          return response()->api(true, '',$data);
    }
  
  
     public function getCategory()
    {
      $type=Type::all();
        $data['data'] = TypeResource::collection($type);
          return response()->api(true, '',$data);
    }
}
