<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Appointment;
  use App\Http\Resources\AppointmentResource;
use App\Models\time;
use DateTime;
use DatePeriod;
use DateInterval;
use Auth;
use Illuminate\Support\Facades\DB;
use App\Helpers\Messages;
use Carbon\Carbon;
use App\Notifications\ReminderNotification;

class AppointmentController extends Controller
{
     
      public function booked(Request $request)
      {
           
        $id=1;
        $appointment=Appointment::where('date', 'LIKE','%'.date("Y-m-d", strtotime($request->date)).'%')->get();
        $work_time = time::find($id);
        $start = new DateTime($work_time['start_time']);
      $end = new DateTime($work_time['end_time']);
      $interval = DateInterval::createFromDateString('30 minutes');

      $period = new DatePeriod($start, $interval, $end);

      $available_times = [];
      $reseved_times = [];


        foreach ($appointment as $appointments) {
          $reseved_times[] = $appointments->time;
            }
        foreach ($period as $time) {
        if (!in_array($time->format('H:i:s'), $reseved_times)) {
          $available_times[] = $time->format('H:i:s');
        }
         
      }
       return response()->api(true, '',$available_times ); 
      }
  

    public function create(Request $request){
    $date=date("Y-m-d", strtotime($request->date));
    $appointment=Appointment::where('date', 'LIKE','%'.$date.'%')->get();
        
      $reseved_times = [];
        foreach ($appointment as $appointments) {
          $reseved_times[] = $appointments->time;
        }
      
        $user = Auth::user();
      $appointment = new Appointment;
      $appointment->date=$date;
      $appointment->time=$request->time;

      $appointment ->start=  date('Y-m-d H:i:s', strtotime("$date $request->time"));
      $tt = Carbon::parse($appointment->time)
            ->addMinutes(30)
            ->format('H:i:s');
      $appointment ->end=date('Y-m-d H:i:s', strtotime("$date $tt"));
  
      $appointment->title =$user->name;
      $appointment->mobile_number =$user->mobile_number;
      $appointment->type ='بروفا';
      $appointment ->color='yellow';
if (!in_array($request->time, $reseved_times)) {
  $appointment->time =$request->time;
  $isSaved =  $appointment->save();

    
$data=AppointmentResource::make($appointment);
    return response()->api(true, Messages::getMessage($isSaved ? 'CREATE_SUCCESS' : 'CREATE_FAILED'),$data);
}
   
    
}

public function updateAppointment(Request $request){
    $date=date("Y-m-d", strtotime($request->date));
    $appointment=Appointment::where('date', 'LIKE','%'.$date.'%')->get();
        
      $reseved_times = [];
        foreach ($appointmente as $appointments) {
          $reseved_times[] = $appointments->time;
        }
      
    $appointment  = Appointment::find($request->id);
       $appointment->date =  $request->get('date');
        $appointment->customer_name = $request->get('customer_name');
        $appointment->mobile_number = $request->get('mobile_number');
        $appointment->type = $request->get('type');
       if (!in_array($request->time, $reseved_times)) {
  $appointment->time =$request->time;
    $appointment->save();
$data=AppointmentResource::collection($appointment);
    return response()->api(true, Messages::getMessage('UPDATE_SUCCESS'),$data);
}
    else{
    return response()->api(false, Messages::getMessage('UPDATE_FAILED'),null);
      } 
}  


public function getAppointment()
{
    $user=Auth::user();
   $Appointment=Appointment::where('mobile_number','LIKE','%'.$user->mobile_number.'%')->get();
   $data=AppointmentResource::collection($Appointment);
    return response()->api( true, 'تم ',$data);

 
}
public function deleteAppointment($id){

     $appointment = Appointment::find($id);
        $appointment->delete();
    return response()->api(false, Messages::getMessage('DELETE_SUCCESS'),null);

}

}
