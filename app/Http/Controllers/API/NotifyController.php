<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\fcm_notify;
use App\Helpers\Messages;
use App\Models\Appointment;

use Auth;
use App\Http\Resources\UserResource;
use App\Http\Resources\NotifyResource;

class NotifyController extends Controller
{
    public function savePushNotificationToken(Request $request)
{
    $user=Auth::user();
      User::where('id', $user['id'])
        ->update([
            'device_token' => $request->device_token,
        ]);
         $data['user'] = new UserResource($user);
    return response()->api( true, 'token saved successfully',$data);
}

public function sendPushNotification(Request $request)
{
    $firebaseToken = User::whereNotNull('device_token')->pluck('device_token')->all();
    $SERVER_API_KEY = 'AAAACRck99Y:APA91bH-R1TeDSr5UPRCVPU8eKymYOEnGvwNbN_wQMZtj2--shkJm9TAqGTaaBG9PElTJldDGSahVYIzUzvj6EnC-PmG4Bi04DP_RukhfeCS9HMg8-vHsWsS3hxecHilWfksou2RwNTX';

    $data = [
        "registration_ids" => $firebaseToken,
        "notification" => [
            "title" => $request->title,
            "body" => $request->body,  
        ]
    ];

foreach ($firebaseToken as $ft) {
    $user=User::where('device_token','LIKE','%'.$ft.'%')->first();
   $fcm_notify=new fcm_notify;
    $fcm_notify->user=$user->id;
    $fcm_notify->title=$request->title;
    $fcm_notify->body=$request->body;
    $fcm_notify->save();

}
    

    $dataString = json_encode($data);

    $headers = [
        'Authorization: key=' . $SERVER_API_KEY,
        'Content-Type: application/json',
    ];

    $ch = curl_init();
  
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
           
    $response = curl_exec($ch);

  // dd($response);
    return redirect('add_dress');
}
public function sendnotifyappointment(Request $request,$id)
{
    $app=Appointment::find($id);
   $use = User::where('mobile_number',$app->mobile_number)->first();
    $firebaseToken = $use->device_token;
    $SERVER_API_KEY = 'AAAACRck99Y:APA91bH-R1TeDSr5UPRCVPU8eKymYOEnGvwNbN_wQMZtj2--shkJm9TAqGTaaBG9PElTJldDGSahVYIzUzvj6EnC-PmG4Bi04DP_RukhfeCS9HMg8-vHsWsS3hxecHilWfksou2RwNTX';

    $data = [
        "registration_ids" =>array($firebaseToken),
        "notification" => [
            "title" => $request->title,
            "body" => $request->body,  
        ]
    ];

   $user = User::where('device_token',$firebaseToken)->first();

   $fcm_notify=new fcm_notify;
    $fcm_notify->user=$user->id;
    $fcm_notify->title=$request->title;
    $fcm_notify->body=$request->body;
    $fcm_notify->save();

 $app = Appointment::find($id);

               $app->statue_app="مثبت ";

              $app->color = 'green';
               $app->save();
               $user=Auth::User();
             foreach($user->unreadNotifications as $appRemider){
               if($appRemider->data['type']=="app"  ){
                 if($appRemider->data['app']['id']==$id)
                    {
                        $appRemider->markAsRead();
                    }
                  
             }

        }
    

    $dataString = json_encode($data);

    $headers = [
        'Authorization: key=' . $SERVER_API_KEY,
        'Content-Type: application/json',
    ];

    $ch = curl_init();
  
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
           
    $response = curl_exec($ch);

 return redirect('/');
}
public function sendnotifyappointmentCancel(Request $request,$id)
{
    $app=Appointment::find($id);
   $use = User::where('mobile_number',$app->mobile_number)->first();
    $firebaseToken = $use->device_token;
    $SERVER_API_KEY = 'AAAACRck99Y:APA91bH-R1TeDSr5UPRCVPU8eKymYOEnGvwNbN_wQMZtj2--shkJm9TAqGTaaBG9PElTJldDGSahVYIzUzvj6EnC-PmG4Bi04DP_RukhfeCS9HMg8-vHsWsS3hxecHilWfksou2RwNTX';

    $data = [
        "registration_ids" => array($firebaseToken),
        "notification" => [
            "title" => $request->title,
            "body" => $request->body,  
        ]
    ];

   $user = User::where('device_token',$firebaseToken)->first();

   $fcm_notify=new fcm_notify;
    $fcm_notify->user=$user->id;
    $fcm_notify->title=$request->title;
    $fcm_notify->body=$request->body;
    $fcm_notify->save();

              $app = Appointment::find($id);

               $app->statue_app="ملغى ";
               $app->color = 'red';
               $app->save();
                $user=Auth::User();
             foreach($user->unreadNotifications as $appRemider){
               if($appRemider->data['type']=="app"  ){
                 if($appRemider->data['app']['id']==$id)
                    {
                        $appRemider->markAsRead();
                    }
                  
             }

        }
    

    $dataString = json_encode($data);

    $headers = [
        'Authorization: key=' . $SERVER_API_KEY,
        'Content-Type: application/json',
    ];

    $ch = curl_init();
  
    curl_setopt($ch, CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send');
    curl_setopt($ch, CURLOPT_POST, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $dataString);
           
    $response = curl_exec($ch);
    return redirect('/');
}
public function getnotify()
{
    $user=Auth::user();
   $fcm_notify=fcm_notify::where('user','LIKE','%'.$user->id.'%')->get();
   $data['notification']=$fcm_notify;
    return response()->api( true, 'تم ',$data);

 
}
public function delete()
{
      $user=Auth::user();
   $fcm_notify=fcm_notify::find($user->id);
        $fcm_notify->delete();
    return response()->api(false, Messages::getMessage('DELETE_SUCCESS'),null);
 
}

}
