<?php

namespace App\Http\Controllers;

use App\Models\Invoice;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Type;
use App\Models\Cleaning;
use App\Models\Design;
 use App\Models\User;
 use Auth;
use Illuminate\Support\Facades\DB;
class InvoiceController extends Controller
{
       function __construct()
    {

     $this->middleware('permission:إدارة الفواتير ', ['only' => ['create','store','edit','update','destroy']]);
      $this->middleware('permission:إدارة التقارير ', ['only' => ['sellingReport']]);
    }
    public function create()
    {
        $in=Invoice::all();
        foreach ($in as $in){
            if($in->invoice_line->count()==0){
                $in->delete();
            }
        }
        $Type=Type::all();
        $invoice = Invoice::all();

          $user=Auth::user();
       if(Auth::user()){

           return view('invoice.biginvoice',['invoice'=>$invoice,'Type'=>$Type,'user'=>$user]);
       }
      else return redirect('/logi');
    }

    public function store(Request $request)
    {
        if($request->invoice_date > Carbon::now()){
            $Type=Type::all();
            $invoice2 = Invoice::all();

            $user=Auth::user();
            return redirect()->back()->withErrors(['msg' => 'تاريخ غير صالح']);
        }

        $request->validate([
            'invoice_date'=>'required',
        ]);


        $invoice = new Invoice([
            'invoice_date' => $request->get('invoice_date'),
            'total_price' => 0
        ]);
        $invoice->save();
        return redirect('/create_invoiceLine_with_id/'.$invoice->id);
    }


    public function edit($id)
    {
        $invoice = Invoice::find($id);
        return view('invoice.biginvoice', compact('invoice'));
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'invoice_date'=>'required',
        ]);
        $invoice = Invoice::find($id);
        $invoice->invoice_date =  $request->get('invoice_date');
        $invoice->save();

       return redirect('/biginvoice');
    }

    public function destroy($id)
    {
        $invoice = Invoice::find($id);
        foreach ($invoice->invoice_line as $inv){
            $h=new InvoiceLineController();
            $h->destroy($inv->id);
        }
        $invoice->delete();


        return redirect('/biginvoice');
    }
   public function total_price($id){

        $invoice = Invoice::find($id);
        $invoice->total_price =0;
        foreach ($invoice->invoice_line as $line){
            $invoice->total_price += $line->total_price;
        }
        $invoice->save();
               return redirect('/in/'.$invoice->id);

    }
   public function sellingReport()
    {
         $price_clean=0;
        $price_design=0;
        $price_invoice=0;
        $num_pieces=0;
        $total_pieces=0;
        $Type=Type::all();
          $user=Auth::user();
       $Cleans=Cleaning::with('clothe')->get();
        $designs=Design::all();
        $invoices=Invoice::with('invoice_line')->get();

       foreach ($Cleans as $Clean)  {
        $price_clean=$price_clean+$Clean->cleaning_price;
       }
         foreach ($designs as $design)  {
        $price_design=$price_design+$design->designing_price;
       }
         foreach ($invoices as $invoice)  {
        $price_invoice=$price_invoice+$invoice->total_price;

       }
       if(Auth::user()){
            return view('report.SellingReport',compact('Cleans','price_clean','invoices','designs','price_design','num_pieces','total_pieces','price_invoice','user','Type')) ->with('i')->with('j')->with('k'); }
          else return redirect('/logi');
    }

}
