<?php

namespace App\Http\Controllers;

use App\Models\Pieces_Reminder;
use App\Models\Brides_Reminder;
use App\Models\Reminder;
use App\Models\User;
use App\Models\Type;
use App\Notifications\ReminderNotification;
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

class BridesRemindersController extends Controller
{

   function __construct()
    {

       $this->middleware('permission:التذكيرات' , ['only' => ['bride','store','bride_notification','destroy']]);
    }
    public function bride()
    {
          $user=Auth::user();
         $brideReminder = Brides_Reminder::all();
         $pieceReminder = Pieces_Reminder::all();
           $c=Carbon::today()->toDateString();
         $Type=Type::all();
          if(Auth::user()){
        return view('brideReminder.notification',['brideReminder'=>$brideReminder,'pieceReminder'=>$pieceReminder,'user'=>$user,'Type'=>$Type,'c'=>$c,'bride_errors'=>null,'piece_errors'=>null]);}
         else return redirect('/logi');
    }
    public function store(Request $request)
    {
        $user=Auth::user();
        $brideReminders = Brides_Reminder::all();
        $pieceReminder = Pieces_Reminder::all();
        $c=Carbon::today()->toDateString();
        $Type=Type::all();

        if($request->get('date')< Carbon::now()->toDateString()){
            return view('brideReminder.notification',['brideReminder'=>$brideReminders,'pieceReminder'=>$pieceReminder,'user'=>$user,'Type'=>$Type,'c'=>$c,'bride_errors'=>'تاريخ غير صالح','piece_errors'=>null]);
    }
        $request->validate([
            'content'=>'required',
            'date'=>'required',
            'mobile_number'=>'required',
            'emboridery'=>'required'
        ]);

        $brideReminder = new Brides_Reminder([
            'customer_name' => $request->get('content'),
            'date' => $request->get('date'),
            'mobile_number' => $request->get('mobile_number'),
            'emboridery' => $request->get('emboridery'),
            'content'=>'null'
        ]);
        $brideReminder->save();
        Auth::user()->notify(new ReminderNotification($brideReminder));
        return back();

    }



    public function destroy($id)
    {
        $brideReminder = Brides_Reminder::find($id);
        $brideReminder->delete();

        foreach (Auth::user()->unreadNotifications as $noti){
            if($noti->data['type']=='bride'){
                if($noti->data['id']==$id){
                    $noti->markAsRead();
                }
            }
        }

        return back();
    }


   public function bride_notification() {
          $user=Auth::user();
        $Type=Type::all();

        return view('master',['user'=>$user,'Type'=>$Type]);

    }
    public function mark_as_read($id) {
          $user=Auth::user();

        $user->unreadNotifications->where('id', $id)->markAsRead();
        return back();
    }



}
