<?php

namespace App\Http\Controllers;

use App\Models\Clothe;
use App\Models\Range;
use App\Models\Type;
use App\Models\size_general;
use App\Models\Invoice;
use App\Models\Design;
use App\Models\Cleaning;
use App\Models\Contract;
use App\Models\User;
use App\Models\Piece_Contract;
use App\Models\Store_Clothe;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;
use Auth;
use Illuminate\Http\Request;

class Filter extends Controller
{

    public function filt(Request $request)
    {
        $cl = Clothe::all()->where('clothes_type', $request->get('ctype'));
        $clothe = $cl->filter(function ($value, $key) use ($request) {
            if ($request->get('size') != 'non') {
                if ($request->get('kind') == 'selling_range') {
                    $r_sell = Range::all()->find($request->get('range'));
                    $r1_sell = $r_sell->from;
                    $r2_sell = $r_sell->to;
                    return ($value->size == $request->get('size')
                        && $value->selling_price  >= $r1_sell
                        && $value->selling_price  <= $r2_sell
                    );
                } elseif ($request->get('kind') == 'renting_range') {
                    $r_rent = Range::all()->find($request->get('range'));
                    $r1_rent = $r_rent->from;
                    $r2_rent = $r_rent->to;
                    return ($value->size == $request->get('size')
                        && $value->rent_price  >= $r1_rent
                        && $value->rent_price  <= $r2_rent
                    );
                } elseif ($request->get('kind') == 'non') {
                    return ($value->size == $request->get('size')
                    );
                }
            } elseif ($request->get('size') == 'non') {
                if ($request->get('kind') == 'selling_range') {
                    $r_sell = Range::all()->find($request->get('range'));
                    $r1_sell = $r_sell->from;
                    $r2_sell = $r_sell->to;
                    return ($value->selling_price  >= $r1_sell
                        && $value->selling_price  <= $r2_sell
                    );
                } elseif ($request->get('kind') == 'renting_range') {
                    $r_rent = Range::all()->find($request->get('range'));
                    $r1_rent = $r_rent->from;
                    $r2_rent = $r_rent->to;
                    return ($value->rent_price  >= $r1_rent
                        && $value->rent_price  <= $r2_rent
                    );
                }
                //
            }
            //
        });
        $range = range::all();
        $size_general = size_general::all();
        $Type = Type::all();
        $user = Auth::user();
        return view('filter.gallery', ['clothe' => $clothe, 'range' => $range, 'size_general' => $size_general, 'Type' => $Type, 'name' => $request->get('ctype'), 'user' => $user]);
    }
    public function index()
    {
        $Type = Type::all();
        $user = Auth::user();
        return view('master', compact('Type'), compact('user'));
    }
    public function display()
    {
        $clothe = Clothe::all();
        $range = range::all();
        $size_general = size_general::all();
        $Type = Type::all();
        $user = Auth::user();
        return view('filter.gallery', ['clothe' => $clothe, 'range' => $range, 'size_general' => $size_general, 'Type' => $Type, 'user' => $user]);
    }
    public function show($name)
    {
        $clothe = Clothe::all()->where('clothes_type', $name);

        $range = Range::all();
        $user = Auth::user();
        $size_general = size_general::all();
        $Type = Type::all();
        if (Auth::user()) {
            return view('filter.gallery', ['clothe' => $clothe, 'range' => $range, 'size_general' => $size_general, 'Type' => $Type, 'name' => $name, 'user' => $user]);
        } else {
            return redirect('/logi');
        }
    }
    public function search(Request $request)
    {
        $clothe = Clothe::where('name', 'LIKE', "%$request->name%")->get();
        $range = range::all();
        $size_general = size_general::all();
        $Type = Type::all();
        $user = Auth::user();
        return view('filter.search', ['clothe' => $clothe, 'range' => $range, 'size_general' => $size_general, 'Type' => $Type, 'user' => $user]);
    }
    public function filter_design(Request $request)
    {
        $design = Design::all();

        return $design->filter(function ($value, $key) use ($request) {

            $date = explode("-", $value->delivery_date);
            $day = $date[2];
            $month = $date[1];
            $year = $date[0];
            if ($request->get('year') != null) {
                if ($request->get('month') == null) {

                    return ($year == $request->get('year')
                    );
                } elseif ($request->get('month') != null) {

                    return ($year == $request->get('year') &&
                        $month == $request->get('month')
                    );
                }
            }
        });
    }
    public function filter_clean(Request $request)
    {
        $clean = Cleaning::all();

        return $clean->filter(function ($value, $key) use ($request) {

            $date = explode("-", $value->end);
            $day = $date[2];
            $month = $date[1];
            $year = $date[0];
            if ($request->get('year') != null) {
                if ($request->get('month') == null) {

                    return ($year == $request->get('year')
                    );
                } elseif ($request->get('month') != null) {

                    return ($year == $request->get('year') &&
                        $month == $request->get('month')
                    );
                }
            }
        });
    }
    public function filter_invoice(Request $request)
    {
        $invoice = Invoice::all();

        return $invoice->filter(function ($value, $key) use ($request) {

            $date = explode("-", $value->invoice_date);
            $day = $date[2];
            $month = $date[1];
            $year = $date[0];
            if ($request->get('year') != null) {
                if ($request->get('month') == null) {

                    return ($year == $request->get('year')
                    );
                } elseif ($request->get('month') != null) {

                    return ($year == $request->get('year') &&
                        $month == $request->get('month')
                    );
                }
            }
        });
    }
    public function filter_invoice_store(Request $request)
    {
        $invoice = Invoice::all();

        return $invoice->filter(function ($value, $key) use ($request) {


            if ($request->get("date") != null) {

                return $value->invoice_date = $request->get("date");
            }
        });
    }
    public function filter_invoice_date(Request $request)
    {
        $invoice = $this->filter_invoice_store($request);
        $Type = Type::all();
        $user = Auth::user();
        $Store_Clothe = Store_Clothe::all();
        return view('clothe.buy_product', ['invoice' => $invoice, 'Type' => $Type, 'user' => $user, 'Store_Clothe' => $Store_Clothe]);
    }
    public function filter_report(Request $request)
    {
        $piece_contract = Piece_Contract::all();

        return $piece_contract->filter(function ($value, $key) use ($request) {

            $date = explode("-", $value->contract['contract_date']);
            $day = $date[2];
            $month = $date[1];
            $year = $date[0];
            if ($request->get('year') != null) {
                if ($request->get('month') == null) {

                    return ($year == $request->get('year')
                    );
                } elseif ($request->get('month') != null) {

                    return ($year == $request->get('year') &&
                        $month == $request->get('month')
                    );
                }
            }
        });
    }

    public function filter_payment(Request $request)
    {
        $sum = 0;
        $sum1 = 0;
        $sum2 = 0;
        $user = Auth::user();
        $Type = Type::all();
        $piece_contract = $this->filter_report($request);
        $contract = Contract::all();;
        foreach ($piece_contract as $pc) {
            if ($pc->Clothe != null) {
                if ($pc->contract['contract_type'] == 'آجار') {
                    $sum = $sum + $pc->contract['total_price'];
                }
                if ($pc->contract['contract_type'] == 'بيع') {
                    $sum1 = $sum1 + $pc->contract['total_price'];
                }
                $sum2 = $sum2 + $pc->contract['total_price'];
            }
        }



        return view('report.PaymentsReport', ['piece_contract' => $piece_contract, 'contract' => $contract, 'sum1' => $sum1, 'sum' => $sum, 'Type' => $Type, 'sum2' => $sum2, 'user' => $user])->with('i')->with('j');
    }

    public function filter_selling(Request $request)
    {



        $price_clean = 0;
        $price_design = 0;
        $price_invoice = 0;
        $num_pieces = 0;
        $total_pieces = 0;
        $Type = Type::all();
        $user = Auth::user();
        $Cleans = $this->filter_clean($request);
        $designs = $this->filter_design($request);
        $invoices = $this->filter_invoice($request);
        foreach ($Cleans as $Clean) {
            $price_clean = $price_clean + $Clean->cleaning_price;
        }
        foreach ($designs as $design) {
            $price_design = $price_design + $design->designing_price;
        }
        foreach ($invoices as $invoice) {
            $price_invoice = $price_invoice + $invoice->total_price;
        }
        return view('report.SellingReport', ['Cleans' => $Cleans, 'price_clean' => $price_clean, 'invoices' => $invoices, 'designs' => $designs, 'price_design' => $price_design, 'num_pieces' => $num_pieces, 'total_pieces' => $total_pieces, 'price_invoice' => $price_invoice, 'user' => $user, 'Type' => $Type])->with('i')->with('j')->with('k');
    }
}
