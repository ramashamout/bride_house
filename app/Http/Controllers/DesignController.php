<?php

namespace App\Http\Controllers;

use App\Models\Design;
use App\Models\Clothe;
use App\Models\Type;
use App\Models\Customer;
use App\Models\Body_Measurement;
 use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Auth;
use Carbon\Carbon;
class DesignController extends Controller
{

    function __construct()
    {

       $this->middleware('permission:التصميم', ['only' => ['display','store','edit','update','destroy','show']]);
    }

  public function display()
  {

    $type=Type::all();
    $design=Design::all();
    $user=Auth::user();
    $Type=Type::all();
    if(Auth::user()){

    return view('design.design',['type'=>$type ,'design'=>$design,'user'=>$user,'Type'=>$Type]);}
     else return redirect('/logi');

  }




    public function store(Request $request)
    {

        $request->validate([

            'color'=>'required',
            'fabric_type'=>'required',
            'model'=>'required',
            'general_size'=>'required',
            'design_notes'=>'required',
            'received_date'=>'required',
            'delivery_date'=>'required',
            'designing_price'=>'required',
            'type'=>'required',
            'design_img'=>'required'
        ]);
        if($request->get('delivery_date')>$request->get('received_date')||$request->get('delivery_date') < Carbon::now()->toDateString()){
           return redirect()->back()->withErrors(['msg' =>'تاريخ غير صالح']);
        }
        $customer =new CustomerController();
        $custom=Customer::where('full_name', 'LIKE',"%{$request->get('full_name')}%")->first();
        if($custom!=null){
            $customer->update($request,$custom->id);
            $cust=$custom;
        }
        else{
            $cust=$customer->store($request);
        }

        $image=$request->file('design_img');
       $extension=$image->getClientOriginalExtension();
        $imageName = time().'.'.$extension;
        $path=public_path().'/images/design/';
      $image->move($path, $imageName);

        $design=new Design();
        $design->create([
        'customer_id' => $cust->id,
        'color' => $request->get('color'),
        'fabric_type' => $request->get('fabric_type'),
        'model' => $request->get('model'),
        'general_size' => $request->get('general_size'),
        'design_notes' => $request->get('design_notes'),
        'received_date' => $request->get('received_date'),
        'delivery_date' => $request->get('delivery_date'),
        'designing_price' => $request->get('designing_price'),
        'type' => $request->get('type'),
        'design_img' => $imageName
    ])->save();

         $id=Design::all()->last()->id;
        return redirect('/display/'.$id);
    }


    public function show($id)
    {
        $selected_Item = Design::find($id);
    }

    public function edit($id)
    {$type=Type::all();
        $design = Design::find($id);

        return view('design.design', compact('design'), compact('type'));
    }

    public function update(Request $request, $id)
    {

        $request->validate([
            'color'=>'required',
            'fabric_type'=>'required',
            'model'=>'required',
            'general_size'=>'required',
            'design_notes'=>'required',
            'received_date'=>'required',
            'delivery_date'=>'required',
            'designing_price'=>'required',
            'type'=>'required'
        ]);
        if($request->get('delivery_date')>$request->get('received_date')){

            return redirect()->back()->withErrors(['msg' =>'تاريخ غير صالح']);
        }
        $design = Design::find($id);
        $design->color =  $request->get('color');
        $design->fabric_type =  $request->get('fabric_type');
        $design->model =  $request->get('model');
        $design->general_size =  $request->get('general_size');
        $design->design_notes =  $request->get('design_notes');
        $design->received_date =  $request->get('received_date');
        $design->delivery_date =  $request->get('delivery_date');
        $design->designing_price =  $request->get('designing_price');
        $design->type =  $request->get('type');
        $image=$request->file('design_img');
        if($image!=null){
            File::delete(public_path( $design->design_img));

          $extension=$image->getClientOriginalExtension();
        $imageName = time().'.'.$extension;
        $path=public_path().'/images/design/';
      $image->move($path, $imageName);
            $design->design_img =$imageName;


        }

        $customer =new CustomerController();
        $custom=Customer::where('full_name', 'LIKE',"%{$request->get('full_name')}%")->first();
        if($custom!=null){
            $customer->update($request,$custom->id);
            $cust=$custom;
        }
        else{
            $cust=$customer->store($request);
        }
        $design->customer_id=$cust->id;
        $design->save();


        return redirect('/design_t');
    }

    public function destroy($id)
    {
        $design = Design::find($id);
        File::delete(public_path( $design->design_img));

        $design->delete();

        return redirect('/design_t');
    }



}
