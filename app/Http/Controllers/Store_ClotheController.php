<?php

namespace App\Http\Controllers;

use App\Models\Accessory;
use App\Models\Invoice_Line;
use Illuminate\Http\Request;
use App\Models\Store_Clothe;
use App\Models\Type;
use Auth;
use App\Models\Clothe;
use App\Models\User;
use App\Models\Image;

class Store_ClotheController extends Controller
{
    //


    public function store(Request $request)
    {


        $Store_Clothe = new Store_Clothe;
        $Store_Clothe->name = $request->name;
        $Store_Clothe->piece_img = $request->piece_img;
        $Store_Clothe->selling_price = $request->selling_price;
        $Store_Clothe->rent_price = $request->rent_price;
        $Store_Clothe->designer_name = $request->designer_name;
        $Store_Clothe->color = $request->color;
        $Store_Clothe->size = $request->size;
        $Store_Clothe->clothes_type = $request->get('type');
        $Store_Clothe->discount = $request->discount;
        if ($request->discount == 0) {

            $Store_Clothe->selling_price = $request->selling_price;
        } else {
            $Store_Clothe->selling_price = $request->selling_price - ($request->selling_price * ($request->discount / 100));
        }
        $request->old_price = $request->selling_price;
        $Store_Clothe->old_price = $request->old_price;


        $Store_Clothe->type = $request->get('type_general');




        $Store_Clothe->save();
    }

    public function store_invoice($id)
    {

        $Store =  Store_Clothe::find($id);
        $Store_Clothe = $Store->piece;
        if ($Store_Clothe->type == "clothe") {

            $clothe = new Clothe();
            $clothe->name = $Store_Clothe->name . ' ' . $Store->number_of_pieces;
            $clothe->piece_img = $Store_Clothe->piece_img;
            $clothe->old_price = $Store_Clothe->old_price;
            $clothe->selling_price = $Store_Clothe->selling_price;
            $clothe->rent_price = $Store_Clothe->rent_price;
            $clothe->designer_name = $Store_Clothe->designer_name;
            $clothe->color = $Store_Clothe->color;
            $clothe->discount = $Store_Clothe->discount;
            $clothe->statue = "متوفر ";
            $clothe->clothes_type = $Store_Clothe->clothes_type;
            $clothe->size = $Store_Clothe->size;
            $clothe->save();

            foreach ($Store_Clothe->image as $image) {
                $img = new Image;
                $img->id_product = $clothe->id;
                $img->src = $image->src;
                $img->save();
            }
            $Store->number_of_pieces = $Store->number_of_pieces - 1;
            $Store->save();
            return back();
            ////////////////////////////////////////////////////////////////////////

        }
        if ($Store_Clothe->type == "accessory") {
            $accessory = new Accessory;
            $accessory->name = $Store_Clothe->name . ' ' . $Store->number_of_pieces;
            $accessory->piece_img = $Store_Clothe->piece_img;
            $accessory->selling_price = $Store_Clothe->selling_price;
            $accessory->rent_price = $Store_Clothe->rent_price;
            $accessory->category = $Store_Clothe->category;
            $accessory->old_price = $Store_Clothe->selling_price;
            $accessory->statue_acc = "متوفر ";
            $accessory->Type_accessory = $Store_Clothe->Type_accessory;
            $accessory->save();

            $Store->number_of_pieces = $Store->number_of_pieces - 1;
            $Store->save();
            return back();
        }
    }

    public function show()
    {
        $Store_Clothe = Store_Clothe::all()->where('number_of_pieces', '>', 0);
        $Type = Type::all();
        $user = Auth::User();
        return view('clothe.buy_product', ['Store_Clothe' => $Store_Clothe, 'Type' => $Type, 'user' => $user]);
    }
    public function edit($id)
    {
        $Type = Type::all();
        $Store_Clothe = Store_Clothe::find($id);
        return view('clothe.buy_product', compact('Store_Clothe'), compact('Type'));
    }
    public function update(Request $request, $id)
    {
        $Store =  Store_Clothe::find($id);
        $Store_Clothe = $Store->piece;
        $Store_Clothe->name = $request->name;
        $Store_Clothe->piece_img = $request->piece_img;
        $Store_Clothe->selling_price = $request->selling_price;
        $Store_Clothe->rent_price = $request->rent_price;
        $Store_Clothe->designer_name = $request->designer_name;
        $Store_Clothe->color = $request->color;
        $Store_Clothe->size = $request->size;
        $Store_Clothe->clothes_type = $request->clothes_type;
        $Store_Clothe->discount = $request->discount;
        $Store_Clothe->old_price = $request->selling_price;
        $Store_Clothe->other_image = $request->other_image;
        $Store_Clothe->type = $request->type;
        $Store_Clothe->save();
    }

    public function destroy($id)
    {
        $Store_Clothe = Store_Clothe::find($id);
        $Store_Clothe->delete();

        return back();
    }
    public function v()
    {
        return view('clothe.ex');
    }
}
