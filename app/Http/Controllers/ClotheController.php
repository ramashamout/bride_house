<?php

namespace App\Http\Controllers;
use App\Models\Clothe;
use App\Models\size;
use App\Models\Calendar;
use App\Models\Type;
 use App\Models\User;
  use App\Models\size_general;
  use App\Models\Image;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Auth;

class ClotheController extends Controller
{
   function __construct()
    {
     $this->middleware('permission:إدارة الفساتين', ['only' => ['store','edit','update','destroy']]);


    }
public function master()
{
$user=Auth::User();
$Type = Type::all();

return view('master',['user'=>$user,'Type'=>$Type]);
}
   public function display($id)
{
    $events=array();

$clothe=Clothe::find($id);
$Type = Type::all();
$calendar=Calendar::where('piece_id','=',$id)->get();
$length = count($calendar);

for($i=0;$i<$length;$i++){

$events[$i]= [

'title'=> $calendar[$i]->title,
'start'=>$calendar[$i]->start,
'end'=>$calendar[$i]->end,
'color'=>$calendar[$i]->color,
];

}
    $Type=Type::all();
  $user=Auth::User();

return view('clothe.product', ['clothe'=>$clothe,'events'=>$events,'Type'=>$Type,'user'=>$user]);

}
  public function create()
    {
        $clothe=Clothe::all();
        $Type=Type::all();
        $user=Auth::user();
        $Type1=Type::all();
        $size_general=size_general::all();
        $si=size_general::all();
         if(Auth::user())
{
        return view('clothe.add_dress',['Type'=>$Type,'clothe'=>$clothe,'user'=>$user,'Type1'=>$Type1,'size_general'=>$size_general,'si'=>$si]); }
    else return redirect('/logi');

    }

    public function store(Request $request)
    {

        $request->validate([
            'name'=>'required',
            'piece_img'=>'required',
            'selling_price'=>'required',
            'rent_price'=>'required',
            'designer_name'=>'required',
            'color'=>'required',
            'size'=>'required',
            'clothes_type'=>'required',
            'other_image'=>'required',
        ]);

        $image = $request->file('piece_img');
        $extension=$image->getClientOriginalExtension();
        $imageName = time().'.'.$extension;
        $path=public_path().'/images/clothe/';
        $image->move($path, $imageName);

        $clothe = new Clothe;
        $clothe->id=$request->id;
        $clothe->name=$request->name;
        $clothe->piece_img=$imageName;
        $request->old_price=$request->selling_price;
        $clothe->rent_price=$request->rent_price;
        $clothe->designer_name=$request->designer_name;
        $clothe->color=$request->color;
        $clothe->discount=$request->discount;
        $clothe->statue="متوفر ";
        if($request->discount==0)
        {

            $clothe->selling_price=$request->selling_price;
        }
        else
        {
            $clothe->selling_price=$request->selling_price-($request->selling_price *($request->discount/100));

        }
        $request->old_price=$request->selling_price;
        $clothe->old_price=$request->old_price;
        $clothe->clothes_type=$request->clothes_type;
        $clothe->size=$request->size;
        if($this->check($request)==false){

            $clothe->save();



        }

        else
            return back();

        if($files=$request->file('other_image')){
            foreach($files as $image){

                $extension=$image->getClientOriginalExtension();
                $imageName = time().'.'.$extension;
                $path=public_path().'/images/clothe/';
                $image->move($path, $imageName);
                $img=new Image;
                $img->id_product=$clothe->id;
                $img->src=$imageName;
                $img->save();
            }}



        return back();


    }


    public  function  check(Request $request):bool
   {
       $clothe = Clothe::all();

        $bool=false;
        foreach ($clothe as $clothe) {

             if($clothe->name==$request->get('name'))
             {
                  $bool=true;
             }

        }

           return $bool;

 }



    public function edit($id)
    {   $type=Type::all();
        $clothe = Clothe::find($id);
        return view('clothe.add_dress', compact('clothe'), compact('type'));
    }

    public function update(Request $request, $id)
    {



        $clothe = Clothe::find($id);
        $clothe->name =  $request->name;
        $clothe->selling_price =$request->selling_price;
        $clothe->rent_price =$request->rent_price;
        $clothe->old_price=$request->old_price;
        $clothe->designer_name =$request->designer_name;
        $clothe->color =$request->color;
        $clothe->clothes_type =$request->clothes_type;
        $image=$request->file('piece_img');
        $clothe->discount=$request->discount;
        $clothe->size=$request->size;
        if($request->discount==0)
        {

            $clothe->selling_price=$request->selling_price;
        }
        else
        {
            $clothe->selling_price=$request->selling_price-($request->selling_price *($request->discount/100));

        }
        if($image!=null){
            File::delete(public_path( $clothe->piece_img));
            $extension=$image->getClientOriginalExtension();
            $imageName = time().'.'.$extension;
            $path=public_path().'/images/clothe/';
            $image->move($path, $imageName);

            $clothe->piece_img = $imageName;
        }

        if($files=$request->file('other_image')){
            foreach($files as $image){

                $extension=$image->getClientOriginalExtension();
                $imageName = time().'.'.$extension;
                $path=public_path().'/images/clothe/';
                $image->move($path, $imageName);
                $img= Image::find($clothe->id);
                $img->id_product=$clothe->id;
                $img->src=$imageName;
                $img->save();
            }}
        $clothe->save();
        return redirect('/add_dress');
    }
    public function destroy($id)
    {    $clothe = Clothe::find($id);
        foreach ($clothe->clean as $process){

            if ($process->start>=Carbon::now()->toDateString()){
                return redirect()->back()->withErrors(['msg' => ' يوجد عملية اخرى لهذه القطعة فلا يمكن حذفها']);
            }
        }

        foreach ($clothe->maintenance_notes as $process){
            if ($process->start>=Carbon::now()->toDateString()){
                return redirect()->back()->withErrors(['msg' =>' يوجد عملية اخرى لهذه القطعة فلا يمكن حذفها']);
            }
        }

        foreach ($clothe->contracts as $process){
            if($process->start>=Carbon::now()->toDateString()){
                return redirect()->back()->withErrors(['msg' => ' يوجد عملية اخرى لهذه القطعة فلا يمكن حذفها']);
            }
        }


        $clothe->statue="مباع ";
        $clothe->save();
//        $clothe->delete();
//        foreach ($clothe->image as $clothe)
//            File::delete(public_path($clothe->src));


        return redirect('/add_dress');
    }


    public function update_statue(Request $request,$id)
    {
        $clothe = Clothe::find($id);
        $clothe->statue="مؤجر ";
        $clothe->save();
    }
    public function update_statue_c($id)
    {


        $clothe = Clothe::find($id);
        $clothe->statue="متوفر ";
        $clothe->save();

        return back();

    }
    public function statue_maintenance(Request $request,$id)
    {
        $clothe = Clothe::find($id);
        $clothe->statue="صيانة ";
        $clothe->save();
    }
    public function statue_clean(Request $request,$id)
    {
        $clothe = Clothe::find($id);
        $clothe->statue="تنظيف ";
        $clothe->save();
    }
    public function search_piece(Request $request){
        $piece=Clothe::where('name', 'LIKE',"%{$request->get('name')}%")->first();
        if ($piece!=null){
            return $piece;
        }else{
            return'noresult';
        }

    }

}
