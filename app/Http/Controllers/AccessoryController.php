<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Models\Accessory;
use Illuminate\Support\Facades\File;
use App\Models\User;
use App\Models\Type;
use Auth;
class AccessoryController extends Controller
{

     function __construct()
    {
         $this->middleware('permission:إدارة الملحقات ', ['only' => ['create','store','edit','update','destroy']]);

    }
    public function create()
    {
        $accessory = Accessory::all();
          $user=Auth::user();
          $Type=Type::all();


            if(Auth::user()){
        return view('accessory.accessories',['Type'=>$Type,'user'=>$user,'accessory'=>$accessory]); }
       else
         return redirect('/logi');

    }

    public function store(Request $request)
    {


        $image = $request->file('piece_img');
        $extension=$image->getClientOriginalExtension();
        $imageName = time().'.'.$extension;
        $path=public_path().'/images/clothe/';
        $image->move($path, $imageName);

        // Getting values from the blade template form
        $accessory = new Accessory;
        $accessory->name=$request->name;
        $accessory->piece_img=$imageName;
        $accessory->selling_price=$request->selling_price;
        $accessory->old_price=$request->selling_price;
        $accessory->rent_price= $request->rent_price;
        $accessory->category=$request->category;
        $accessory->Type_accessory=$request->Type_accessory;
        $accessory->statue_acc="متوفر ";
        if($this->check($request)==true){

            return back();
        }
        else{
            $accessory->save();

            return back();  }

    }

    public  function  check(Request $request):bool
   {
       $accessory = Accessory::all();

        $bool=false;
        foreach ($accessory as $accessory) {

             if($accessory->name==$request->get('name'))
             {
                  $bool=true;
             }

        }

           return $bool;

 }
    public function edit($id)
    {
         $user=Auth::user();
        $accessory = Accessory::find($id);
         $Type=Type::all();
          return view('accessory.accessories',['Type'=>$Type,'user'=>$user,'accessory'=>$accessory]);
    }

    public function update(Request $request, $id)
    {



        $accessory = Accessory::find($id);
        // Getting values from the blade template form
        $accessory->name =  $request->name;
        $accessory->selling_price = $request->selling_price;
        $accessory->old_price=$request->selling_price;

        $accessory->rent_price = $request->rent_price;
        $accessory->category = $request->category;
        $accessory->statue_acc="متوفر ";
        $image=$request->file('piece_img');
        if($image!=null){
            File::delete(public_path($accessory->piece_img));

            $extension=$image->getClientOriginalExtension();
            $imageName = time().'.'.$extension;
            $path=public_path().'/images/accessory/';
            $image->move($path, $imageName);
            $accessory->piece_img = $imageName ;
        }
        $accessory->Type_accessory=$request->Type_accessory;
        $accessory->save();

        return back();
    }

    public function destroy($id)
    {
        $accessory = Accessory::find($id);

        foreach ($accessory->piece_contracts as $process){
            $p_c=$process->contract;
            if($p_c->start>=Carbon::now()->toDateString()){
                return redirect()->back()->withErrors(['msg' => ' يوجد عملية اخرى لهذه القطعة فلا يمكن حذفها']);
            }
        }

        $accessory->statue_acc="مباع ";
        $accessory->save();
//        File::delete(public_path( $accessory->piece_img));
//        $accessory->delete();

        return back();
    }

       public function crowns($name){
        $accessory=Accessory::all()->where('Type_accessory',$name);
        $user=Auth::user();
         $Type=Type::all();

         if(Auth::user()){
          return view('accessory.crowns',['Type'=>$Type,'accessory'=>$accessory,'user'=>$user]);}
          else return redirect('/logi');


    }
    public function update_statue(Request $request,$id)
    {
        $accessory = Accessory::find($id);
        $accessory->statue_acc="مؤجر ";
        $accessory->save();
    }
    public function update_statue_a($id)
    {
        $accessory = Accessory::find($id);
        $accessory->statue_acc="متوفر ";
        $accessory->save();
        return back();
    }


}
