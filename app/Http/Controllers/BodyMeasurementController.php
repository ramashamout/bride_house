<?php

namespace App\Http\Controllers;

use App\Models\Body_Measurement;
use Illuminate\Http\Request;
use App\Models\Type;
use App\Models\Customer;
use App\Models\Design;
use App\Models\User;
use Auth;
use Carbon\Carbon;

class BodyMeasurementController extends Controller
{
  
  public function display($id)
  {
    $Type = Type::all();
    $measure = Body_Measurement::all();
    $design = Design::find($id);
    $user = Auth::user();
    $c = Carbon::today()->toDateString();
    return view('measure.measure', ['Type' => $Type, 'measure' => $measure, 'id' => $id, 'design' => $design, 'user' => $user, 'c' => $c]);
  }
  public function show_measure($id)
  {
    $Type = Type::all();
    $user = Auth::user();
    $design = Design::find($id);
    $measure = $design->body_measurements;
    $c = Carbon::today()->toDateString();
    return view('measure.show_measure', ['Type' => $Type, 'user' => $user, 'measure' => $measure, 'design' => $design, 'c' => $c]);
  }



  public function store(Request $request)
  {
    $request->validate([
      'chest_circumference' => 'required',
      'waistline' => 'required',
      'hip_circumference' => 'required',
      'shoulder_width' => 'required',
      'shoulder_length' => 'required',
      'back_length' => 'required',
      'breast_length' => 'required',
      'distance_breasts' => 'required',

      'design_id' => 'required',
    ]);
    $Body = new  Body_Measurement([
      'chest_circumference' => $request->chest_circumference,
      'waistline' => $request->waistline,
      'hip_circumference' => $request->hip_circumference,
      'shoulder_width' => $request->shoulder_width,
      'shoulder_length' => $request->shoulder_length,
      'back_length' => $request->back_length,
      'breast_length' => $request->breast_length,
      'distance_breasts' => $request->distance_breasts,
      'Sleeve_Length' => $request->Sleeve_Length,
      'cuff_bracelet' => $request->cuff_bracelet,
      'skirt_length' => $request->hip_circumference,
      'design_id' => $request->design_id,


    ]);
    $Body->save();
    return redirect('/design_t');
   
  }


  public function edit($id)
  {
    $Type = Type::all();
    $design = Body_Measurement::find($id);
    $measure = $design->body_measurements;
    $user = Auth::user();
    $c = Carbon::today()->toDateString();
    return view('measure.measure', ['measure' => $measure, 'Type' => $Type, 'user' => $user, 'c' => $c]);
  }


  public function update(Request $request, $id)
  {
    $Body = Body_Measurement::find($id);
    $Body->chest_circumference = $request->chest_circumference;
    $Body->waistline = $request->waistline;
    $Body->hip_circumference = $request->hip_circumference;
    $Body->shoulder_width = $request->shoulder_width;
    $Body->shoulder_length = $request->shoulder_length;
    $Body->back_length = $request->back_length;
    $Body->breast_length = $request->breast_length;
    $Body->distance_breasts = $request->distance_breasts;
    $Body->Sleeve_Length = $request->Sleeve_Length;
    $Body->cuff_bracelet = $request->cuff_bracelet;
    $Body->skirt_length = $request->skirt_length;

    $Body->save();
    return back();
  }

  public function destroy($id)
  {
    $measure = Body_Measurement::find($id);
    $measure->delete();

    return redirect('/display/{{$measure->id}}');
  }
}
