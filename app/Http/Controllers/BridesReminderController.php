<?php

namespace App\Http\Controllers;

use App\Models\Brides_Reminder;
use Carbon\Carbon;
use Illuminate\Http\Request;

class BridesReminderController extends Controller
{
    public function index()
    {
        $brideReminder = Brides_Reminder::all();
        return view('brideReminder.index', compact('brideReminder'));
    }

    public function create()
    {
        return view('brideReminder.create');
    }
    public function store(Request $request)
    {
        if ($request->get('date') < Carbon::now()->toDateString()) {
            return 'invaleid date';
        }
        $request->validate([
            'content' => 'required',
            'date' => 'required',
            'mobile_number' => 'required',
            'emboridery' => 'required'
        ]);
        $brideReminder = new Brides_Reminder([
            'content' => $request->get('content'),
            'date' => $request->get('date'),
            'mobile_number' => $request->get('mobile_number'),
            'emboridery' => $request->get('emboridery')

        ]);
        $brideReminder->save();
        return redirect('/brideReminder')->with('success', 'brideReminder saved.');
    }

    public function show($id)
    {
        $selected_Item = Brides_Reminder::find($id);
    }

    public function edit($id)
    {
        $brideReminder = Brides_Reminder::find($id);
        return view('brideReminder.edit', compact('brideReminder'));
    }

    public function update(Request $request, $id)
    {
        
        $request->validate([
            'content' => 'required',
            'date' => 'required',
            'mobile_number' => 'required',
            'emboridery' => 'required'
        ]);
        $brideReminder = Brides_Reminder::find($id);
        $brideReminder->content =  $request->get('content');
        $brideReminder->date = $request->get('date');
        $brideReminder->mobile_number = $request->get('mobile_number');
        $brideReminder->emboridery = $request->get('emboridery');
        $brideReminder->save();

        return redirect('/brideReminder')->with('success', 'brideReminder updated.');
    }

    public function destroy($id)
    {
        $brideReminder = Brides_Reminder::find($id);
        $brideReminder->delete();

        return redirect('/brideReminder')->with('success', 'brideReminder removed.');
    }

    public function bride_notification()
    {
        $user = \App\Models\User::find(2);
        return view('brideReminder.bride', compact('user'));
    }
    public function mark_as_read($id)
    {
        $user = \App\Models\User::find(2);
        $user->unreadNotifications->where('id', $id)->markAsRead();
        return redirect('/brideNotification');
    }
}
