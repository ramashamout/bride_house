<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use App\Models\Type;
use App\Models\Maintenance_Note;
use App\Models\User;
use Auth;

class CustomerController extends Controller
{

    public function all_customer()
    {
        $Type = Type::all();
        $customer = customer::all();
        $user = Auth::user();
        if (Auth::user()) {

            return view('customer.all_customer', ['Type' => $Type, 'customer' => $customer, 'user' => $user]);
        } else {
            return redirect('/logi');
        }
    }
    public function customer_note($id)
    {
        $Type = Type::all();
        $customer = customer::find($id);
        $user = Auth::User();
        return view('customer.customer_note', ['Type' => $Type, 'customer' => $customer, 'user' => $user]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'full_name' => 'required',
            'mobile_number' => 'required',
            'address' => 'required'
        ]);

        $customer = new Customer([
            'full_name' => $request->get('full_name'),
            'mobile_number' => $request->get('mobile_number'),
            'address' => $request->get('address')
        ]);
        $customer->save();
        if ($request->get('designing_price') != null || $request->get('hall'))
            return $customer;
        return redirect('/customer');
    }


    public function edit($id)
    {
        $Type = Type::all();
        $customer = Customer::find($id);
        $maintenance = Maintenance_Note::find($id);
        $user = Auth::user();

        return view('customer.customer', ['Type' => $Type, 'customer' => $customer, 'maintenance' => $maintenance, 'user' => $user]);
    }

    public function update(Request $request, $id)
    {


        $customer = Customer::find($id);
        $customer->full_name =  $request->full_name;
        $customer->mobile_number = $request->mobile_number;
        $customer->address = $request->address;
        $customer->save();

        return redirect('/edit/{{$customer->id}}');
    }


    public function destroy($id)
    {
        $customer = Customer::find($id);
        $customer->delete();

        return redirect('/all_customer');
    }
}
