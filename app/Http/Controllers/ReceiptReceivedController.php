<?php

namespace App\Http\Controllers;

use App\Models\Receipt_Received;
use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Type;
use Auth;

class ReceiptReceivedController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:وصولة الاستلام ', ['only' => ['index', 'create', 'store', 'edit', 'update']]);
    }
    public function index()
    {
        $receipt = Receipt_Received::all();
        $contract_id = 1;
        return view('receipt.index', compact('receipt'), compact('contract_id'));
    }

    public function create()
    {
        return view('receipt.create'); 

    }

    public function store(Request $request)
    {
       
        $request->validate([
            'contract_id' => 'required',
            'amount' => 'required',
            'description' => 'required',
            'receipt_date' => 'required'
        ]);
        $receipt = new Receipt_Received([
            'contract_id' => $request->get('contract_id'),
            'amount' => $request->get('amount'),
            'description' => $request->get('description'),
            'receipt_date' => $request->get('receipt_date')
        ]);
        $receipt->save();
        return back();
    }

    public function show($id)
    {
        $selected_Item = Receipt_Received::find($id);
    }

    public function edit($id)
    {
        $receipt = Receipt_Received::find($id);
        return view('receipt.receipt', compact('receipt')); 
    }

    public function update(Request $request, $id)
    {
        
        $request->validate([
            'amount' => 'required',
            'description' => 'required',
            'receipt_date' => 'required'
        ]);
        $receipt = Receipt_Received::find($id);
        
        $receipt->amount =  $request->get('amount');
        $receipt->description =  $request->get('description');
        $receipt->receipt_date = $request->get('receipt_date');
        $receipt->save();

        return back();
    }

    public function destroy($id)
    {
        $receipt = Receipt_Received::find($id);
        $receipt->delete();

        return back();
    }


    public function create_receipt_with_id($contract_id)
    {

        $Type = Type::all();
        $user = Auth::user();
        $receipt = Receipt_Received::all();
        return view('receipt.receipt', ['receipt' => $receipt, 'contract_id' => $contract_id, 'Type' => $Type, 'user' => $user]);
    }
    public function display_receipt($contract_id)
    {
        $receipt = Receipt_Received::all();
       
        return view('receipt.index', compact('receipt'), compact('contract_id')); 
    }
}
