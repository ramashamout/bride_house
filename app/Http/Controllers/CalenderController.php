<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Calendar;
use App\Models\Cleaning;
use Auth;

class CalenderController extends Controller
{
	//
	public function store(Request $request)
	{
		$calender = new Calendar;
		$calender->title = $request->title;
		$calender->color = $request->color;
		$calender->start = $request->start;
		$calender->piece_id = $request->piece_id;
		$calender->end = $request->end;
		$calender->is_check = $request->is_check;
		$calender->is_check1 = $request->is_check1;

		$calender->save();
		return back();
	}
	public function edit($id)
	{
		$Type = Type::all();
		$calender = Calendar::find($id);
		return view('calender', ['Type' => $Type, 'calender' => $calender]);
	}
	public function update(Request $request, $id)
	{

		$calender = Calendar::find($id);
		$calender->title = $request->title;
		$calender->color = $request->color;
		$calender->start = $request->start;
		$calender->piece_id = $request->piece_id;
		$calender->end = $request->end;
		$calender->is_check = $request->is_check;
		$calender->is_check1 = $request->is_check1;
		$calender->save();
		return back();
	}
	public function update_color($id)
	{
		$calender = Calendar::find($id);
		$calender->color = "palegreen";
		$calender->title = "انتهاء الآجار ";
		$calender->is_check = 1;
		$calender->is_check1 = 0;
		$calender->save();
	}
	public function destroy($id)
	{
		$calender = Calendar::find($id);
		$calender->delete();
		return redirect('/all_customer');
	}
}
