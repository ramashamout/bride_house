<?php

namespace App\Http\Controllers;

use App\Models\Banner;
use App\Models\Type;

use App\Models\User;
use Auth;
use Illuminate\Http\Request;

class BannerController extends Controller
{

    public function index()
    {
        $Banner = Banner::all();
        $Type = Type::all();
        $user = Auth::user();

        return view('Banner.add_Banner', ['Type' => $Type, 'Banner' => $Banner, 'user' => $user]);
    }

    public function create()
    {
        return view('Banner.create'); 
    }
    public function store(Request $request)
    {

        $image = $request->file('image');
        $extension = $image->getClientOriginalExtension();
        $imageName = time() . '.' . $extension;
        $path = public_path() . '/images/Banner/';
        $image->move($path, $imageName);
        $Banner = new Banner;
        $Banner->image = $imageName;
        $Banner->save();
        return back();
    }

    public function show($id)
    {
        $selected_Item = Banner::find($id);
    }

    public function edit($id)
    {
        $Banner = Banner::find($id);
        return view('Banner.edit', compact('Banner'));
    }

    public function update(Request $request, $id)
    {
        $image = $request->file('image');
        if ($image != null) {
            File::delete(public_path($Banner->image));
            $extension = $image->getClientOriginalExtension();
            $imageName = time() . '.' . $extension;
            $path = public_path() . '/images/Banner/';
            $image->move($path, $imageName);
        }
        $Banner = Banner::find($id);
        $Banner->image =  $imageName;
        $Banner->save();

        return redirect('/Banner')->with('success', 'type updated.');
    }

    public function destroy($id)
    {
        $Banner = Banner::find($id);
        $Banner->delete();

        return back();
    }
}
