<?php

namespace App\Http\Controllers;

use App\Models\Cleaning;
use DemeterChain\C;
use Illuminate\Http\Request;
use DateTime;
use App\Models\Type;
use App\Models\Clothe;
use App\Models\Calendar;
use Carbon\Carbon;
use App\Models\User;
use App\Notifications\ReminderNotification;
use Auth;
class CleaningController extends Controller
{
        function __construct()
    {
         $this->middleware('permission:التنظيف' , ['only' => ['create','store','edit','update','destroy']]);

    }
    public function create()
{
    $Type=Type::all();
    $clean=Cleaning::all();
     $clothe=Clothe::all();
          $user=Auth::user();

          if(Auth::user()){
    return view('clean.cleaning',['Type'=>$Type,'clean'=>$clean,'clothe'=>$clothe,'user'=>$user]);}
    else return redirect('/logi');

}
    public function store(Request $request)
    {

        if($request->end<$request->start || $request->start < Carbon::now()->toDateString()){
            return redirect()->back()->withErrors(['msg' => 'تاريخ غير صالح']);

        }

        $c=Clothe::where('name',$request->name_product)->first();
 if($c==null){
            return redirect()->back()->withErrors(['msg' => 'هذه القطعة غير موجودة']);
        }
        foreach ($c->clean as $process){
            if($process->start<=$request->start && $request->start <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($process->start<=$request->end && $request->end <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($request->start<=$process->start && $process->start <= $request->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
        }

        foreach ($c->maintenance_notes as $process){
            if($process->start<=$request->start && $request->start <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($process->start<=$request->end && $request->end <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($request->start<=$process->start && $process->start <= $request->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
        }

        foreach ($c->contracts as $process){
            if($process->contract_type =='بيع'){
                if($process->start<=$request->start || $process->start<=$request->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
            }else{
                if($process->start<=$request->start && $request->start <= $process->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
                elseif ($process->start<=$request->end && $request->end <= $process->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
                elseif ($request->start<=$process->start && $process->start <= $request->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
            }

        }

        $i=0;
        $calendar =new CalenderController();
        $ca=$calendar->store($request);
        $clean = new Cleaning;
        $clean->id=$request->id;
        $clean->piece_id=$request->piece_id;
        $clean->end=$request->end;
        $clean->start=$request->start;
        $clean->cleaning_price=$request->cleaning_price;
        $clean->laundry=$request->laundry;
        $clean->title=$request->title;
        $clean->color=$request->color;
        $clean->name_product=$request->name_product;
        $clean->is_check=$request->is_check;
        $clean->is_check1=$request->is_check1;


        if($this->check($request)==true)
        {

            return redirect('/');
        }
        else{
            $clean->save();



            return redirect('/');
        }

    }
    public  function  check(Request $request):bool
   {
       $clean = Cleaning::all();

        $bool=false;
        foreach ($clean as $clean) {

            if($clean->name_product==$request->get('name_product'))
             {

            if(($clean->start <= $request->get('start'))&&($clean->end >= $request->get('start')))
                  {
                      if(($clean->start <= $request->get('end'))&&($clean->end >= $request->get('end')))


                        { $bool=true;}

                    }

             }

        }

           return $bool;

   }


    public function edit($id)
    {
        $clean = Cleaning::find($id);

        return view('clean.cleaning',['clean'=>$clean]);
    }
    public function update(Request $request, $id)
    {
        $request->validate([
            'piece_id'=>'required',
            'start'=>'required',
            'end'=>'required',
            'cleaning_price'=>'required',
            'laundry'=>'required',
            'title'=>'required',
            'color'=>'required',
            'name_product'=>'required',

        ]);

        $clean = Cleaning::find($id);
        if($request->end<$request->start || $request->start < Carbon::now()->toDateString()){
            return redirect()->back()->withErrors(['msg' => 'تاريخ غير صالح']);
        }

            $c=Clothe::where('name',$request->name_product)->first();
           if($c==null){
            return redirect()->back()->withErrors(['msg' => 'هذه القطعة غير موجودة']);
             }        foreach ($c->clean as $process){
            if($process->id !=$clean->id){
            if($process->start<=$request->start && $request->start <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($process->start<=$request->end && $request->end <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($request->start<=$process->start && $process->start <= $request->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
        }
        }

        foreach ($c->maintenance_notes as $process){
            if($process->start<=$request->start && $request->start <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($process->start<=$request->end && $request->end <= $process->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
            elseif ($request->start<=$process->start && $process->start <= $request->end){
                return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
            }
        }

        foreach ($c->contracts as $process){
            if($process->contract_type =='بيع'){
                if($process->start<=$request->start || $process->start<=$request->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
            }else{
                if($process->start<=$request->start && $request->start <= $process->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
                elseif ($process->start<=$request->end && $request->end <= $process->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
                elseif ($request->start<=$process->start && $process->start <= $request->end){
                    return redirect()->back()->withErrors(['msg' => 'يوجد عملية اخرى لهذه القطعة في التاريخ المدخل']);
                }
            }

        }
        $user=Auth::user();
        $calendar =new CalenderController();
        $cal=calendar::where('id', 'LIKE',"%{$request->get('id')}%")->first();
        if($cal!=null){
            $calendar->update($request,$cal->id);
        }

        $clean = Cleaning::find($id);
        if($clean->start<= Carbon::now()->toDateString() && $request->start > Carbon::now()->toDateString()){
            $clo=Clothe::find($clean->clothe->id);
            $clo->statue="متوفر ";
            $clo->save();
        }

        foreach ($user->unreadNotifications as $appRemider ){

            if($appRemider->data['type']=="clean"  ){

                if($appRemider->data['clean']['end']==$clean->end)
                {

                    $appRemider->delete();
                }

            }

        }
        $clean->piece_id=$request->get('piece_id');
        $clean->title=$request->get('title');
        $clean->color=$request->get('color');
        $clean->end =  $request->get('end');
        $clean->start = $request->get('start');
        $clean->cleaning_price = $request->get('cleaning_price');
        $clean->laundry = $request->get('laundry');
        $clean->name_product= $request->get('name_product');
        $clean->is_check=$request->get('is_check');
        $clean->is_check1=$request->get('is_check1');
        $clean->save();




        return redirect('/');
    }

    public function destroy($id)
    {


        $user=Auth::user();
        foreach ($user->unreadNotifications as $appRemider ){

            if($appRemider->data['type']=="clean"  ){

                if($appRemider->data['clean']['id']==$id)
                {

                    $appRemider->delete();
                }

            }

        }
        $clean = Cleaning::find($id);
        if($clean->start<= Carbon::now()->toDateString() ){
            $clo=Clothe::find($clean->clothe->id);
            $clo->statue="متوفر ";
            $clo->save();
        }


        $calendar =new CalenderController();
        $cal=calendar::where('piece_id', 'LIKE',$clean->piece_id)->first();
        $cal->delete();


        $clean->delete();

        return redirect('/');
    }

       public function mark_as_read($id) {
        $user=Auth::user();
        $g=$user->unreadNotifications->where('id', $id)->first();
        $user->unreadNotifications->where('id', $id)->markAsRead();
         $piece_id= $g->data['clean']['piece_id'];

        return back();
    }
    public function update_notification_clean($id)
    {

               $clean = Cleaning::find($id);
               $user=Auth::user();

             foreach($user->unreadNotifications as $appRemider){
               if($appRemider->data['type']=="clean"  ){
                 if($appRemider->data['clean']['id']==$id)
                    {


                    $g=$user->unreadNotifications->where('id', $id)->first();
                    $e=$clean->end;
                     $g->data['clean']['end']=$g->data['clean'][$e];



                    }}}





    }



}
