<?php

namespace App\Providers;
use Illuminate\Routing\ResponseFactory;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
         Schema::defaultStringLength(191);
          ResponseFactory::macro('api', function ( $status = false , $message = '',$data = null) {
            return response()->json([
                'status' => $status, //1 or 0
                
                'message' => $message,
                'data' => $data,
                
                
                

            ]);
        });
    }

}
