<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to the "home" route for your application.
     *
     * This is used by Laravel authentication to redirect users after login.
     *
     * @var string
     */
    public const HOME = '/dashboard';

    /**
     * The controller namespace for the application.
     *
     * When present, controller route declarations will automatically be prefixed with this namespace.
     *
     * @var string|null
     */
    // protected $namespace = 'App\\Http\\Controllers';

    /**
     * Define your route model bindings, pattern filters, etc.
     *
     * @return void
     */
    public function boot()
    {
        $this->configureRateLimiting();

        $this->routes(function () {
            Route::prefix('api')
                ->middleware('api')
                ->namespace($this->namespace)
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/web.php'));

            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/BodyMeasurement.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/customer.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/Design.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/MaintenanceNote.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/Contract.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/ReceiptReceived.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/ItemsDeliverd.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/Cleaning.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/Appointments.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/InvoiceLine.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/Invoice.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/BridesReminders.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/PieceReminder.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/Clothe.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/Accessory.php'));
            Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/report.php'));
           Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/store_clothe.php'));
           Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/Type.php'));
          Route::middleware('web')
                ->namespace($this->namespace)
                ->group(base_path('routes/Banner.php'));
        });
    }

    /**
     * Configure the rate limiters for the application.
     *
     * @return void
     */
    protected function configureRateLimiting()
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by(optional($request->user())->id ?: $request->ip());
        });
    }
}
