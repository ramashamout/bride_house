<?php

namespace App\Console;

use App\Models\Accessory;
use App\Models\Clothe;
use App\Models\Contract;
use App\Models\Cleaning;
use App\Models\Maintenance_Note;
use App\Models\Reminder;
use App\Models\User;
use App\Models\action;
use App\Notifications\ReminderNotification;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

use Illuminate\Support\Facades\Notification;


//
//use Illuminate\Support\Facades\Notification;


class Kernel extends ConsoleKernel
{
    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {

        $schedule->call(function () {
            $now=Carbon::now()->year;

            $clean=Cleaning::all();
            foreach ($clean as $clean){
                if($now-date('Y', strtotime($clean->start))>=5){
                    $clean->delete();
                }
            }
            $maintenance=Maintenance_Note::all();
            foreach ($maintenance as $maintenance){
                if($now-date('Y', strtotime($maintenance->start))>=5){
                    $maintenance->delete();
                }
            }
            $contract=Contract::all();
            foreach ($contract as $contract){
                if($now-date('Y', strtotime($contract->start))>=5){
                    $contract->delete();
                }
            }

         })->yearly();

        $schedule->call(function () {
//            $user=User::all()->where('type' ,'!=','admin')->first();
            $clean = Cleaning::all();
            $contract = Contract::all();
            $users=User::all()->whereIn('type',['admin','employ']);
            foreach ($users as $user) {

                foreach ($user->unreadNotifications as $noti) {
                    if ($noti->data['type'] == "bride" || $noti->data['type'] == "piece") {
                        if ($noti->data['date'] < Carbon::today()->toDateString()) {
                            $noti->markAsRead();

                        }
                    }

                }
                $r = Reminder::all();
                foreach ($r as $r) {
                    if ($r->date < Carbon::today()->toDateString()) {
                        $r->delete();
                    }
                }
                $bool = 1;


                if ($bool == 1) {

                    $maintenance = Maintenance_Note::all();

                    foreach ($clean as $clean) {
                        if ($clean->start == Carbon::today()->toDateString()) {
                            $clothe = Clothe::find($clean->piece_id);
                            $clothe->statue = "تنظيف  ";
                            $clothe->save();
                        }
                        if ($clean->end == Carbon::today()->toDateString()) {
                            $cc=1;
                            foreach ($user->unreadNotifications as $noti){
                                if ($noti->data['type'] == "clean" && $noti->data['clean']['id'] == $clean->id ) {
                                    $cc = 0;
                                }
                            }
                            if($cc==1){
                                $result = ['type' => 'clean', 'clean' => $clean];
                                Notification::send($users, new ReminderNotification($result));

                            }

                        }

                    }
                    foreach ($maintenance as $maintenance) {

                        if ($maintenance->start == Carbon::today()->toDateString()) {
                            $clothe = Clothe::find($maintenance->piece_id);
                            $clothe->statue = "صيانة ";
                            $clothe->save();
                        }
                        if ($maintenance->end == Carbon::today()->toDateString()) {
                            $mm=1;
                            foreach ($user->unreadNotifications as $noti){
                                if ($noti->data['type'] == "maintenance" && $noti->data['maintenance']['id'] == $maintenance->id ) {
                                    $mm = 0;
                                }
                            }
                            if($mm==1){
                                $result = ['type' => 'maintenance', 'maintenance' => $maintenance];
                                Notification::send($users, new ReminderNotification($result));
                            }

                        }


                    }


                    foreach ($contract as $contract) {


                        if ($contract->start == Carbon::today()->toDateString()) {
                            $clothe = Clothe::find($contract->piece_id);
                            if ($clothe->statue !='متوفر ') {
                                $co=1;
                                foreach ($user->unreadNotifications as $noti){
                                    if ($noti->data['type'] == "warning" && $noti->data['con_id'] == $contract->id ) {
                                        $co = 0;
                                    }
                                }
                                if($co==1){
                                    $con='يوجد عقد للقطعة '.$contract->clothe->name.' و لم تتوفر بالمتجر بعد';
                                    $result = ['type' => 'warning', 'content' =>$con ,'con_id'=>$contract->id ];
                                    Notification::send($users, new ReminderNotification($result));

                                }

                            }
                            else{
                                if ($contract->contract_type == "بيع") {
                                    $clothe = Clothe::find($contract->piece_id);
                                    $clothe->statue = "مباع ";
                                    $clothe->save();

                                    foreach ($contract->piece_contracts as $p_con){

                                        if($p_con->accessory != null){
                                            $ac=Accessory::find($p_con->piece_id);
                                            $ac->statue_acc=  "مباع ";
                                            $ac->save();

                                        }
                                    }
                                } else {
                                    $clothe = Clothe::find($contract->piece_id);
                                    $clothe->statue = "مؤجر ";
                                    $clothe->save();

                                    foreach ($contract->piece_contracts as $p_con){
                                        if($p_con->accessory != null){
                                            $ac=Accessory::find($p_con->piece_id);
                                            $ac->statue_acc= "مؤجر ";
                                            $ac->save();
                                        }
                                    }

                                }
                            }
                        }
                        if ($contract->end != null && $contract->end == Carbon::today()->toDateString()) {
                            $co=1;
                            foreach ($user->unreadNotifications as $noti){
                                if ($noti->data['type'] == "contract" && $noti->data['contract']['id'] == $contract->id ) {
                                    $co = 0;
                                }
                            }
                            if($co==1){
                                $result = ['type' => 'contract', 'contract' => $contract];
                                Notification::send($users, new ReminderNotification($result));

                            }

                        }



                    }
                }

            }
            $action=action::all()->last();
            $clothes=Clothe::All();
            $user=User::all()->where('type' ,'admin')->first();
            if($action){

                if(Carbon::now()->toDateString()==$action->action_date)
                {
                    foreach ($clothes as $clothe) {
                        if($clothe->statue=="في انتظار الزبون")
                        {
                            $clothe->statue="متوفر";
                            $clothe->save();
                            foreach ($user->unreadNotifications as $noti){
                                if ($noti->data['type'] == "action" ) {
                                    $noti->markAsRead();
                                }
                            }
                        }
                    }
                }
            }


         })->everyMinute();


//        })->daily();

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
